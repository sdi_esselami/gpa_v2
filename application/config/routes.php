<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route[LOGIN_PAGE] = 'examples/login';



// Loging and sessions...
$route['default_controller'] = 'controller';
//$route['login'] = 'controller/login';
//$route['logout'] = 'controller/logout';
//$route['lock_session'] = 'controller/lock_session';
$route['wake_up'] = 'controller/wake_up';
$route['unlock_session'] = 'controller/unlock_session';
$route['account_settings'] = 'controller/account_settings';
//$route['edit_password'] = 'controller/edit_password';
// Index page
$route['accueil'] = 'controller/index';
// Vehicle management : add edit delete show...
$route['add_vehicle_page'] = 'controller/add_vehicle_page';
$route['edit_vehicle_page'] = 'controller/edit_vehicle_page';
$route['add_vehicle'] = 'controller/add_vehicle';
$route['all_vehicles'] = 'controller/all_vehicles';
$route['add_affectation'] = 'controller/add_affectation';
$route['add_inssurance'] = 'controller/add_inssurance';
$route['add_accident'] = 'controller/add_accident';

// maintenances reparations..
$route['add_maintenance'] = 'controller/add_maintenance';
$route['add_reparation'] = 'controller/add_reparation';
$route['all_maintenances_page'] = 'controller/all_maintenances_page';
$route['all_reparations_page'] = 'controller/all_reparations_page';
$route['all_accidents_page'] = 'controller/all_accidents_page';



// drivers management
$route['add_driver'] = 'controller/add_driver';
$route['edit_driver'] = 'controller/edit_driver';
$route['edit_driver_page'] = 'controller/edit_driver_page';
$route['add_driver_page'] = 'controller/add_driver_page';
$route['all_drivers'] = 'controller/all_drivers';
$route['delete_driver'] = 'controller/delete_driver';

// history
$route['get_accidents'] = 'controller/get_accidents';
$route['get_assurances'] = 'controller/get_assurances';
$route['get_affectations'] = 'controller/get_affectations';

//vehicle_info
$route['get_vehicle_info'] = 'controller/get_vehicle_info';
$route['update_vehicle'] = 'controller/update_vehicle';
$route['update_status'] = 'controller/update_status';
$route['add_driver_vehicle'] = 'controller/add_driver_vehicle';
$route['update_carte_naftal'] = 'controller/update_carte_naftal';
$route['delete_vehicle'] = 'controller/delete_vehicle';

// table management (chef de parc)
$route['add_compagnie_assurance'] = 'controller/add_compagnie_assurance';
$route['add_reparateur'] = 'controller/add_reparateur';
$route['compagnies_assurances'] = 'controller/compagnies_assurances';
$route['all_reparateurs_page'] = 'controller/all_reparateurs_page';

// manager panel
$route['all_users_page'] = 'controller/all_users_page';
$route['add_user'] = 'controller/add_user';
$route['edit_user'] = 'controller/edit_user';
$route['delete_user'] = 'controller/delete_user';
$route['table_entretiens'] = 'controller/table_entretiens';
$route['delete_type_entretien'] = 'controller/delete_type_entretien';
$route['add_type_entretien'] = 'controller/add_type_entretien';
$route['edit_type_entretien'] = 'controller/edit_type_entretien';

$route['delete_exterior_part'] = 'controller/delete_exterior_part';
$route['add_exterior_part'] = 'controller/add_exterior_part';
$route['edit_exterior_part'] = 'controller/edit_exterior_part';

$route['delete_interior_part'] = 'controller/delete_interior_part';
$route['add_interior_part'] = 'controller/add_interior_part';
$route['edit_interior_part'] = 'controller/edit_interior_part';

$route['composants_internes'] = 'controller/composants_internes';
$route['composants_externes'] = 'controller/composants_externes';




// print
$route['vehicle_details'] = 'controller/vehicle_details';


// upload files
$route['upload_decision'] = 'controller/upload_decision';
$route['upload_accident'] = 'controller/upload_accident';
// Errors...
$route['404_override'] = 'controller/error_';
$route['translate_uri_dashes'] = FALSE;
