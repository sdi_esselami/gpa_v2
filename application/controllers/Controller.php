<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Controller extends CI_Controller {

	  public function __construct()
  {
    parent::__construct();
   
     // $this->load->file('plugins/js/class.uploader.php');
  }

	public function index()
	{
		$this->load->view("admin_panel/accueil");


		/* if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());
        $data['username'] = $this->session->userdata('username');
        $data['firstname'] = $this->session->userdata('firstname');
        $data['lastname'] = $this->session->userdata('lastname');
        $data['group_name'] = $this->session->userdata('group_name');
        if ($data['group_name'] == 'CHEF_PARC') {
        	 $data['number_drivers'] = $this->ModelDrivers->get_number_drivers($this->session->userdata('id_structure'));
             $data['number_vehicles'] = $this->ModelVehicles->get_number_vehicles($this->session->userdata('code_wilaya'));
        $this->load->view("admin_panel/accueil",$data);
        } else if ($data['group_name'] == 'MANAGER') {
        	$data['number_users'] = $this->ModelUsers->get_number_users();
        	$this->load->view("manager_panel/accueil",$data);
        } 
       
      }
    }else {
      $this->login_form();
    }
		
	}

		public function all_maintenances_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());


		$data = array(
						'maintenances' => $this->ModelMaintenances->get_all_maintenances($this->session->userdata('code_wilaya')) ,
						'vehicles' => $this->ModelVehicles->get_all_vehicles($this->session->userdata('code_wilaya')),
						'type_entretiens' => $this->ModelMaintenances->get_types_entretiens(),
						'reparateurs' => $this->ModelMaintenances->get_reparateurs($this->session->userdata('code_wilaya'))
					 );
		

		$this->load->view('admin_panel/maintenances_reparations/all_maintenances_page',$data);
	}
    }else {
      $this->login_form();
    } */
	}


	public function all_reparations_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data = array(
						'reparations' => $this->ModelMaintenances->get_all_reparations($this->session->userdata('code_wilaya')) ,
						'vehicles' => $this->ModelVehicles->get_all_vehicles($this->session->userdata('code_wilaya')),
						// 'type_entretiens' => $this->ModelMaintenances->get_types_entretiens(),
						'reparateurs' => $this->ModelMaintenances->get_reparateurs($this->session->userdata('code_wilaya'))
					 );
		

		$this->load->view('admin_panel/maintenances_reparations/all_reparations_page',$data);
	}
    }else {
      $this->login_form();
    }
	}

	public function compagnies_assurances()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data = array(
						'compagnies' => $this->ModelTables->get_all_compagnie_assurance($this->session->userdata('code_wilaya')) 
					 );
		

		$this->load->view('admin_panel/table_management/compagnies_assurance',$data);
	}
    }else {
      $this->login_form();
    }
	}

	public function all_reparateurs_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data = array(
						'reparateurs' => $this->ModelTables->get_all_reparateurs($this->session->userdata('code_wilaya')) 
					 );
		

		$this->load->view('admin_panel/table_management/reparateurs',$data);
	}
    }else {
      $this->login_form();
    }
	}

	public function add_compagnie_assurance()
	{
			 $data = array(  
							 	 'nom' =>  $this->input->post('nom'),
					             'adresse' =>  $this->input->post('adresse'),
					             'telephone' =>  $this->input->post('telephone'),
					             'numero_police' =>  $this->input->post('numero_police'),
					             'code_wilaya' => $this->session->userdata('code_wilaya')
					         );
			 
			 $inserted_row = $this->ModelTables->add_compagnie_assurance($data);
			 if ($inserted_row) {
			 	$reply = array('message' => 'data inserted'  );
				echo json_encode($reply);
			 } else {
			 	$reply = array('message' => 'error'  );
				echo json_encode($reply);
			 }
			 
			}
			
	public function add_reparateur()
	{
			 $data = array(  
							 	 'nom' =>  $this->input->post('nom'),
					             'adresse' =>  $this->input->post('adresse'),
					             'telephone' =>  $this->input->post('telephone'),
					             'code_wilaya' => $this->session->userdata('code_wilaya')
					         );
			 
			 $inserted_row = $this->ModelTables->add_reparateur($data);
			 if ($inserted_row) {
			 	$reply = array('message' => 'data inserted'  );
				echo json_encode($reply);
			 } else {
			 	$reply = array('message' => 'error'  );
				echo json_encode($reply);
			 }
			 
			}


		public function all_accidents_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data = array(
						'accidents' => $this->ModelMaintenances->get_all_accidents($this->session->userdata('code_wilaya')) ,
						'vehicles' => $this->ModelVehicles->get_all_vehicles($this->session->userdata('code_wilaya')),
						// 'type_entretiens' => $this->ModelMaintenances->get_types_entretiens(),
						'chauffeurs' => $this->ModelDrivers->get_drivers($this->session->userdata('id_structure'))
					 );
		

		$this->load->view('admin_panel/accidents_management/all_accidents_page',$data);
	}
    }else {
      $this->login_form();
    }
	}
	public function add_vehicle_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data = array(
						'types' => $this->ModelStructureAffectation->get_all_types_structure() ,
						'exterior_parts' => $this->ModelTables->get_exterior_parts() ,
						'interior_parts' => $this->ModelTables->get_interior_parts() ,
						'structures' => $this->ModelStructureAffectation->get_all_structures(),
						'priorietaires' => $this->ModelStructureAffectation->get_all_priorietaires(),
						'marques' => $this->ModelVehicles->get_all_marques(),
						'compagnies' => $this->ModelTables->get_all_compagnie_assurance($this->session->userdata('code_wilaya'))
					 );
		

		$this->load->view('admin_panel/vehicle_management/add_vehicle_page',$data);
	}
    }else {
      $this->login_form();
    }
	}


		public function add_driver_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		

		$this->load->view('admin_panel/driver_management/add_driver_page');
	}
    }else {
      $this->login_form();
    }
	}	

	public function get_vehicle_info()
	{
		$data = array(
						'id_vehicle' => $this->input->post('id_vehicle')
					 );

		
		$all_vehicle_info['vehicle'] = $this->ModelVehicles->get_vehicle_info($data);
		$all_vehicle_info['assurance'] = $this->ModelVehicles->get_vehicle_assurance($data);
		$all_vehicle_info['affectation'] = $this->ModelVehicles->get_vehicle_affectation($data);
		$all_vehicle_info['exterior_status'] = $this->ModelVehicles->get_vehicle_exterior_status($data);
		$all_vehicle_info['interior_status'] = $this->ModelVehicles->get_vehicle_interior_status($data);
		$all_vehicle_info['drivers'] = $this->ModelVehicles->get_vehicle_driver_info($data);
		$all_vehicle_info['carte_naftal'] = $this->ModelVehicles->get_vehicle_carte_naftal($data);
		$reply = array('message' => 'vehicle exists' , 'all_info' =>  $all_vehicle_info );
				echo json_encode($reply);
		
	}	


		public function update_vehicle()
	{


			 $data = array(  
							 	 'marque' =>  $this->input->post('marque'),
					             'modele' =>  $this->input->post('modele'),
					             'motorisation' =>  $this->input->post('motorisation'),
					             'numero_immatriculation' =>  $this->input->post('numero_immatriculation'),
					             'etat' =>  $this->input->post('etat'),
					             'gps_tracker' =>  $this->input->post('gps_tracker'),
					             'numero_immatriculation_old' =>  $this->input->post('numero_immatriculation_old'),
					             'type_vehicle' =>  $this->input->post('type_vehicle'),
					             'numero_chassis' =>  $this->input->post('numero_chassis')
					         );
			 $id = $this->input->post('id_vehicle');
			 $this->ModelVehicles->update_vehicle($data,$id);
			 $reply = array('message' => 'vehicle updated'  );
				echo json_encode($reply);
			}

			public function update_carte_naftal()
	{


			 $data = array(  
							 	 'numero' =>  $this->input->post('numero'),
					             'debit' =>  $this->input->post('debit'),
					             'credit' =>  $this->input->post('credit'),
					             'date_expiration' =>  $this->input->post('date_expiration'),
					         );
			 $id = $this->input->post('id_vehicle');
			$id_updated_row = $this->ModelVehicles->update_carte_naftal($data,$id);
			if ($id_updated_row) {
				$reply = array('message' => 'carte updated'  );
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error'  );
				echo json_encode($reply);
			 
			}
			}
		public function update_status()
	{

			$exterior_status =  $this->input->post('exterior_status'); 
			$interior_status =  $this->input->post('interior_status'); 

			 $id = $this->input->post('id_vehicle');
			 $this->ModelVehicles->update_status($exterior_status,$interior_status,$id);
			 $reply = array('message' => 'status updated'  );
				echo json_encode($reply);
			}

	public function add_vehicle()
	{


			 $vehicle_info = array(  
							 	 'marque' =>  $this->input->post('marque'),
					             'modele' =>  $this->input->post('modele'),
					             'motorisation' =>  $this->input->post('motorisation'),
					             'numero_immatriculation' =>  $this->input->post('numero_immatriculation'),
					             'etat' =>  $this->input->post('etat'),
					             'gps_tracker' =>  $this->input->post('gps_tracker'),
					             'numero_immatriculation_old' =>  $this->input->post('numero_immatriculation_old'),
					             'type_vehicle' =>  $this->input->post('type_vehicle'),
					             'numero_chassis' =>  $this->input->post('numero_chassis'),
					             'code_wilaya' =>  $this->session->userdata('code_wilaya')
					         );


			

			$exterior_status =  $this->input->post('exterior_status'); 
			$interior_status =  $this->input->post('interior_status'); 
			

			$carte_naftal = array(
								'numero' => $this->input->post ('numero'),
								'debit' => $this->input->post ('debit'),
								'credit' => $this->input->post ('credit'),
								'date_expiration' => $this->input->post ('date_expiration'),
								'id_structure' => $this->session->userdata('id_structure'),
								'code_wilaya' => $this->session->userdata('code_wilaya')

							);



					       
			 $inserted_vehicle_id = $this->ModelVehicles->insert_vehicle($vehicle_info);
      		 $carte_naftal['id_vehicle'] = $inserted_vehicle_id;
			 $inserted_exterior_status = $this->ModelVehicles->insert_exterior_status($exterior_status,$inserted_vehicle_id);
		 	 $inserted_interior_status = $this->ModelVehicles->insert_interior_status($interior_status,$inserted_vehicle_id);
			 $inserted_carte_naftal = $this->ModelVehicles->insert_carte_naftal($carte_naftal);

			 	$inserted_rows = array(
			 		'inserted_vehicle_id' =>  $inserted_vehicle_id,
			 		 'inserted_interior_status' => $inserted_interior_status, 
			 		'inserted_exterior_status' => $inserted_exterior_status,
			 		'inserted_carte_naftal' => $inserted_carte_naftal
			 );
      			
        		

			// if($inserted_rows){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'ids' =>  $inserted_rows );
				echo json_encode($reply);
		//	} else echo json_encode("error");


	}

	public function add_inssurance()
	{


			 $data = array(  
							 	 'date_debut' =>  $this->input->post('date_debut'),
					             'date_fin' =>  $this->input->post('date_fin'),
					             'id_compagnie' =>  $this->input->post('id_compagnie'),
					             'id_vehicle' =>  $this->input->post('id_vehicle'),
					             
					       );
			 $inserted_row = $this->ModelStructureAffectation->insert_inssurance($data);
			 if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'id' =>  $inserted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}
		public function add_interior_part()
	{
			

			 $data = array(  
							 	 'part' =>  $this->input->post('part')
					             
					             
					       );
			 $inserted_row = $this->ModelTables->add_interior_part($data);
			 if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'id' =>  $inserted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function edit_interior_part()
	{
					             
			$id_composant =   $this->input->post('id_composant');
			 $data = array(  
							 	 'part' =>  $this->input->post('part')
					             
					             
					       );
			 $updated_row = $this->ModelTables->edit_interior_part($data,$id_composant);
			 if($updated_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data updated' , 'id' =>  $updated_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function delete_interior_part()
	{
			 $id =  $this->input->post('id_composant');
			 $deleted_row = $this->ModelTables->delete_interior_part($id);
			 if($deleted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data deleted' , 'id' =>  $deleted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function add_exterior_part()
	{
			

			 $data = array(  
							 	 'part' =>  $this->input->post('part')
					             
					             
					       );
			 $inserted_row = $this->ModelTables->add_exterior_part($data);
			 if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'id' =>  $inserted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function edit_exterior_part()
	{
					             
			$id_composant =   $this->input->post('id_composant');
			 $data = array(  
							 	 'part' =>  $this->input->post('part')
					             
					             
					       );
			 $updated_row = $this->ModelTables->edit_exterior_part($data,$id_composant);
			 if($updated_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data updated' , 'id' =>  $updated_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function delete_exterior_part()
	{
			 $id =  $this->input->post('id_composant');
			 $deleted_row = $this->ModelTables->delete_exterior_part($id);
			 if($deleted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data deleted' , 'id' =>  $deleted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

			public function add_type_entretien()
	{
			

			 $data = array(  
							 	 'nom' =>  $this->input->post('nom')
					             
					             
					       );
			 $inserted_row = $this->ModelTables->add_type_entretien($data);
			 if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'id' =>  $inserted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function edit_type_entretien()
	{
					             
			$id_type =   $this->input->post('id_type');
			 $data = array(  
							 	 'nom' =>  $this->input->post('nom')
					             
					             
					       );
			 $updated_row = $this->ModelTables->edit_type_entretien($data,$id_type);
			 if($updated_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data updated' , 'id' =>  $updated_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function delete_type_entretien()
	{
			 $id =  $this->input->post('id_type');
			 $deleted_row = $this->ModelTables->delete_type_entretien($id);
			 if($deleted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data deleted' , 'id' =>  $deleted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}
		public function add_user()
	{
			

			 $data = array(  
							 	 'username' =>  $this->input->post('username'),
					             'fullname' =>  $this->input->post('fullname'),
					             'group_name' =>  $this->input->post('group_name'),
					             'email' =>  $this->input->post('email'),
					             'id_structure' =>  $this->input->post('id_structure'),
					             'code_wilaya' =>  $this->input->post('code_wilaya'),
					             'password' =>  $this->input->post('password')
					             
					       );
			 $inserted_row = $this->ModelUsers->add_user($data);
			 if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'id' =>  $inserted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function edit_user()
	{
					             
			$id_user =   $this->input->post('id_user');
			 $data = array(  
							 	 'username' =>  $this->input->post('username'),
					             'fullname' =>  $this->input->post('fullname'),
					             'group_name' =>  $this->input->post('group_name'),
					             'email' =>  $this->input->post('email'),
					             'id_structure' =>  $this->input->post('id_structure'),
					             'code_wilaya' =>  $this->input->post('code_wilaya'),
					             'password' =>  $this->input->post('password')
					             
					       );
			 $updated_row = $this->ModelUsers->edit_user($data,$id_user);
			 if($updated_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data updated' , 'id' =>  $updated_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}



		public function add_driver()
	{


			 $data = array(  
							 	 'nom' =>  $this->input->post('nom'),
					             'prenom' =>  $this->input->post('prenom'),
					             'type_permis' =>  $this->input->post('type_permis'),
					             'validite_permis' =>  $this->input->post('validite_permis'),
					             'id_structure' =>  $this->session->userdata('id_structure')
					             
					       );
			 $inserted_row = $this->ModelDrivers->insert_driver($data);
			 if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted' , 'id' =>  $inserted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function edit_driver()
	{


			 $data = array(  
							 	 'nom' =>  $this->input->post('nom'),
					             'prenom' =>  $this->input->post('prenom'),
					             'type_permis' =>  $this->input->post('type_permis'),
					             'validite_permis' =>  $this->input->post('validite_permis')
					             
					             
					       );
			 $id =  $this->input->post('id');
			 $updated_row = $this->ModelDrivers->update_driver($data,$id);
			 if($updated_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data updated' , 'id' =>  $updated_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

	public function delete_driver()
	{
			 $id =  $this->input->post('id');
			 $deleted_row = $this->ModelDrivers->delete_driver($id);
			 if($deleted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data deleted' , 'id' =>  $deleted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}
	
	public function delete_vehicle()
	{
			 $id =  $this->input->post('ID');
			 $deleted_row = $this->ModelVehicles->delete_vehicle($id);
			 if($deleted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data deleted' , 'id' =>  $deleted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

		public function delete_user()
	{
			 $id =  $this->input->post('id');
			 $deleted_row = $this->ModelUsers->delete_user($id);

			 if($deleted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data deleted' , 'id' =>  $deleted_row );
				echo json_encode($reply);
			} else echo json_encode("error");


	}

		public function add_accident()
	{


			 $data = array(  
							 	 'accident_date' =>  $this->input->post('accident_date'),
					             'accident_time' =>  $this->input->post('accident_time'),
					             'id_chauffeur' =>  $this->input->post('id_driver'),
					             'id_vehicle' =>  $this->input->post('id_vehicle'),
					             'details' =>  $this->input->post('details'),
					             
					       );
			 $inserted_row = $this->ModelStructureAffectation->insert_accident($data);




      	if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted',
			 	 	'id_inserted_accident' => $inserted_row);
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error' );
				echo json_encode($reply);
			}


	}

		public function add_driver_vehicle()
	{


			 $data = array(  
							 	 'id_chauffeur' =>  $this->input->post('id_chauffeur'),
					             'type_chauffeur' =>  $this->input->post('type_chauffeur'),
					             'id_vehicle' =>  $this->input->post('id_vehicle'),
					             
					       );
			 $inserted_row = $this->ModelVehicles->add_driver_vehicle($data);




      	if($inserted_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted');
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error' );
				echo json_encode($reply);
			}


	}

		public function edit_password()
	{


			 $data['password'] = $this->input->post('password');
			 $username = $this->session->userdata('username');
			 $updated_row = $this->ModelUsers->edit_password($data,$username);


      	if($updated_row){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data updated');
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error' );
				echo json_encode($reply);
			}


	}


	public function all_vehicles()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

				$data = array(
						'types' => $this->ModelStructureAffectation->get_type_structure($this->session->userdata('id_structure')),
						'exterior_parts' => $this->ModelTables->get_exterior_parts() ,
						'interior_parts' => $this->ModelTables->get_interior_parts() ,
						'structures' => $this->ModelStructureAffectation->get_structure($this->session->userdata('id_structure'),$this->session->userdata('code_wilaya')),
						'priorietaires' => $this->ModelStructureAffectation->get_priorietaires($this->session->userdata('id_structure')),
						'chauffeurs' => $this->ModelDrivers->get_drivers($this->session->userdata('id_structure')),
						'compagnies' => $this->ModelTables->get_all_compagnie_assurance($this->session->userdata('code_wilaya'))
					 );
		$data['vehicles'] = $this->ModelVehicles->get_all_vehicles($this->session->userdata('code_wilaya'));
		$this->load->view('admin_panel/vehicle_management/all_vehicles',$data);
	}
    }else {
      $this->login_form();
    }
	}	

	public function all_drivers()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data['chauffeurs'] = $this->ModelDrivers->get_all_drivers($this->session->userdata('id_structure'));
		$this->load->view('admin_panel/driver_management/all_drivers',$data);
	}
    }else {
      $this->login_form();
    }
	}	
	public function table_entretiens()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data['type_entretiens'] = $this->ModelTables->get_type_entretien();
		$this->load->view('manager_panel/table_entretiens',$data);
	}
    }else {
      $this->login_form();
    }
	}

		public function composants_internes()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data['composants_internes'] = $this->ModelTables->get_interior_parts();
		$this->load->view('manager_panel/composants_internes',$data);
	}
    }else {
      $this->login_form();
    }
	}

			public function composants_externes()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data['composants_externes'] = $this->ModelTables->get_exterior_parts();
		$this->load->view('manager_panel/composants_externes',$data);
	}
    }else {
      $this->login_form();
    }
	}



	public function all_users_page()
	{
		if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data['users'] = $this->ModelUsers->get_all_users();
		$data['structures'] = $this->ModelStructureAffectation->get_all_structures();
		$data['types'] = $this->ModelStructureAffectation->get_all_types_structure();
		
		$this->load->view('manager_panel/all_users_page',$data);
	}
    }else {
      $this->login_form();
    }
	}

	// 	public function edit_vehicle_page()
	// 	{

	// 	$data = array(
	// 					'types' => $this->ModelStructureAffectation->get_all_types_structure(),
	// 					'structures' => $this->ModelStructureAffectation->get_all_structures(),
	// 					'priorietaires' => $this->ModelStructureAffectation->get_all_priorietaires()
	// 				 );
	// 	$data['vehicles'] = $this->ModelVehicles->get_all_vehicles();
	// 	$this->load->view('admin_panel/vehicle_management/edit_vehicle_page',$data);
	// }

			public function edit_vehicle_page()
		{
			if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

		$data = array(
						'types' => $this->ModelStructureAffectation->get_all_types_structure(),
						'structures' => $this->ModelStructureAffectation->get_all_structures(),
	 					'priorietaires' => $this->ModelStructureAffectation->get_all_priorietaires(),
						'exterior_parts' => $this->ModelTables->get_exterior_parts() ,
						'interior_parts' => $this->ModelTables->get_interior_parts() ,
						//'structures' => $this->ModelStructureAffectation->get_structure($this->session->userdata('id_structure'),$this->session->userdata('code_wilaya')),
						//'priorietaires' => $this->ModelStructureAffectation->get_priorietaires($this->session->userdata('id_structure')),
						'chauffeurs' => $this->ModelDrivers->get_drivers($this->session->userdata('id_structure')),
						'compagnies' => $this->ModelTables->get_all_compagnie_assurance($this->session->userdata('code_wilaya'))
					 );
		$data['vehicles'] = $this->ModelVehicles->get_all_vehicles($this->session->userdata('code_wilaya'));
		$this->load->view('admin_panel/vehicle_management/edit_vehicle_page',$data);
	}
    }else {
      $this->login_form();
    }
	}

	public function get_affectations()
	{
		$id_vehicle = $this->input->post('id_vehicle');
		$data['affectation_history'] = $this->ModelStructureAffectation->get_affectation_history($id_vehicle);
		//$this->load->view('admin_panel/vehicle_management/all_vehicles',$data);
		$data['table_name'] = 'affectations';
		echo json_encode($data);
	}	

	public function get_accidents()
	{
		$id_vehicle = $this->input->post('id_vehicle');
		$data['accidents_history'] = $this->ModelStructureAffectation->get_accident_history($id_vehicle);
		//$this->load->view('admin_panel/vehicle_management/all_vehicles',$data);
		$data['table_name'] = 'accidents';

		echo json_encode($data);
	}	

	public function get_assurances()
	{
		$id_vehicle = $this->input->post('id_vehicle');
		$data['assurance_history'] = $this->ModelStructureAffectation->get_assurance_history($id_vehicle);
		//$this->load->view('admin_panel/vehicle_management/all_vehicles',$data);
		$data['table_name'] = 'assurances';
		echo json_encode($data);
	}

	public function login(){

	if ($this->session->userdata('username')==NULL)
	{
    	
		    $data = array(
		      'username' => $this->input->post('username'),
		      'password' => $this->input->post('password')
		    );

		    if ($this->ModelUsers->user_exists($data)) {

		    	$user_id = $this->ModelUsers->check_user_password($data);
		      if ($user_id) {


		      	$user_data = $this->ModelUsers->get_user($user_id);
		        $this->session->set_userdata('username',$user_data['username']);
		        $this->session->set_userdata('code_wilaya',$user_data['code_wilaya']);
		        $this->session->set_userdata('id_structure',$user_data['id_structure']);
		        $this->session->set_userdata('group_name',$user_data['group_name']);
		        $this->session->set_userdata('user_rights',$user_data['user_rights']);
		        $this->session->set_userdata('last_time',time());

		        
				$group_name = $this->session->userdata('group_name');
				switch ($group_name) {
					case 'CHEF_PARC':
					$stats['username'] = $data['username'];
		            $stats['number_drivers'] = $this->ModelDrivers->get_number_drivers($this->session->userdata('id_structure'));
		            $stats['number_vehicles'] = $this->ModelVehicles->get_number_vehicles($this->session->userdata('code_wilaya'));
		        	$this->load->view('admin_panel/accueil',$stats);
					break;
					case 'cc':
					$stats['username'] = $data['username'];
					$stats['number_users'] = $this->ModelUsers->get_agency_users($this->session->userdata('code_wilaya'));
					$this->load->view('cc_panel/accueil',$stats);
					break;
					case 'manager':
					$stats['username'] = $data['username'];
		        	$data['number_users'] = $this->ModelUsers->get_number_users();
        			$this->load->view("manager_panel/accueil",$data);
					break;
					case 'responsable_patrimoine':
					$stats['username'] = $data['username'];
		            $stats['number_drivers'] = $this->ModelDrivers->get_number_drivers($this->session->userdata('id_structure'));
		            $stats['number_vehicles'] = $this->ModelVehicles->get_number_vehicles($this->session->userdata('code_wilaya'));
		        	$this->load->view('admin_panel/accueil',$stats);
					break;
					case 'superviseur':
						# code...
					break;
					}
		    }else {
		        $data = array(
							'error_message' => 'Mot de passe incorrect!'
							);
				  $this->load->view('authentication/login_page', $data);
		      }
		    }

		    else {
		     // echo json_encode("username not found");
		      $data = array(
							'error_message' => 'Utilisateur non trouvé!'
							);
				  $this->load->view('authentication/login_page', $data);
				}
		    
	} else   $this->load->view('authentication/login_page');
	        

  }


public function account_settings()
{
	if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

	$group_name = $this->session->userdata('group_name');
	if ($group_name == 'CHEF_PARC') {
		$this->load->view("authentication/account_settings");
	} else if ($group_name == 'manager') {
		$this->load->view("manager_panel/account_settings");
		
	}
}
    }else {
      $this->login_form();
    }
	
}

public function vehicle_details()
{	
	if ($this->session->userdata('username')!=NULL) {

      if ((time()-$this->session->userdata('last_time'))>1470) { // logout if session expires after 24 min
        $this->logout();
      }else {
        $this->session->set_userdata('last_time',time());

	$this->load->view("print/vehicle_details");
}
    }else {
      $this->login_form();
    }
}
public function wake_up()
{
	$this->session->set_userdata('last_time',time());
}


  public function logout()
  {
      $this->session->sess_destroy();
   //   redirect('login_form','refresh');
   $this->load->view('authentication/login_page','refresh');
  }

	public function login_form()
	{

		$this->load->view('authentication/login_page');
		
	}

	public function lock_session()
	{
	$this->logout();
		//$this->load->view('authentication/login_form');
	}


	public function unlock_session()
	{
		
	  if ($this->session->userdata('username')!=NULL) {
	  	 $data = array(
		      'username' => $this->session->userdata('username'),
		      'password' => $this->input->post('password')
		    );
	  	 
	  	if ($this->ModelUsers->check_user_password($data)) {

		        

		        $this->session->set_userdata('username',$data['username']);
		        $this->session->set_userdata('last_time',time());
		        $this->load->view('admin_panel/accueil',$data);

		      }else {
		      	
		        $data = array(
							'error_message' => 'Mot de passe incorrect!'
							);
				  $this->load->view('authentication/lock_screen', $data);
		      }
		    }
      
        
      }


      public function add_affectation()
      {
      	$data['etat'] = $this->input->post('etat');
      //	$data['id_structure'] = $this->session->userdata('id_structure');
      	$data['id_structure'] = $this->input->post('id_structure');
      	$data['id_priorietaire'] = $this->input->post('id_priorietaire');
      	$data['numero_decision'] = $this->input->post('numero_decision');
      //	$data['kilometrage'] = $this->input->post('kilometrage');
      	$data['id_vehicle'] = $this->input->post('id_vehicle');
      	$data['type_affectation'] = $this->input->post('type_affectation');
      	$id_inserted_affectation = $this->ModelStructureAffectation->insert_affectation($data);



      	if($id_inserted_affectation){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted',
			 	 	'id_inserted_affectation' => $id_inserted_affectation);
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error' );
				echo json_encode($reply);
			}




      }  

          public function add_maintenance()
      {
      	$data['entretien'] = $this->input->post('type_entretien');
      	$data['date_entretien'] = $this->input->post('date_entretien');
      	$data['kilometrage'] = $this->input->post('kilometrage');
      	$data['id_vehicle'] = $this->input->post('id_vehicle');
      	$data['cout'] = $this->input->post('cout');
      	$data['id_reparateur'] = $this->input->post('id_reparateur');
      	$id_inserted = $this->ModelMaintenances->add_maintenance($data);



      	if($id_inserted){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted',
			 	 	'id_inserted' => $id_inserted);
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error' );
				echo json_encode($reply);
			}




      }

       public function add_reparation()
      {
      	$data['reparations'] = $this->input->post('reparations');
      	$data['date_reparation'] = $this->input->post('date_reparation');
      	// $data['kilometrage'] = $this->input->post('kilometrage');
      	$data['id_vehicle'] = $this->input->post('id_vehicle');
      	$data['cout'] = $this->input->post('cout');
      	$data['id_reparateur'] = $this->input->post('id_reparateur');
      	$id_inserted = $this->ModelMaintenances->add_reparation($data);



      	if($id_inserted){
		//	echo json_encode($data);
			 	 $reply = array('message' => 'data inserted',
			 	 	'id_inserted' => $id_inserted);
				echo json_encode($reply);
			} else {
				$reply = array('message' => 'error' );
				echo json_encode($reply);
			}




      }


   public function upload_decision()
   {
   	$username = $this->session->userdata('username');

    //$this->load->helper(array('path'));
    //$this->load->file('plugins/uploader/class.uploader.php');
    $uploader = new Uploader();
    $data = $uploader->upload($_FILES['files'], array(
        'limit' => 1, //Maximum Limit of files. {null, Number}
        'maxSize' => 0, //Maximum Size of files {null, Number(in MB's)}
        'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
        'required' => true, //Minimum one file is required for upload {Boolean}
        'uploadDir' => 'uploads/'.$username.'/FICHIERS/DECISIONS/', //Upload directory {String}
        'title' => array('auto', 10), //New file name {null, String, Array} *please read documentation in README.md
        'removeFiles' => false, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
        'replace' => false, //Replace the file if it already exists  {Boolean}
        'perms' => null, //Uploaded file permisions {null, Number}
        'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
        'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
        'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
        'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
        'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
        'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
    ));

    if($data['isComplete']){
        $info = $data['data'];
        // $data['sender'] = $this->input->post('sender');
        // $data['compose_textarea'] = $this->input->post('compose_textarea');
        // $data['users_ids'] = $this->input->post('users_ids');
        // $data['users'] = $this->input->post('users');
        // $data['compose_subject'] = $this->input->post('compose_subject');
        // $data['compose_stickers'] = $this->input->post('compose_stickers');
         $id_inserted_affectation =  $this->input->post('id_inserted_affectation');

        //  echo '<pre>';
        //  echo print_r($info["metas"]);
        //  echo print_r($username);
        //  echo print_r($id_of_msg);
        //  echo '</pre>';
        $files_data = array( 
             'file' => $info["metas"][0]['file'],
             'id_inserted_affectation' => $id_inserted_affectation
             
           );

      $this->ModelStructureAffectation->update_files($files_data);

    echo '<pre>';
     echo print_r($info["metas"][0]);
    //  $info["metas"]['name'] = iconv(mb_detect_encoding($info["metas"]['name'], mb_detect_order(), true), "UTF-8", $info["metas"]['name']);

      echo print_r($info["metas"]);

    echo '</pre>';
    //  echo json_encode($id_of_msg);
    }

    if($data['hasErrors']){
        $errors = $data['errors'];
        print_r($errors);
    }
   }  

    public function upload_accident()
   {
   	$username = $this->session->userdata('username');

    //$this->load->helper(array('path'));
    //$this->load->file('plugins/uploader/class.uploader.php');
    $uploader = new Uploader();
    $data = $uploader->upload($_FILES['files'], array(
        'limit' => 1, //Maximum Limit of files. {null, Number}
        'maxSize' => 0, //Maximum Size of files {null, Number(in MB's)}
        'extensions' => null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
        'required' => true, //Minimum one file is required for upload {Boolean}
        'uploadDir' => 'uploads/'.$username.'/FICHIERS/accidents/', //Upload directory {String}
        'title' => array('auto', 10), //New file name {null, String, Array} *please read documentation in README.md
        'removeFiles' => false, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
        'replace' => false, //Replace the file if it already exists  {Boolean}
        'perms' => null, //Uploaded file permisions {null, Number}
        'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
        'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
        'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
        'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
        'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
        'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
    ));

    if($data['isComplete']){
        $info = $data['data'];
        // $data['sender'] = $this->input->post('sender');
        // $data['compose_textarea'] = $this->input->post('compose_textarea');
        // $data['users_ids'] = $this->input->post('users_ids');
        // $data['users'] = $this->input->post('users');
        // $data['compose_subject'] = $this->input->post('compose_subject');
        // $data['compose_stickers'] = $this->input->post('compose_stickers');
         $id_inserted_accident =  $this->input->post('id_inserted_accident');

        //  echo '<pre>';
        //  echo print_r($info["metas"]);
        //  echo print_r($username);
        //  echo print_r($id_of_msg);
        //  echo '</pre>';
        $files_data = array( 
             'file' => $info["metas"][0]['file'],
             'id_inserted_accident' => $id_inserted_accident
             
           );

      $this->ModelStructureAffectation->update_accident($files_data);

    echo '<pre>';
     echo print_r($info["metas"][0]);
    //  $info["metas"]['name'] = iconv(mb_detect_encoding($info["metas"]['name'], mb_detect_order(), true), "UTF-8", $info["metas"]['name']);

      echo print_r($info["metas"]);

    echo '</pre>';
    //  echo json_encode($id_of_msg);
    }

    if($data['hasErrors']){
        $errors = $data['errors'];
        print_r($errors);
    }
   }
    

	public function error_()
	{
		$this->load->view('errors/html/error_404');
	}
}
