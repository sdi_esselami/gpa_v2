<?php

class ModelDrivers extends CI_Model {

    function __construct()
    {
      parent::__construct();
      
    }
    public function num($value='')
    {
      $query = $this->db->query('SELECT * FROM ');

      return $query->num_rows();
    }

    



    public function get_drivers($id_structure){
        
        $query = $this->db->query("select id,
                                          nom,
                                          prenom,
                                          type_permis,
                                          validite_permis
                                   from   chauffeur
                                   where  id_structure=' $id_structure'");
      
      return $query->result_array();
    }   




    
    public function add_user($data)
    {
      $this->db->insert('users',$data);
      return $this->db->affected_rows();
    }   


     public function insert_driver($data)
    {
      $this->db->insert('chauffeur',$data);
      return $this->db->affected_rows();
    } 

     public function update_driver($data,$id)
    {
     // $this->db->set_where('chauffeur',$data);



      $this->db->where('id', $id);
      $this->db->update('chauffeur',$data);

      return $this->db->affected_rows();
    }


        public function get_all_drivers($id_structure){
        $query = $this->db->get_where('chauffeur',array('id_structure' => $id_structure));
        return $query->result_array();
    }

    public function get_number_drivers($id_structure){
        $query = $this->db->get_where('chauffeur',array('id_structure' => $id_structure));
        return $query->num_rows();
    }



    public function delete_driver($id)
    {  

      $this->db->where('id_chauffeur', $id);
      $this->db->delete('historique_vehicle_chauffeur');

      $this->db->where('id', $id);
      $this->db->delete('chauffeur');

      return $this->db->affected_rows();
    }


}
