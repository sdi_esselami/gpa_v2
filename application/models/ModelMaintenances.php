<?php

class ModelMaintenances extends CI_Model {

    function __construct()
    {
      parent::__construct();
      
    }
    


       public function add_maintenance($data)
    {
      $this->db->insert('historique_entretien',$data);
      return $this->db->insert_id();
    }

    public function add_reparation($data)
    {
      $this->db->insert('historique_reparation',$data);
      return $this->db->insert_id();
    }




public function get_vehicle_info($data){
     
      $query = $this->db->get_where('vehicle', array('id' => $data['id_vehicle']));
      
      return $query->row_array();
    }

    public function get_all_marques(){
     
      $query = $this->db->get('vehicle_brands');
      
      return $query->result_array();
    }

  public function get_all_maintenances($code_wilaya){


      
      $query = $this->db->query("select v.marque,v.modele, v.numero_immatriculation,e.kilometrage,
                                        e.entretien,e.date_entretien,e.cout, r.nom as reparateur
                                from vehicle v, historique_entretien e, reparateur r
                                where e.id_vehicle = v.ID and e.id_reparateur = r.id
                                and v.code_wilaya = '$code_wilaya'
                                order by e.date_system ASC"

                                );


      return $query->result_array();
    } 
  public function get_all_accidents($code_wilaya){


      
      $query = $this->db->query("select  v.marque,
                                         v.modele,
                                         v.numero_immatriculation,
                                         c.accident_date,
                                         c.accident_time,
                                         c.details as accident,
                                         c.constat_accident,
                                         concat(f.nom,' ',f.prenom) AS chauffeur
                                   from  vehicle v,
                                         historique_accident c,
                                         chauffeur f
                                   where c.id_vehicle = v.ID AND c.id_chauffeur = f.id AND v.code_wilaya = '$code_wilaya'");


      return $query->result_array();
    }

  public function get_all_reparations($code_wilaya){


      
      $query = $this->db->query("select v.marque,v.modele, v.numero_immatriculation,
                                        e.reparations,e.date_reparation,e.cout, r.nom as reparateur
                                from vehicle v, historique_reparation e, reparateur r
                                where e.id_vehicle = v.ID and e.id_reparateur = r.id
                                and v.code_wilaya = '$code_wilaya'
                                order by e.date_system ASC"

                                );


      return $query->result_array();
    } 
  public function get_reparateurs($code_wilaya){


      
      $query = $this->db->get_where('reparateur', array('code_wilaya' => $code_wilaya));
      

      return $query->result_array();
    } 


    public function get_types_entretiens(){

        $query = $this->db->get('type_entretien');
      
      return $query->result_array();
    }

   
    



    
    
}
