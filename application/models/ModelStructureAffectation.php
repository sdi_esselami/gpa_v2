<?php

class ModelStructureAffectation extends CI_Model {

    function __construct()
    {
      parent::__construct();
      
    }
    
    public function insert_type_structure($data)
    {
      $this->db->insert('type_structure',$data);
      return $this->db->insert_id();
    } 

      public function insert_affectation($data)
    {
      $this->db->insert('historique_affectation',$data);
      return $this->db->insert_id();
    }


  public function insert_inssurance($data)
    {
      $this->db->insert('historique_assurance',$data);
      return $this->db->insert_id();
    } 

     public function insert_accident($data)
    {
      $this->db->insert('historique_accident',$data);
      return $this->db->insert_id();
    }

    public function update_files($data)
    {
         $file = $data["file"];
        $id_inserted_affectation = $data["id_inserted_affectation"];
      // $query = $this->db->query("update historique_affectation set document_decision = '$file' where id = '$id_inserted_affectation'");

      


      $this->db->set('document_decision', $file);
      $this->db->where('id', $id_inserted_affectation);
      $this->db->update('historique_affectation');

        return $this->db->affected_rows();
    }  

      public function update_accident($data)
    {
         $file = $data["file"];
        $id_inserted_accident = $data["id_inserted_accident"];
      // $query = $this->db->query("update historique_affectation set document_decision = '$file' where id = '$id_inserted_affectation'");

      


      $this->db->set('constat_accident', $file);
      $this->db->where('id', $id_inserted_accident);
      $this->db->update('historique_accident');

        return $this->db->affected_rows();
    }



    public function get_all_types_structure()
    {
        

       $query = $this->db->query('select id,nom from type_structure');
       return $query->result_array();
        
    } 
    public function get_all_structures()
    {
        

      $query = $this->db->query('select s.code_wilaya as code_wilaya, s.id as id, s.nom as nom ,t.nom as type from structure_affectation s , type_structure t where t.id = s.id_type_structure');
     return $query->result_array();
        
    }  


     public function get_all_priorietaires()
    {
        

      $query = $this->db->query('select p.id as id, p.nom as nom ,t.nom as type from priorietaire p , type_structure t where t.id = p.id_type_structure');
     return $query->result_array();
        
    }  


    public function get_type_structure($id_structure)
    {
        

       $query = $this->db->query("select t.id, t.nom from structure_affectation s , type_structure t
        where t.id = s.id_type_structure and s.id='$id_structure'");
       return $query->result_array();
        
    } 
    public function get_structure($code_wilaya,$id_structure)
    {
        

      $query = $this->db->query("select s.code_wilaya as code_wilaya, s.id as id, s.nom as nom ,t.nom as type from structure_affectation s , type_structure t where t.id = s.id_type_structure and s.id='$id_structure'");
     return $query->result_array();
        
    }  


     public function get_priorietaires($id_structure)
    {
        

      $query = $this->db->query("select p.id as id, p.nom as nom ,t.nom as type from priorietaire p , 
       structure_affectation s , type_structure t where t.id = p.id_type_structure and t.id = s.id_type_structure and s.id='$id_structure'");
     return $query->result_array();
        
    } 
      public function get_affectation_history($id_vehicle)
    {
        

      $query = $this->db->query("
       select h.date_affectation ,
              h.type_affectation ,
              h.document_decision ,
              h.numero_decision ,
              h.etat ,
              h.kilometrage ,
              s.nom as structure , 
              p.nom as priorietaire
       from historique_affectation h , structure_affectation s , priorietaire p
       where h.id_priorietaire = p.id and h.id_structure = s.Id and h.id_vehicle = '$id_vehicle'");
     return $query->result_array();
        
    }  

     public function get_accident_history($id_vehicle)
    {
        

      $query = $this->db->query("select  h.accident_date , h.accident_time , 
                                         h.constat_accident, concat(f.nom,' ',f.prenom) as Chauffeur,h.details
                                from     historique_accident h, chauffeur f
                                where    h.id_vehicle ='$id_vehicle'
                                and      f.id = h.id_chauffeur");
     return $query->result_array();
        
    }  

     public function get_assurance_history($id_vehicle)
    {
        

      $query = $this->db->query("select c.nom,
                                        h.date_debut,
                                        h.date_fin
                                 from historique_assurance h , compagnie_assurance c 
                                 where c.id = h.id_compagnie
                                 and id_vehicle = '$id_vehicle'");
     return $query->result_array();
        
    }  

    

    public function user_exists($data){
      // verification Email Only .....(YES)
      $query = $this->db->get_where('users', array('username' => $data['username']));
      // We Must Close The Connection To the Database ***
      return $query->row_array();
    }


    public function get_all_users(){
        $query = $this->db->get('users');
        return $query->result_array();
    }

    public function get_user($id)
    {
        $query = $this->db->get_where('users', array('user_id' => $id));
        // We Must Close The Connection To the Database ***
        return $query->row_array();
    }

    public function add_user($data)
    {
      $this->db->insert('users',$data);
      return $this->db->affected_rows();
    }

    public function add_userXX($data)
    {  //  $data->fullname; ...etc.
      $sql = "INSERT INTO users (*) VALUES (null)";
      $this->db->query($sql);
      return $this->db->affected_rows();
    }

    public function remove_user($idUser)
    {  //  $data->fullname; ...etc.
      $sql = "INSERT INTO users (*) VALUES ()";
      $this->db->query($sql);
      return $this->db->affected_rows();
    }

    public function update_user($idUser,$data)
    {  //  $data->fullname; ...etc.
      $sql = "INSERT INTO users (*) VALUES ()";
      $this->db->query($sql);
      return $this->db->affected_rows();
    }
}
