<?php

class ModelTables extends CI_Model {

    function __construct()
    {
      parent::__construct();
      
    }
    
    public function add_compagnie_assurance($data)
    {
      $this->db->insert('compagnie_assurance',$data);
      return $this->db->affected_rows();
    }

    public function add_type_entretien($data)
    {
      $this->db->insert('type_entretien',$data);
      return $this->db->affected_rows();
    }

     public function edit_type_entretien($data,$id_type)
    {

      $this->db->where('Id', $id_type);
      $this->db->update('type_entretien',$data);

        return $this->db->affected_rows();
    } 

     public function delete_type_entretien($id)
    {  

      

      $this->db->where('Id', $id);
      $this->db->delete('type_entretien');

      return $this->db->affected_rows();
    }


       public function add_interior_part($data)
    {
      $this->db->insert('vehicle_interior_parts',$data);
      return $this->db->affected_rows();
    }

     public function edit_interior_part($data,$id_type)
    {

      $this->db->where('id', $id_type);
      $this->db->update('vehicle_interior_parts',$data);

        return $this->db->affected_rows();
    } 

     public function delete_interior_part($id)
    {  

      

      $this->db->where('id', $id);
      $this->db->delete('vehicle_interior_parts');

      return $this->db->affected_rows();
    }


          public function add_exterior_part($data)
    {
      $this->db->insert('vehicle_exterior_parts',$data);
      return $this->db->affected_rows();
    }

     public function edit_exterior_part($data,$id_type)
    {

      $this->db->where('id', $id_type);
      $this->db->update('vehicle_exterior_parts',$data);

        return $this->db->affected_rows();
    } 

     public function delete_exterior_part($id)
    {  

      

      $this->db->where('id', $id);
      $this->db->delete('vehicle_exterior_parts');

      return $this->db->affected_rows();
    }
     public function get_all_compagnie_assurance($code_wilaya){
        $query = $this->db->get_where('compagnie_assurance', array('code_wilaya' => $code_wilaya));
        return $query->result_array();
    }


    public function add_reparateur($data)
    {
      $this->db->insert('reparateur',$data);
      return $this->db->affected_rows();
    }

     public function get_all_reparateurs($code_wilaya){
        $query = $this->db->get_where('reparateur', array('code_wilaya' => $code_wilaya));
        return $query->result_array();
    }

    public function get_exterior_parts(){
     
      $query = $this->db->get('vehicle_exterior_parts');
      
      return $query->result_array();
    }

        public function get_interior_parts(){
     
      $query = $this->db->get('vehicle_interior_parts');
      
      return $query->result_array();
    } 

    public function get_type_entretien(){
     
      $query = $this->db->get('type_entretien');
      
      return $query->result_array();
    }
    

    
}
