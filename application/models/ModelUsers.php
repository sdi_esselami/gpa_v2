<?php

class ModelUsers extends CI_Model {

    function __construct()
    {
      parent::__construct();
      
    }
    public function get_number_users()
    {
      $query = $this->db->get('users');

      return $query->num_rows();
    }

    public function get_agency_users($data)
    {
      $query = $this->db->get('users');
      $query = $this->db->get_where('users', array('code_wilaya' => $data['code_wilaya']));
      return $query->num_rows();
    }

    public function check_user_password($data)
    {
        


        $query = $this->db->get_where('users', array('username' => $data['username']));
        $hash = $query->row('password');
        // if (password_verify($data['password'],$hash)) {
        if ($data['password'] == $hash) {
          
          return $query->row('ID');
        } else return false;
        
    }

    public function user_exists($data){
      // verification Email Only .....(YES)
      $query = $this->db->get_where('users', array('username' => $data['username']));
      // We Must Close The Connection To the Database ***
      return $query->row_array();
    }

    public function get_all_users(){
        $query = $this->db->query('select t.nom as type_structure,u.id ,u.username, u.fullname, u.email, u.groupid, s.id as id_structure ,s.nom as affectation , s.code_wilaya from users u , structure_affectation s , type_structure t where s.id = u.id_structure and t.id = s.id_type_structure');
        return $query->result_array();
    }

    public function get_user($id)
    {
       // $query = $this->db->get_where('users', array('ID' => $id));
        $query = $this->db->query("select  u.ID,
                                           u.username,
                                           u.fullname,
                                           u.email,
                                           u.groupid,
                                           u.active,
                                           u.id_structure,
                                           u.code_wilaya,
                                           u.user_rights,
                                           g.group_name,
                                           g.group_rights
                                  from users u,usergroups g
                                  where u.ID = '$id' 
                                  and u.groupid = g.id
                                   ");


        
        // We Must Close The Connection To the Database ***
        return $query->row_array();
    }

    public function add_user($data)
    {
      $this->db->insert('users',$data);
      return $this->db->affected_rows();
    }

     public function edit_user($data,$id_user)
    {

      $this->db->where('id', $id_user);
      $this->db->update('users',$data);

        return $this->db->affected_rows();
    } 


 public function delete_user($id)
    {  

      

      $this->db->where('id', $id);
      $this->db->delete('users');

      return $this->db->affected_rows();
    }

    public function edit_password($data,$username)
    {  //  $data->fullname; ...etc.
     $this->db->where('username', $username);
      $this->db->update('users',$data);

        return $this->db->affected_rows();
    }
}
