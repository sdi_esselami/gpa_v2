<?php

class ModelVehicles extends CI_Model {

    function __construct()
    {
      parent::__construct();
      
    }
    
    public function insert_vehicle($vehicle_info) 
    {
      $this->db->insert('vehicle',$vehicle_info);
      $inserted_vehicle_id = $this->db->insert_id();
      

      return $inserted_vehicle_id;
        
    }


        public function insert_exterior_status($exterior_status,$id_vehicle)
    {
      $exterior_status=explode(',', $exterior_status);
      foreach ($exterior_status as $status ) {
        $status = explode('_', $status);

       $this->db->insert('vehicle_exterior_status',array('id_part' => $status[0],'status' => $status[1],'id_vehicle' => $id_vehicle));
      }
      
      return  $this->db->affected_rows();
    }

     public function insert_interior_status($interior_status,$id_vehicle)
    {
      $interior_status=explode(',', $interior_status);
      foreach ($interior_status as $status ) {
        $status = explode('_', $status);

       $this->db->insert('vehicle_interior_status',array('id_part' => $status[0],'status' => $status[1],'id_vehicle' => $id_vehicle));
      }
      
      return  $this->db->affected_rows();
    }
       
    public function insert_carte_naftal($carte_naftal)
    {
      $this->db->insert('carte_naftal',$carte_naftal);
      return $this->db->insert_id();
    }


    public function add_driver_vehicle($data)
    {
      $this->db->insert('historique_vehicle_chauffeur',$data);
      return $this->db->insert_id();
    } 

       public function add_maintenance($data)
    {
      $this->db->insert('historique_entretien',$data);
      return $this->db->insert_id();
    }




    public function user_exists($data){
      // verification Email Only .....(YES)
      $query = $this->db->get_where('users', array('username' => $data['username']));
      // We Must Close The Connection To the Database ***
      return $query->row_array();
    }


public function get_vehicle_info($data){
     
      $query = $this->db->get_where('vehicle', array('id' => $data['id_vehicle']));
      
      return $query->row_array();
    }

    public function get_all_marques(){
     
      $query = $this->db->get('vehicle_brands');
      
      return $query->result_array();
    }

    
  public function get_vehicle_assurance($data){


      $id_vehicle = $data['id_vehicle'];
      $query = $this->db->query("
       select  a.nom,a.numero_police,a.adresse,a.telephone,a.id,a.code_wilaya ,h.date_debut,h.date_fin
       from historique_assurance h,compagnie_assurance a
       where id_vehicle = '$id_vehicle'
       and h.id_compagnie = a.id
       and str_to_date(h.date_fin,'%d/%m/%Y') = (select MAX(str_to_date(h.date_fin,'%d/%m/%Y'))
                               from historique_assurance 
                               where id_vehicle = '$id_vehicle')
                               ");


      return $query->row_array();
    } 
     public function get_vehicle_affectation($data){

      
  $id_vehicle = $data['id_vehicle'];
    $query = $this->db->query("
       select h.date_affectation ,
              h.type_affectation ,
              h.document_decision ,
              h.numero_decision ,
              h.etat ,
              h.kilometrage ,
              s.nom as structure_affectation , 
              s.id as id_structure_affectation , 
              s.code_wilaya , 
              p.nom as detail_affectation,
              p.id as id_priorietaire,
              t.nom as type_structure
       from historique_affectation h , structure_affectation s , priorietaire p, type_structure t
       where h.id_priorietaire = p.id and h.id_structure = s.Id and s.id_type_structure = t.Id
        and h.id_vehicle = '$id_vehicle'
       and date_affectation = (select max(date_affectation)
                               from historique_affectation 
                               where id_vehicle = '$id_vehicle')");


      return $query->row_array();
    }

    public function get_vehicle_exterior_status($data){
     
      $query = $this->db->get_where('vehicle_exterior_status', array('id_vehicle' => $data['id_vehicle']));
      
      return $query->result_array();
    } 

    public function get_vehicle_interior_status($data){
     
      $query = $this->db->get_where('vehicle_interior_status', array('id_vehicle' => $data['id_vehicle']));
      
      return $query->result_array();
    }

    public function get_number_vehicles($code_wilaya){
        $query = $this->db->get_where('vehicle',array('code_wilaya' => $code_wilaya));
        return $query->num_rows();
    }


    public function get_vehicle_driver_info($data){
     
      $id_vehicle = $data['id_vehicle'];
      $query = $this->db->query("
       select c.nom,
              c.prenom,
              c.id,
              c.type_permis,
              c.validite_permis,
              h.type_chauffeur 
       from   historique_vehicle_chauffeur h, chauffeur c
       where  h.id_vehicle = '$id_vehicle'
              and h.id_chauffeur = c.id 
              and date_affectation = (select max(date_affectation)
                               from historique_vehicle_chauffeur 
                               where id_vehicle = '$id_vehicle')");
      
      return $query->row_array();
    }   

     public function get_vehicle_carte_naftal($data){
     
      $id_vehicle = $data['id_vehicle'];
      $query = $this->db->get_where('carte_naftal', array('id_vehicle' => $id_vehicle));

      
      return $query->row_array();
    }



    public function get_all_vehicles($code_wilaya){
       $query = $this->db->get_where('vehicle', array('code_wilaya' => $code_wilaya));
       // $query =  $this->db->query("SELECT * FROM vehicle");
        return $query->result_array();
    }


    public function delete_vehicle($id)
    {  

      $this->db->where('ID', $id);
      $this->db->delete('vehicle');

      /* $this->db->where('id', $id);
      $this->db->delete('chauffeur'); */

      return $this->db->affected_rows();
    }


    public function get_user($id)
    {
        $query = $this->db->get_where('users', array('user_id' => $id));
        // We Must Close The Connection To the Database ***
        return $query->row_array();
    }

    public function add_user($data)
    {
      $this->db->insert('users',$data);
      return $this->db->affected_rows();
    }

    public function add_userXX($data)
    {  //  $data->fullname; ...etc.
      $sql = "INSERT INTO users (*) VALUES (null)";
      $this->db->query($sql);
      return $this->db->affected_rows();
    }

    public function remove_user($idUser)
    {  //  $data->fullname; ...etc.
      $sql = "INSERT INTO users (*) VALUES ()";
      $this->db->query($sql);
      return $this->db->affected_rows();
    }

    public function update_user($idUser,$data)
    {  //  $data->fullname; ...etc.
      $sql = "INSERT INTO users (*) VALUES ()";
      $this->db->query($sql);
      return $this->db->affected_rows();
    }



        public function update_vehicle($data,$id)
    {

      $this->db->where('id', $id);
      $this->db->update('vehicle',$data);

        return $this->db->affected_rows();
    } 

  public function update_carte_naftal($data,$id_vehicle)
    {

      $this->db->where('id_vehicle', $id_vehicle);
      $this->db->update('carte_naftal',$data);

        return $this->db->affected_rows();
    } 



    public function update_status($exterior_status,$interior_status,$id)
    {
      $exterior_status=explode(',', $exterior_status);
      foreach ($exterior_status as $status ) {
        $status = explode('_', $status);
      $this->db->where('id_vehicle', $id);
      $this->db->where('id_part', $status[0]);
      $this->db->update('vehicle_exterior_status',array('status' => $status[1]));
      $exterior = $this->db->affected_rows();
      } 

      $interior_status=explode(',', $interior_status);
      foreach ($interior_status as $status ) {
        $status = explode('_', $status);
      $this->db->where('id_vehicle', $id);
      $this->db->where('id_part', $status[0]);
      $this->db->update('vehicle_interior_status',array('status' => $status[1]));
      $interior = $this->db->affected_rows();
      }
        return ($exterior && $interior);
      
    } 

}
