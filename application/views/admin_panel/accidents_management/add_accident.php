<div class=" col-md-12 animated" id="accident_panel" style="display: none;">
  <div class="panel " style="background-color: #BEBEBE;">
    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
    Les accidents  </a>
    <div class="pull-right">
      <a href="javascript;" data-perform="panel-collapse">
      <i class="ti-plus"></i></a>
    </div>
  </div>
  <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;" id="accident_collapse">
    <div class="row">
      
      <!--              <div class="col-md-4">
        <h5 class="m-t-30 m-b-10"><b>NOM</b></h5>
        <select class="selectpicker show-tick " data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="structure_affectation" data-hide-disabled= 'true' onchange="get_wilaya()">
          <?php foreach ($types as $type)
          {
          echo '<optgroup disabled = "true" label="' .$type['nom']. '">';
            foreach ($structures as $structure) {
            if ($structure['type'] == $type['nom'] ) {
            echo '<option value ="' .$structure['id']. '" code_wilaya ="' .$structure['code_wilaya']. '" >'. $structure['nom'] . '</option>';
            }
            }
          } echo '</optgroup>'; ?>
        </select>
        <script type="text/javascript">
        function show_structure(type) {
        $('#code_wilaya').val('');
        $('.selectpicker').find('[label]').attr('disabled',true);
        $('.selectpicker').find("[label='"+type+"']").attr('disabled',false);
        $('.selectpicker').selectpicker('refresh');}
        function get_wilaya() {
        $('#code_wilaya').val('');
        var value = $('#structure_affectation').val();
        var code_wilaya = $('.selectpicker').find("[value='"+value+"']" ).attr('code_wilaya');
        $('#code_wilaya').val(code_wilaya);
        }
        </script>
      </div> -->
      <div class="col-md-6">
        <h5 class="m-t-30 m-b-10"><b>Nom du chauffeur</b></h5>
        <select class="selectpicker show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control" id="driver"
              onchange="show_structure(this.value)">
          <?php foreach ($chauffeurs as $chauffeur)
          {
          echo '<option >'. $chauffeur['nom']. '</option>';
          } ?> 
          
        </select>
      </div>
      <div class="col-md-3">
        <h5 class="m-t-30 m-b-10"><b>Date d'accident</b></h5>
        <div class="input-group">
          <input type="text" class="form-control" class="datepicker-autoclose" placeholder="" style="text-align: center;font-size: 17px;" id="accident_date">
          <span class="input-group-addon"><i class="icon-calender"></i></span> </div>
        </div>
        <div class="col-md-3">
          <h5 class="m-t-30 m-b-10"><b>Heure</b></h5>
          <div class="input-group clockpicker " data-placement="bottom" data-align="top" data-autoclose="true" >
            <input type="text" class="form-control" value="" style="text-align: center; font-size: 17px;" id="accident_time">
            <span class="input-group-addon"> <span class="glyphicon glyphicon-time"></span> </span>
          </div>
        </div>
        <div class="col-md-12">
          <h5 class="m-t-30 m-b-10"><b>Détail d'accident</b></h5>
          <!-- <input type="text" class="form-control" id="details" /> -->
          <textarea type="text" class="form-control" id="details"></textarea>
        </div>
        <div class="col-md-12">
          <h5 class="m-t-30 m-b-10"><b>Constat d'accident</b></h5>
          <iframe name="hiddenFrame_" style="position:absolute; top:-1px; left:-1px; width:1px; height:1px;display: none;"></iframe>
          <form action="upload_accident" method="post" enctype="multipart/form-data" target="_blank" id="accident_form">
            <input type="file" name="files" id="filer_input2" single>
            <input type="text" name="id_inserted_accident" id="id_inserted_accident" style="display: none;">
            <!-- <input type="submit" value="Submit">  -->
          </form>
        </div>
      </div>
      <hr>
      <div class="row ">
        <div class=" col-sm-12 ">
          <div class="text-right">
            <button class="fcbtn btn btn-danger btn-outline btn-1c "
            style="width:20%" id="cancel_affectation_btn" data-perform="panel-collapse">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <button class="fcbtn btn btn-success btn-outline btn-1c "
            style="width:20%" id="add_affectation_btn" onclick="add_accident()">Ajouter</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
      function add_accident() {
          // id of inserted vehicle
          var id_vehicle = $('#id_selected_vehicle').val();
          // Get values from inputs
          // var etat = $('#etat').val();
          var details = $('#details').val();
          var accident_date = $('#accident_date').val();
          var accident_time = $('#accident_time').val();
          var driver = $('#driver').selectpicker('val');
          if (details != "" &&  accident_date != "" && accident_time != ""
          && driver != "" ) {
          // insert data to database
          //ex : '{"key":"val","key1":"val1"}'
          var data2db = JSON.parse(
          '{"details":"' +details+
          '","accident_date":"' +accident_date+
          '","accident_time":"' +accident_time+
          '","driver":"' +driver+
          '","id_vehicle":"' +id_vehicle+ '"}'
          );
          console.log(data2db);
          var url = "add_accident";
          $.post(url,data2db).done(function( data ) {
          var reply = JSON.parse(data);
          console.log(reply);
          if (reply['message'] == 'data inserted') {
          $('#id_inserted_accident').val(reply['id_inserted_accident']);
          $('#accident_form').trigger('submit');
          swal({
          title: "Succès!",
          text: "Informations ajoutées avec succès",
          type: "success",
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Terminer",
          closeOnConfirm: true,
          });
          $('#accident_collapse').removeClass('in');
          $('#accident_collapse').attr('aria-expanded',false);
          $('#accident_panel').hide();
          }else if (reply == 'error') {
          swal("Erreur...","Erreur lors l'ajout d'informations", "error");
          }
          // swal("ID CAS", mObj, "success");
          }).fail(function(data, textStatus, errorThrown){
          swal("Erreur...","", "error");
          });
          }
          else swal("Erreur!","Veuillez remplir tous les champs", "error");
          }
      </script>