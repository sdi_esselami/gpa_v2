<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <title>CNAS | Gestion Parc Automobile </title>
    <!-- HTML5 Shim and Respond.js IE10 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 10]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="description" content="#">
    <meta name="keywords" content="Admin , Responsive, Landing, Bootstrap, App, Template, Mobile, iOS, Android, apple, creative app">
    <meta name="author" content="#">
    <!-- Favicon icon -->
    <link rel="icon" href="<?php echo base_url(); ?>plugins/assets/images/favicon.ico" type="image/x-icon">
   <!-- radial chart -->
   <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/assets/pages/chart/radial/css/radial.css" type="text/css" media="all">
   <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/assets/icon/themify-icons/themify-icons.css">

    <!-- Required Fremwork -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/bower_components/bootstrap/css/bootstrap.min.css">
    <!-- feather Awesome -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/assets/icon/feather/css/feather.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/assets/css/font-awesome.min.css">
    <!-- Style.css -->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/assets/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/assets/css/fonts.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/assets/css/jquery.mCustomScrollbar.css">
</head>

<body>
    <!-- Pre-loader start -->
    <div class="theme-loader">
        <div class="ball-scale">
            <div class='contain'>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
                <div class="ring">
                    <div class="frame"></div>
                </div>
            </div>
        </div>
    </div>
    <!-- Pre-loader end -->
    <div id="pcoded" class="pcoded">
        <div class="pcoded-overlay-box"></div>
        <div class="pcoded-container navbar-wrapper">

            <nav class="navbar header-navbar pcoded-header" >
                <div class="navbar-wrapper">

                    <div class="navbar-logo" logo-theme="theme1">
                        <a class="mobile-menu" id="mobile-collapse" href="javascript:void()">
                            <i class="feather icon-menu"></i>
                        </a>
                        <a href="accueil">
                            <img class="img-fluid" src="<?php echo base_url(); ?>plugins/assets/images/logo.png" alt="Theme-Logo" />
                        </a>
                        <a class="mobile-options">
                            <i class="feather icon-more-horizontal"></i>
                        </a>
                    </div>

                    <div class="navbar-container container-fluid">
                        <ul class="nav-left">
<!--                             <li class="header-search">
                                <div class="main-search morphsearch-search">
                                    <div class="input-group">
                                        <span class="input-group-addon search-close"><i class="feather icon-x"></i></span>
                                        <input type="text" class="form-control">
                                        <span class="input-group-addon search-btn"><i class="feather icon-search"></i></span>
                                    </div>
                                </div>
                            </li> -->
                            <li>
                                <a href="javascript:void()" onclick="javascript:toggleFullScreen()">
                                    <i class="feather icon-maximize full-screen"></i>
                                </a>
                            </li>
                        </ul>
                         <ul class="nav-right">
                         <li class="header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <i class="feather icon-bell"></i>
                                       <!--  <span class="badge bg-c-pink"></span> -->
                                    </div>
                                    <!-- <ul class="show-notification notification-view dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <h6>Notifications</h6>
                                            <label class="label label-danger">New</label>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo base_url(); ?>plugins/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">John Doe</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo base_url(); ?>plugins/assets/images/avatar-3.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Joseph William</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="media">
                                                <img class="d-flex align-self-center img-radius" src="<?php echo base_url(); ?>plugins/assets/images/avatar-4.jpg" alt="Generic placeholder image">
                                                <div class="media-body">
                                                    <h5 class="notification-user">Sara Soudein</h5>
                                                    <p class="notification-msg">Lorem ipsum dolor sit amet, consectetuer elit.</p>
                                                    <span class="notification-time">30 minutes ago</span>
                                                </div>
                                            </div>
                                        </li>
                                    </ul> -->
                                </div>
                            </li>
                         <li class="user-profile header-notification">
                                <div class="dropdown-primary dropdown">
                                    <div class="dropdown-toggle" data-toggle="dropdown">
                                        <img src="<?php echo base_url(); ?>plugins/assets/images/avatar-.jpg" class="img-radius" alt="">
                                        <span> <?php echo '$username;' ?></span>
                                        <i class="feather icon-chevron-down"></i>
                                    </div>
                                    <ul class="show-notification profile-notification dropdown-menu" data-dropdown-in="fadeIn" data-dropdown-out="fadeOut">
                                        <li>
                                            <a href="change_password_v">
                                                <i class="feather icon-unlock"></i> Changer mot de passe
                                            </a>
                                        </li>
                                        <li>
                                            <a href="my_profile_v">
                                                <i class="feather icon-user"></i> Mon profile
                                            </a>
                                        </li>
                                       <!--  <li>
                                            <a href="email-inbox.html">
                                                <i class="feather icon-mail"></i> My Messages
                                            </a>
                                        </li> -->
                                        <!-- <li>
                                            <a href="auth-lock-screen.html">
                                                <i class="feather icon-lock"></i> Fermé la session
                                            </a>
                                        </li> -->
                                        <li>
                                            <a href="logout">
                                                <i class="feather icon-log-out"></i> Déconnexion
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                            </li>
                          
                        </ul>
                    </div>
                </div>
            </nav>



            <div class="pcoded-main-container">
                <div class="pcoded-wrapper">
                    <nav class="pcoded-navbar">
                        <div class="pcoded-inner-navbar main-menu">
                            <div class="pcoded-navigatio-lavel">Menu principal</div>
                            <ul class="pcoded-item">
                                <li class="active">
                                    <a href="accueil">
                                        <span class="pcoded-micon"><i class="feather icon-home"></i></span>
                                        <span class="pcoded-mtext">Accueil</span>
                                    </a>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-user"></i></span>
                                        <span class="pcoded-mtext" >Options du compte</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="my_profile_v">
                                                <span class="pcoded-mtext" >Mon profile</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="change_password_v">
                                                <span class="pcoded-mtext" >Changer mot de passe</span>
                                            </a>
                                        </li>  
                                        <li class=" ">
                                            <a href="notifications_v">
                                                <span class="pcoded-mtext" >Notifications</span>
                                                <span class="pcoded-badge label label-danger">NOUVEAU</span>
                                            </a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                            <div class="pcoded-navigatio-lavel">Gestion des tables</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-car"></i></span>
                                        <span class="pcoded-mtext" >Vèhicules</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="vehicule_add_v">
                                                <span class="pcoded-mtext">Ajouter un véhicule</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="vehicules_list_v">
                                                <span class="pcoded-mtext" >Liste des vèhicules</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-users"></i></span>
                                        <span class="pcoded-mtext" >Chauffeurs</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="chauffeur_add_v">
                                                <span class="pcoded-mtext">Ajouter un chauffeur</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="chauffeurs_list_v">
                                                <span class="pcoded-mtext" >Liste des chauffeurs</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="feather icon-shield"></i></span>
                                        <span class="pcoded-mtext" >Agences d'assurance</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="assurance_add_v">
                                                <span class="pcoded-mtext">Ajouter une agence</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="assurances_list_v">
                                                <span class="pcoded-mtext" >Liste des agences</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-wrench"></i></span>
                                        <span class="pcoded-mtext" >Réparateur-Mécanicien</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="reparateur_add_v">
                                                <span class="pcoded-mtext">Ajouter un mécanicien</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="reparateurs_list_v">
                                                <span class="pcoded-mtext" >Liste des mécaniciens</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="pcoded-navigatio-lavel">Entretiens-Réparations</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="ti-spray"></i></span>
                                        <span class="pcoded-mtext" >Entretiens</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="entretiens_list_v">
                                                <span class="pcoded-mtext" >Liste des entretiens</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-cogs"></i></span>
                                        <span class="pcoded-mtext" >Réparations</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="reparations_list_v">
                                                <span class="pcoded-mtext" >Liste des réparations</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="pcoded-navigatio-lavel">Accidents-Pannes</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-fire"></i></span>
                                        <span class="pcoded-mtext" >Accidents</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="accident_add_v">
                                                <span class="pcoded-mtext" >Ajouter un accident</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="accidents_list_v">
                                                <span class="pcoded-mtext" >Listes des accidents</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="pcoded-hasmenu">
                                    <a href="javascript:void(0)">
                                        <span class="pcoded-micon"><i class="fa fa-warning"></i></span>
                                        <span class="pcoded-mtext" >Pannes</span>
                                    </a>
                                    <ul class="pcoded-submenu">
                                        <li class=" ">
                                            <a href="panne_add_v">
                                                <span class="pcoded-mtext" >Ajouter une panne</span>
                                            </a>
                                        </li>
                                        <li class=" ">
                                            <a href="pannes_list_v">
                                                <span class="pcoded-mtext" >Listes des pannes</span>
                                            </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                            <div class="pcoded-navigatio-lavel">Déxonnexion</div>
                            <ul class="pcoded-item pcoded-left-item">
                                <li class="">
                                    <a href="logout">
                                        <span class="pcoded-micon"><i class="fa fa-power-off"></i></span>
                                        <span class="pcoded-mtext" >Déconnexion</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div class="pcoded-content">
                        <div class="pcoded-inner-content">
                            <div class="main-body">
                                <div class="page-wrapper">
                                       <!-- Page-header start -->
                                       <div class="page-header">
                                        <div class="row align-items-end">
                                            <div class="col-lg-8">
                                                <div class="page-header-title">
                                                    <div class="d-inline">
                                                        <h4>Accueil</h4>
                                                        <span>Etat du parc automobile</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="page-header-breadcrumb">
                                                    <ul class="breadcrumb-title">
                                                        <li class="breadcrumb-item">
                                                            <a href=""> <i class="feather icon-home"></i> </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Page-header end -->

                                    <div class="page-body">
                                        <div class="row">
                                          <!-- Page content start -->
                                                  <!-- customar project  start -->
                                            <div class="col-xl-3 col-md-6 clickable">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="row align-items-center m-l-0">
                                                            <div class="col-auto">
                                                                <i class="fa fa-car f-36 text-c-lite-green"></i>
                                                            </div>
                                                            <div class="col-auto">
                                                                <h6 class="text-c-lite-muted f-12 m-b-10">VEHICULES</h6>
                                                                <a href="vehicules_list_v"><h2 class="m-b-0 f-22">02</h2></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6 clickable">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="row align-items-center m-l-0">
                                                            <div class="col-auto">
                                                                <i class="fa fa-user f-36 text-warning"></i>
                                                            </div>
                                                            <div class="col-auto">
                                                                <h6 class="text-c-lite-muted f-12 m-b-10">CHAUFFEURS</h6>
                                                                <a href="chauffeurs_list_v"><h2 class="m-b-0 f-22">02</h2></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6 clickable">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="row align-items-center m-l-0">
                                                            <div class="col-auto">
                                                                <i class="fa fa-shield f-36 text-info card1-icon"></i>
                                                            </div>
                                                            <div class="col-auto">
                                                                <h6 class="text-c-lite-muted f-12 m-b-10">AGENCE ASSURANCE</h6>
                                                                <a href="assurances_list_v"><h2 class="m-b-0 f-22">02</h2></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xl-3 col-md-6 clickable">
                                                <div class="card">
                                                    <div class="card-block">
                                                        <div class="row align-items-center m-l-0">
                                                            <div class="col-auto">
                                                                <i class="fa fa-wrench f-36 text-danger"></i>
                                                            </div>
                                                            <div class="col-auto">
                                                                <h6 class="text-c-lite-muted f-12 m-b-10">REPARATEURS</h6>
                                                                <a href="reparateurs_list_v"><h2 class="m-b-0 f-22">02</h2></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        <!-- Page content end -->
                                        </div>
                                    </div>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <!-- Warning Section Starts -->
    <!-- Older IE warning message -->
    <!--[if lt IE 10]>
<div class="ie-warning">
    <h1>Warning!!</h1>
    <p>You are using an outdated version of Internet Explorer, please upgrade <br/>to any of the following web browsers to access this website.</p>
    <div class="iew-container">
        <ul class="iew-download">
            <li>
                <a href="http://www.google.com/chrome/">
                    <img src="<?php echo base_url(); ?>plugins/assets/images/browser/chrome.png" alt="Chrome">
                    <div>Chrome</div>
                </a>
            </li>
            <li>
                <a href="https://www.mozilla.org/en-US/firefox/new/">
                    <img src="<?php echo base_url(); ?>plugins/assets/images/browser/firefox.png" alt="Firefox">
                    <div>Firefox</div>
                </a>
            </li>
            <li>
                <a href="http://www.opera.com">
                    <img src="<?php echo base_url(); ?>plugins/assets/images/browser/opera.png" alt="Opera">
                    <div>Opera</div>
                </a>
            </li>
            <li>
                <a href="https://www.apple.com/safari/">
                    <img src="<?php echo base_url(); ?>plugins/assets/images/browser/safari.png" alt="Safari">
                    <div>Safari</div>
                </a>
            </li>
            <li>
                <a href="http://windows.microsoft.com/en-us/internet-explorer/download-ie">
                    <img src="<?php echo base_url(); ?>plugins/assets/images/browser/ie.png" alt="">
                    <div>IE (9 & above)</div>
                </a>
            </li>
        </ul>
    </div>
    <p>Sorry for the inconvenience!</p>
</div>
<![endif]-->
    <!-- Warning Section Ends -->
    <!-- Required Jquery -->
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/jquery/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/jquery-ui/js/jquery-ui.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/popper.js/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/bootstrap/js/bootstrap.min.js"></script>
    <!-- jquery slimscroll js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/jquery-slimscroll/js/jquery.slimscroll.js"></script>
    <!-- modernizr js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/modernizr/js/modernizr.js"></script>
    <!-- Chart js -->
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/bower_components/chart.js/js/Chart.js"></script>
    <!-- amchart js -->
    <script src="<?php echo base_url(); ?>plugins/assets/pages/widget/amchart/amcharts.js"></script>
    <script src="<?php echo base_url(); ?>plugins/assets/pages/widget/amchart/serial.js"></script>
    <script src="<?php echo base_url(); ?>plugins/assets/pages/widget/amchart/light.js"></script>
    <script src="<?php echo base_url(); ?>plugins/assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/assets/js/SmoothScroll.js"></script>
    <script src="<?php echo base_url(); ?>plugins/assets/js/pcoded.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/assets/pages/widget/excanvas.js"></script>

    <!-- custom js -->
    <script src="<?php echo base_url(); ?>plugins/assets/js/vartical-layout.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>plugins/assets/js/script.min.js"></script>


<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-23581568-13');
</script>
</body>



</html>
