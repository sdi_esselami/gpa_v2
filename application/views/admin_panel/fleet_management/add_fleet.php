<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
    <title>CNAS | Gestion PARC  AUTO</title>
    <!-- Bootstrap Core CSS -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/typeahead.js-master/dist/typehead-min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>/plugins/bower_components/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">

    <!-- Date picker plugins css -->
    <link href="../plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="fix-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <div id="idle-timeout-dialog" data-backdrop="static" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Your session is expiring soon</h4> </div>
                        <div class="modal-body">
                            <p>
                                <i class="fa fa-warning font-red"></i> You session will be locked in
                                <span id="idle-timeout-counter"></span> seconds.</p>
                                <p> Do you want to continue your session? </p>
                            </div>
                            <div class="modal-footer text-center">
                                <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-outline btn-success" data-dismiss="modal">Yes, Keep Working</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Top Navigation -->
                <nav class="navbar navbar-default navbar-static-top m-b-0">
                    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                        <div class="top-left-part"><a class="logo" href="accueil"><b><!--This is dark logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><!--This is dark logo text--><img src="<?php echo base_url(); ?>plugins/images/parcautom-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-text-darkl.png" alt="home" class="light-logo" /></span></a></div>
                        <ul class="nav navbar-top-links navbar-left hidden-xs">
                            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                            <li>
                        <!-- <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form> -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-settings"></i>
                      <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                  </a>
                  <ul class="dropdown-menu animated flipInY">
                    <li><a href="my_profile"><i class="ti-user"></i> Mon Profile</a></li>
                    <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                    <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                    <!-- <li role="separator" class="divider"></li> -->
                    <!-- <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                    <li role="separator" class="divider"></li>
                    <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="<?php echo base_url(); ?>plugins/images/user-me.jpg" alt="user-img" class="img-circle"></div>
                        <a href="#" class="dropdown-toggle u-dropdown text-success" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username'); ?><span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="my_profile"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="account_settings"><i class="ti-settings"></i> Options du compte</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                       
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
        </span> </div></li> -->



        <!-- Change active menu : default accueil is active --> 


        <li class="nav-small-cap m-t-10 text-danger">--- Menu principal</li>
        <li> <a href="accueil" class="waves-effect"><i class="icon-home text-danger" ></i> <span class="hide-menu text-danger"> Accueil </span></a></li>
        <li> <a href="account_settings" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="P"></i> <span class="hide-menu text-danger"> Options du compte </span></a></li>

        <li class="nav-small-cap text-warning">--- Gestion du parc</li>
        <li><a href="javascript:void(0);" class="waves-effect active"><i class="linea-icon linea-basic fa fa-car text-warning" ></i> <span class="hide-menu text-warning" >Véhicules<span class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level collapse in">
                <li> <a href="all_fleets">Tous les véhicules</a></li>
                <li class="active"> <a href="add_fleet" class="active">Ajouter un véhicule</a></li>
                <li> <a href="edit_fleet">Modifier un véhicule</a></li>
            </ul>
        </li>
        <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-speedometer text-warning" ></i> <span class="hide-menu text-warning">Chauffeurs<span class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li> <a href="all_drivers">Tous les chauffeurs</a></li>
                <li> <a href="add_driver">Ajouter un chauffeur</a></li>
                <li> <a href="edit_driver">Modifier un chauffeur</a></li>
            </ul>
        </li>

        <li class="nav-small-cap text-blue">--- Gestion du personnel</li>
        <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-people fa-fw text-blue" ></i> <span class="hide-menu text-blue">Utilisateurs<span class="fa arrow"></span></span></a>
            <ul class="nav nav-second-level">
                <li> <a href="all_users">Tous les utilisateurs</a></li>
                <li> <a href="add_user">Ajouter un utilisateur</a></li>
                <li> <a href="edit_user">Modifier un utilisateur</a></li>
            </ul>
        </li>




        <li><a href="logout" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>

    </ul>
</div>
</div>
<!-- Left navbar-header end -->
<!-- Page Content -->
<div id="page-wrapper">

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

                <!-- Page title goes here --> <h4 class="page-title">Ajouter un véhicule</h4>

            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li ><a href="accueil">Accueil</a></li>
                    <li class="active">Véhicules</li>
                    <li class="active">Ajouter</li>
                    <!-- Breadcrumb goes here : Accueil/.....  -->                            

                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">


         <!-- Page content starts here -->
<!-- //                                    '"marque":"' +$('#marque').val()+ 
  //                                    '"modele":"' +$('#modele').val()+ 
  //                                    '"chauffeur":"' +$('#nom_chauffeur').val()+ 
  //                                    '"motorisation":"' +$('#motorisation').val()+ 
  //                                    '"num_immatriculation":"' +$('#num_immatriculation').val()+ 
  //                                    '"etat":"' +$('#etat').val()+ 
  //                                    '"gps_tracker":"' +$('#gps_tracker').val()+ 
  //                                    '"compagnie_assurance":"' +$('#compagnie_assurance').val()+ 
  //                                    '"num_immatriculattion_old":"' +$('#num_immatriculattion_old').val()+ 
  //                                    '"numero_immatriculation":"' +$('#numero_immatriculation').val()+ 
  //                                    '"type":"' +$('#type').val()+ -->

<div class="col-sm-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0"><b>RESEIGNEMENTS DU VEHICULE</b></h3>
                            <div class="row">
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>DESIGNATION</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="designation">
                                        <option>Agence</option>
                                        <option>Etablissement</option>
                                        <option>Direction générale</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>NOM</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="nom_etablissement">
                                                                                   
                                            <option>AGENCE ADRAR</option>
                                            <option>AGENCE CHLEF</option>
                                            <option>AGENCE LAGHOUAT</option>
                                            <option>AGENCE OUM EL BOUAGHI</option>
                                            <option>AGENCE BATNA</option>
                                            <option>AGENCE BEJAIA</option>
                                            <option>AGENCE BISKRA</option>
                                            <option>AGENCE BLIDA</option>
                                            <option>AGENCE BOUIRA</option>
                                            <option>AGENCE TAMANRASSET</option>
                                            <option>AGENCE TEBESSA</option>
                                            <option>AGENCE TLEMCEN</option>
                                            <option>AGENCE TIARET</option>
                                            <option>AGENCE TIZI OUZOU</option>
                                            <option>AGENCE ALGER</option>
                                            <option>AGENCE DES FONCTIONNAIRES</option>
                                            <option>AGENCE DJELFA</option>
                                            <option>AGENCE JIJEL</option>
                                            <option>AGENCE SETIF</option>
                                            <option>AGENCE SAIDA</option>
                                            <option>AGENCE SKIKDA</option>
                                            <option>AGENCE SIDI BEL ABBES</option>
                                            <option>AGENCE ANNABA</option>
                                            <option>AGENCE GUELMA</option>
                                            <option>AGENCE CONSTANTINE</option>
                                            <option>AGENCE MEDEA</option>
                                            <option>AGENCE MOSTAGANEM</option>
                                            <option>AGENCE M'SILA</option>
                                            <option>AGENCE MASCARA</option>
                                            <option>AGENCE OUARGLA</option>
                                            <option>AGENCE ORAN</option>
                                            <option>AGENCE EL BAYADH</option>
                                            <option>AGENCE ILLIZI</option>
                                            <option>AGENCE BORDJ BOU ARRERIDJ</option>
                                            <option>AGENCE BOUMERDES</option>
                                            <option>AGENCE EL TAREF</option>
                                            <option>AGENCE TINDOUF</option>
                                            <option>AGENCE TISSEMSILT</option>
                                            <option>AGENCE EL OUED</option>
                                            <option>AGENCE KHENCHELA</option>
                                            <option>AGENCE SOUKAHRAS</option>
                                            <option>AGENCE TIPAZA</option>
                                            <option>AGENCE MILA</option>
                                            <option>AGENCE AIN DEFLA</option>
                                            <option>AGENCE NAAMA</option>
                                            <option>AGENCE AIN TEMOUCHENT</option>
                                            <option>AGENCE GHARDAIA</option>
                                            <option>AGENCE RELIZANE</option>
                                            <option>EL DJORF ED'DAHABI (MELBOU)</option>
                                            <option>CENTRE FAMILIALE DE BEN AKNOUNE</option>
                                            <option>IMPRIMERIE DE CONSTANTINE</option>
                                            <option>CSORVAT MESSERGHINE</option>
                                            <option>EHS-CMCI BOUISMAIL</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>CODE WILAYA</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control "  data-size="7" id="code_wilaya">
                                                                                   
                                             <option>W01</option> 
                                             <option>W02</option>
                                             <option>W03</option>
                                             <option>W04</option>
                                             <option>W05</option>
                                             <option>W06</option>
                                             <option>W07</option>
                                             <option>W08</option>
                                             <option>W09</option>
                                             <option>W10</option>
                                             <option>W11</option>
                                             <option>W12</option>
                                             <option>W13</option>
                                             <option>W14</option>
                                             <option>W15</option>
                                             <option>W16</option>
                                             <option>W17</option>
                                             <option>W18</option>
                                             <option>W19</option>
                                             <option>W20</option>
                                             <option>W21</option>
                                             <option>W22</option>
                                             <option>W23</option>
                                             <option>W24</option>
                                             <option>W25</option>
                                             <option>W26</option>
                                             <option>W27</option>
                                             <option>W28</option>
                                             <option>W29</option>
                                             <option>W30</option>
                                             <option>W31</option>
                                             <option>W32</option>
                                             <option>W33</option>
                                             <option>W34</option>
                                             <option>W35</option>
                                             <option>W36</option>
                                             <option>W37</option>
                                             <option>W38</option>
                                             <option>W39</option>
                                             <option>W40</option>
                                             <option>W41</option>
                                             <option>W42</option>
                                             <option>W43</option>
                                             <option>W44</option>
                                             <option>W45</option>
                                             <option>W46</option>
                                             <option>W47</option>
                                             <option>W48</option>

                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <h5 class="m-t-30 m-b-10"><b>AFECTATION</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="affectation">
                                        <option>MINISTERE DE LA JUSTICE</option>
                                        <option>MINISTERE DU TRAVAIL DE L'EMPLOI ET DE LA SECURITE SOCIALE</option>
                                        <option>PRESIDENT DU CONSEIL D'ADMINISTRATION</option>
                                        <option>DIRECTEUR GENERAL</option>
                                        <option>DIRECTEUR GENERAL ADJOINT</option>
                                        <option>PRESIDENT DE LA COMMISSION NATIONALE MEDICALE</option>
                                        <option>D.ASS</option>
                                        <option>D.ATMP</option>
                                        <option>D.CM</option>
                                        <option>D.ESO</option>
                                        <option>D.INFORMATIQUE</option>
                                        <option>D.INSPCTION</option>
                                        <option>D.OF</option>
                                        <option>D.P</option>
                                        <option>D.PF</option>
                                        <option>D.RC</option>
                                        <option>D.REMG</option>
                                        <option>C.CONTENTIEUX</option>
                                        <option>C.CONVENTIONNEMENT</option>
                                        <option>SIE</option>
                                        <option>ASSISTANT CONSEILLER(E)</option>
                                        <option>ASSISTANT DE DIRECTION</option>
                                        <option>ECOLE SUPERIEURE DE LA SECURITE SOCIALE</option>
                                        <option>DIRECTEUR AGENCE</option>
                                        <option>DIRECTRICE AGENCE</option>
                                        <option>DIRECTEUR ETABLISSEMENT</option>
                                        <option>DIRECTRICE ETABLISSEMENT</option>
                                        <option>ŒUVRE SOCIALE</option>
                                        <option>PARC AUTOMOBILE</option>
                                        <option>AUTRE</option>

                                    </select>
                                </div>     
                            </div>
                         <br>
                         <hr>
                            <div class="row">
                                <div class="col-sm-6">
                                   <h5 class="m-t-30 m-b-10"><b>MARQUE</b></h5> 
                                    <div id="marques">
                                      <input class="typeahead form-control" type="text" placeholder="HYUNDAI, BMW,..." id="marque">
                                  </div>
                                </div>
                                <div class="col-sm-6">
                                    <h5 class="m-t-30 m-b-10"><b>MODELE</b></h5>
                                    <input class="form-control" type="text" placeholder="ELANTRA, ACCENT,..." id="modele">
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>MOTORISATION</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="motorisation">
                                        <option>DIESEL</option>
                                        <option>ESSENCE</option>
                                        <option>GPL</option>
                                        <option>ESSENCE / GPL</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>TYPE</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="type">
                                        <div class=""></div>
                                        <option>AMBULANCE</option>
                                        <option>ENGIN</option>
                                        <option>CAMION</option>
                                        <option>VEHICULE TOURISTIQUE</option>
                                        <option>VEHICULE UTILITAIRE</option>
                                        <option>CAMION</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input class="form-control kilometrage" id="kilometrage" >
                                </div>    
                            </div>
                            <div class="row">
                                <div class="col-sm-4">
                                   <h5 class="m-t-30 m-b-10"><b>N° CHASSIS</b></h5> 
                                 
                                      <input class=" form-control" type="text"  maxlength="17" id="num-chassis">
                                  
                                </div>
                                <div class="col-sm-4">
                                    <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION</b></h5>
                                      <input class="form-control immatriculation"  placeholder="_____-___-__"
                                      id="numero_immatriculation">
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION (Ancienne/Provisoire)</b></h5>
                                    <input class="form-control immatriculation"  placeholder="_____-___-__"
                                    id="numero_immatriculation_old">
                                </div>
                         
                            </div>   
                             <div class="row">
                                <div class="col-sm-3">
                                   <h5 class="m-t-30 m-b-10"><b>N° CARTE NAFTAL</b></h5> 
                                 
                                      <input class="form-control naftal" type="text" placeholder="____-____-____-____"
                                      id="num-carte-naftal">
                                   
                                </div>
                                <div class="col-sm-3">
                                    <h5 class="m-t-30 m-b-10"><b>DATE EXPIRATION</b></h5>
                                      <input class="form-control mydatepicker "  placeholder="JJ-MM-AAAA"
                                      id="date-carte-naftal">
                                </div>
                                <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>CREDIT</b></h5>
                                    <input class="form-control credit" id="credit-carte-naftal">
                                </div><div class="col-md-3 ">
                                    <h5 class="m-t-30 m-b-10 " ><b>DEBIT</b></h5>
                                    <input class="form-control debit"  id="debit-carte-naftal">
                                </div>
                             <div class="col-md-12">
                                    <h5 class="m-t-30 m-b-10"><b>AFECTATION DE LA CARTE NAFTAL</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="affectation-carte-naftal">
                                        <option>MINISTERE DE LA JUSTICE</option>
                                        <option>MINISTERE DU TRAVAIL DE L'EMPLOI ET DE LA SECURITE SOCIALE</option>
                                        <option>PRESIDENT DU CONSEIL D'ADMINISTRATION</option>
                                        <option>DIRECTEUR GENERAL</option>
                                        <option>DIRECTEUR GENERAL ADJOINT</option>
                                        <option>PRESIDENT DE LA COMMISSION NATIONALE MEDICALE</option>
                                        <option>D.ASS</option>
                                        <option>D.ATMP</option>
                                        <option>D.CM</option>
                                        <option>D.ESO</option>
                                        <option>D.INFORMATIQUE</option>
                                        <option>D.INSPCTION</option>
                                        <option>D.OF</option>
                                        <option>D.P</option>
                                        <option>D.PF</option>
                                        <option>D.RC</option>
                                        <option>D.REMG</option>
                                        <option>C.CONTENTIEUX</option>
                                        <option>C.CONVENTIONNEMENT</option>
                                        <option>SIE</option>
                                        <option>ASSISTANT CONSEILLER(E)</option>
                                        <option>ASSISTANT DE DIRECTION</option>
                                        <option>ECOLE SUPERIEURE DE LA SECURITE SOCIALE</option>
                                        <option>DIRECTEUR AGENCE</option>
                                        <option>DIRECTRICE AGENCE</option>
                                        <option>DIRECTEUR ETABLISSEMENT</option>
                                        <option>DIRECTRICE ETABLISSEMENT</option>
                                        <option>ŒUVRE SOCIALE</option>
                                        <option>PARC AUTOMOBILE</option>
                                        <option>AUTRE</option>

                                    </select>
                                </div>
                            </div>
                            <br>
                            <hr>
                             <div class="row">
                                <div class="col-sm-6">
                                   <h5 class="m-t-30 m-b-10"><b>NOMBRE DE BONS OCTROYES</b></h5> 
                                
                                      <input class="form-control" type="number" id="nombre-bon-granted">
                                   
                                </div>
                                <div class="col-sm-6">
                                    <h5 class="m-t-30 m-b-10"><b>NOMBRE DE BONS CONSOMMES</b></h5>
                                      <input class="form-control"  type="number" id="nombre-bon-consumed">
                                </div>
                                
                         
                            </div> 
                            <div class="row">
                                <div class="col-sm-3">
                                   <h5 class="m-t-30 m-b-10"><b>COMPAGNIE D'ASSURANCE</b></h5> 
                                 <div id="car-brands">
                                      <input class="form-control" type="text" id="compagnie_assurance">
                                 </div>   
                                </div>     <div class="col-sm-3">
                                   <h5 class="m-t-30 m-b-10"><b>NUMERO POLICE D'ASSURANCE</b></h5> 
                                 <div id="car-brands">
                                      <input class="form-control" type="text" id="numero_police">
                                 </div>   
                                </div>
                                <div class="col-sm-6">
                                    <h5 class="m-t-30 m-b-10"><b>DATE D'ECHEANCE</b></h5>
                                      <div class="input-daterange input-group" id="date-range">
                                            <span class="input-group-addon bg-info b-0 text-white">Du</span>
                                            <input type="text" class="form-control" name="start" id="date_debut_echeance" />
                                            <span class="input-group-addon bg-info b-0 text-white">au</span>
                                            <input type="text" class="form-control" name="end" id="date_fin_echeance" />
                                        </div>
                                </div>                
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                   <h5 class="m-t-30 m-b-10"><b>NOM DU CHAUFFEUR AFFECTE</b></h5> 
                                  <input class="form-control" type="text" placeholder="NOM DU CHAUFFEUR" id="nom_chauffeur">
                                   
                                </div>
                                <div class="col-sm-3">
                                    <h5 class="m-t-30 m-b-10"><b>TYPE CHAUFFEUR</b></h5>
                                        <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control" id="type_chauffeur">
                                        <div class=""></div>
                                        <option>Habituel</option>
                                        <option>Ocasionnel</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <h5 class="m-t-30 m-b-10"><b>TYPE PERMIS DE CONDUIRE</b></h5>
                                        <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control" id="type_permis">
                                        <div class=""></div>
                                        <option>A</option>
                                        <option>B</option>
                                        <option>C</option>
                                    </select>
                                </div>
                                <div class="col-sm-3">
                                    <h5 class="m-t-30 m-b-10"><b>DATE VALIDATION DU P.C</b></h5>
                                      <input class="form-control mydatepicker "  placeholder="JJ-MM-AAAA" id="validation_permis">
                                </div>
                                <div class="col-sm-12">
                                    <h5 class="m-t-30 m-b-10"><b>AUTRE AFFECTATION</b></h5>
                                   <input class="form-control" type="text" id="autre_affectation">
                                </div>                
                            </div> 

                            <div class="row">
                     
                                <div class="col-sm-6">
                                    <h5 class="m-t-30 m-b-10"><b>ETAT DU VEHICULE</b></h5>
                                        <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control" id="etat">
                                        <div class=""></div>
                                        <option>BON ETAT</option>
                                        <option>NEUF</option>
                                        <option>ETAT MOYEN</option>
                                        <option>MAUVAIS ETAT</option>
                                        <option>PROPOSER A LA REFOME</option>
                                        <option>REFORME PAR PV</option>
                                    </select>
                                </div>
                                <div class="col-sm-6">
                                    <h5 class="m-t-30 m-b-10"><b>VEHICULE TRACKER PAR GPS</b></h5>
                                        <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control" id="gps_tracker">
                                        <div class=""></div>
                                        <option>OUI</option>
                                        <option>NON</option>
                                        
                                    </select>
                                </div>
                                      
                            </div>
                            <br>
                            <hr>
                         <div class="row "> 
                           <div class=" col-sm-12 ">
                            <div class="text-right">

                            <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                            style="width:20%" id="cancel-btn">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                            
                            <button class="fcbtn btn btn-success btn-outline btn-1c " 
                           style="width:20%" id="add-fleet-btn" onclick="add_fleet()">Ajouter</button>
                       </div> 
                   </div>
                        </div>
                        
                    </div>      
                 </div>
                     
        </div>
    </div>
    <!-- / Page content ends here -->                
    <!-- .right-sidebar -->
    <div class="right-sidebar">
        <div class="slimscrollright">
            <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
            <div class="r-panel-body">
                <ul>
                    <li><b>Layout Options</b></li>
                    <li>
                        <div class="checkbox checkbox-info">
                            <input id="checkbox1" type="checkbox" class="fxhdr">
                            <label for="checkbox1"> Fix Header </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox checkbox-warning">
                            <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                            <label for="checkbox2"> Fix Sidebar </label>
                        </div>
                    </li>
                    <li>
                        <div class="checkbox checkbox-success">
                            <input id="checkbox4" type="checkbox" class="open-close">
                            <label for="checkbox4"> Toggle Sidebar </label>
                        </div>
                    </li>
                </ul>
                <ul id="themecolors" class="m-t-20">
                    <li><b>With Light sidebar</b></li>
                    <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                    <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                    <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                    <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                    <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                    <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                    <li><b>With Dark sidebar</b></li>
                    <br/>
                    <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                    <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                    <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>
                    <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                    <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                    <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                </ul>
                <ul class="m-t-20 all-demos">
                    <li><b>Choose other demos</b></li>
                </ul>
                <ul class="m-t-20 chatonline">
                    <li><b>Chat option</b></li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                    </li>
                    <li>
                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- /.right-sidebar -->
</div>
<!-- /.container-fluid -->
<footer class="footer text-center text-info"> 2018 &copy; www.cnas.dz </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
      <script src="<?php echo base_url(); ?>plugins/js/inputmask.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.phone.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/jquery.inputmask.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

    <!-- Typehead Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/typeahead.js-master/dist/typeahead.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/typeahead.js-master/dist/typeahead-init.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
       <!-- Date Picker Plugin JavaScript -->
        <script src="<?php echo base_url(); ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
   <script src="<?php echo base_url(); ?>/plugins/bower_components/sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="<?php echo base_url(); ?>/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
    


    <script>
    jQuery(document).ready(function() {

        // For mask
    
        $('.immatriculation').inputmask('99999-999-99');
        $('.kilometrage').inputmask({ regex: "[0-9]*" });
        $('.naftal').inputmask('9999-9999-9999-9999');
        $('.credit').inputmask({regex : '[0-9]* DA'});
        $('.debit').inputmask({regex : '[0-9]* DA'});

        
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
        // For select 2

       
        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }

        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });

        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        // For multiselect

        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });

        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });


     // Add fleet function
     


    });
    </script>
    <script type="text/javascript">
        var substringMatcher = function(strs) {
  return function findMatches(q, cb) {
    var matches, substringRegex;

    // an array that will be populated with substring matches
    matches = [];

    // regex used to determine if a string contains the substring `q`
    substrRegex = new RegExp(q, 'i');

    // iterate through the pool of strings and for any string that
    // contains the substring `q`, add it to the `matches` array
    $.each(strs, function(i, str) {
      if (substrRegex.test(str)) {
        matches.push(str);
      }
    });

    cb(matches);
  };
};

var marque = ['Audi','Bentley Motors,','BMW','Buick','Cadillac','Chevrolet','Chrysler','Dodge','Ferrari','Fiat','Geely','GM - General Motors','GMC','Honda','Hummer','Hyundai','Infiniti','Isuzu','Jaguar','Jeep','Kia','Laforza','Lamborghini','Lancia','Land Rover','Lexus','Lincoln','Lotus','Maserati','Mazda','Mercedes-Benz','Mercury','MINI','Mitsubishi','Nissan','Oldsmobile','Peugeot','Pontiac','Porsche','Renault','Rolls-Royce','Saab','Saturn','Scion','Subaru','Suzuki','Tesla Motors','Toyota','Volkswagen','Volvo','Daewoo'];

$('#marques .typeahead').typeahead({
  hint: true,
  highlight: true,
  minLength: 1
},
{
  name: 'marques',
  source: substringMatcher(marque)
});
    </script>
    <script>
 

    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'

    });

    $('.clockpicker').clockpicker({
            donetext: 'Done',

        })
        .find('input').change(function() {
            console.log(this.value);
        });

    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }

    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',

    });
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        format: 'dd/mm/yyyy'
    });
      jQuery('.myrange').datepicker({
        toggleActive: true,
        text : 'au'
    });
    jQuery('#datepicker-inline').datepicker({

        todayHighlight: true
    });

    // Daterange picker

    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
    </script> 
    <script type="text/javascript">
        function add_fleet() {

        // Get values from inputs
        alert( $('#nom_chauffeur').val());




                                    //ex : '{"key":"val","key1":"val1"}'
  var data2db = JSON.parse(
                                '{"marque":"' +$('#marque').val()+ 
                                     '","modele":"' +$('#modele').val()+ 
                                     '","nom_chauffeur":"' +$('#nom_chauffeur').val()+ 
                                     '","motorisation":"' +$('#motorisation').val()+ 
                                     '","numero_immatriculation":"' +$('#numero_immatriculation').val()+ 
                                     '","etat":"' +$('#etat').val()+ 
                                     '","gps_tracker":"' +$('#gps_tracker').val()+ 
                                     '","compagnie_assurance":"' +$('#compagnie_assurance').val()+ 
                                     '","numero_immatriculation_old":"' +$('#numero_immatriculation_old').val()+ 
                                     '","designation":"' +$('#designation').val()+ 
                                     '","nom_etablissement":"' +$('#nom_etablissement').val()+ 
                                     '","code_wilaya":"' +$('#code_wilaya').val()+ 
                                     '","type_permis":"' +$('#type_permis').val()+ 
                                     '","validation_permis":"' +$('#validation_permis').val()+ 
                                     '","type_chauffeur":"' +$('#type_chauffeur').val()+ '"}'
                           ); 
  console.log(data2db);
   var url = "fleet_add";
  $.post(url,data2db).done(function( data ) {
    var reply = JSON.parse(data);
    console.log(data.responseText);
    if (reply == 'data inserted') {
      swal("Succès!",'Véhicule ajouté avec succès.', "success");
    }else if (reply == 'error') {
      swal("Erreur...",data.responseText +' || ' +errorThrown, "error");
    }
    // swal("ID CAS", mObj, "success");
  }).fail(function(data, textStatus, errorThrown){
    swal("Erreur...",data.responseText +' || ' +errorThrown, "error");
  });
} 
    </script>   

<!--Style Switcher -->
<script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>

</html>
