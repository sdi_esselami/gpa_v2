<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
    <title>CNAS | Gestion PARC  AUTO</title>
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme" rel="stylesheet">
       <link href="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="fix-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">
        <div id="idle-timeout-dialog" data-backdrop="static" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Your session is expiring soon</h4> </div>
                                        <div class="modal-body">
                                            <p>
                                                <i class="fa fa-warning font-red"></i> You session will be locked in
                                                <span id="idle-timeout-counter"></span> seconds.</p>
                                            <p> Do you want to continue your session? </p>
                                        </div>
                                        <div class="modal-footer text-center">
                                            <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-outline btn-success" data-dismiss="modal">Yes, Keep Working</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="accueil"><b><!--This is dark logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><!--This is dark logo text--><img src="<?php echo base_url(); ?>plugins/images/parcautom-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-text-darkl.png" alt="home" class="light-logo" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <!-- <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form> -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-settings"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="my_profile"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="<?php echo base_url(); ?>plugins/images/user-me.jpg" alt="user-img" class="img-circle"></div>
                        <a href="#" class="dropdown-toggle u-dropdown text-success" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username'); ?><span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="my_profile"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="account_settings"><i class="ti-settings"></i> Options du compte</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                       
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                       
                    </li> -->
                    <li class="nav-small-cap m-t-10 text-danger">--- Menu principal</li>
                    <li> <a href="accueil" class="waves-effect "><i class="icon-home text-danger" ></i> <span class="hide-menu text-danger"> Accueil </span></a></li>
                    <li> <a href="account_settings" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="P"></i> <span class="hide-menu text-danger"> Options du compte </span></a></li>
   
                    
                    <li class="nav-small-cap text-warning">--- Gestion du parc</li>
                    <li><a href="javascript:void(0);" class="waves-effect active"><i class="linea-icon linea-basic fa fa-car text-warning" ></i> <span class="hide-menu text-warning" >Véhicules<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_fleets" class="active">Tous les véhicules</a></li>
                            <li> <a href="add_fleet">Ajouter un véhicule</a></li>
                            <li> <a href="edit_fleet">Modifier un véhicule</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-speedometer text-warning" ></i> <span class="hide-menu text-warning">Chaufeurs<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_drivers">Tous les chaufeurs</a></li>
                            <li> <a href="add_driver">Ajouter un chaufeur</a></li>
                            <li> <a href="edit_driver">Modifier un chaufeur</a></li>
                        </ul>
                    </li>
                    
                    <li class="nav-small-cap text-blue">--- Gestion du personnel</li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-people fa-fw text-blue" ></i> <span class="hide-menu text-blue">Utilisateurs<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_users">Tous les utilisateurs</a></li>
                            <li> <a href="add_user">Ajouter un utilisateur</a></li>
                            <li> <a href="edit_user">Modifier un utilisateur</a></li>
                        </ul>
                    </li>
                   
                    
                    
                    
                    <li><a href="logout" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>
            
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

<!-- Page title goes here --> <h4 class="page-title">Accuiel</h4>

                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
 
                        <ol class="breadcrumb">
                                                <li ><a href="accueil">Accueil</a></li>
                                                <li class="active">Liste des véhicules</li>
<!-- Breadcrumb goes here : Accueil/.....  -->                            
                       
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">
 <!-- Page content starts here -->
                    <div class="col-md-12">
                       <div class="white-box">
                            <h3 class="box-title m-b-0">Liste des vhicules</h3>
                            
                            <br>
                            <div class="table-responsive">
                                <table id="example23" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Marque</th>
                                            <th>Modèle</th>
                                            <th>Motorisation</th>
                                            <th>N° d'immatriculation</th>
                                            <th>Etat général</th>
                                            <th>Affectation</th>
                                            <th>Chauffeur</th>
                                        </tr>
                                    </thead>
                                    <tfoot>
                                        <tr>
                                            <th>Marque</th>
                                            <th>Modèle</th>
                                            <th>Motorisation</th>
                                            <th>N° d'immatriculation</th>
                                            <th>Etat général</th>
                                            <th>Affectation</th>
                                            <th>Chauffeur</th>
                                        </tr>
                                    </tfoot>
                                    <tbody id="tbl">
                                        <?php foreach ($all_fleets as $fleet ) {
                                            echo '<tr>'.
                                            '<td>'.$fleet['marque'] .'</td>'.
                                            '<td>'. $fleet['modele'] .'</td>'.
                                            '<td>'. $fleet['motorisation'] .'</td>'.
                                            '<td>'. $fleet['numero_immatriculation'] .'</td>'.
                                            '<td>'. $fleet['etat'] .'</td>'.
                                            '<td>'. $fleet['etat'] .'</td>'.
                                            '<td>'. $fleet['nom_chauffeur'] .'</td>'

                                            .'</tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>



                    </div>
                </div>
<!-- / Page content ends here -->                
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>
                                <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center text-info"> 2018 &copy; www.cnas.dz </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
       <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!-- Session-timeout-idle -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/jquery.idletimeout.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/jquery.idletimer.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/session-timeout-idle-init.js"></script>
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
       <script src="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
    <!-- end - This is for export functionality only -->
    <script>
    $(document).ready(function() {
        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    $('#example23').DataTable({
        "language": {
            "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
            "zeroRecords": "Aucun résultat trouvé",
            "info": "Affichage du la page _PAGE_ de _PAGES_",
            "infoEmpty": "Table vide!",
            "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
            "sSearch" : "Rechercher",
              "oPaginate": {
            "sFirst":    "Premier",
            "sLast":    "Dernier",
            "sNext":    "Suivant",
            "sPrevious": "Précédent"
        }
        } ,

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
function get_users() {
                  var data2db = JSON.parse('{"username":"'+ 'SDI_OFFICINES' +'"}'); //ex : '{"key":"val","key1":"val1"}'
                  var url = "Controller/get_all_users";
                  $.post(url,data2db).done(function( data ) {
                    var all_users = JSON.parse(data);
                    console.log(all_users);
                    // Draw td
                    for (var i = 0; i < all_users.length; i++) {
                    console.log(all_users[i]);
                      $("#table_users_ tbody").append('<tr>'+
                        '<td style="width:1px"><div style="margin-top:5%" id="div_'+all_users[i]['IdUser']+'-'+all_users[i]['IdUser']+'">'+
                        '<div style="height:23px;width:25px"class="btn-group" data-toggle="buttons" id="group_'+all_users[i]['IdUser']+'-'+all_users[i]['IdUser']+'">'+
                        '<label style="height:27px;width:5px;margin-top:-5px" class="btn btn-default lol all_users_check_ cat_'+all_users[i]['idusercat']+'"id="user_'+all_users[i]['IdUser']+'-'+all_users[i]['username']+ '@cnas.dz" onclick="user_check(\''+'userLabel_'+all_users[i]['IdUser']+'-'+all_users[i]['idusercat']+'\');">'+
                        '<input style="background-color:rgb(255, 255, 255);"type="checkbox" name="options"  autocomplete="off">'+
                        '<span style="color:rgb(255, 255, 255);top:-2px;margin-left:-7px;" class="glyphicon glyphicon-ok "></span>'+
                        '</label>'+
                        '</div>'+
                        '</div></td>'+
                        '  <td class="mailbox-date " style="">'+all_users[i]['Nom_prenom']+'</td>'+
                        '  <td class="mailbox-date " style="">'+all_users[i]['username']+'</td>'+
                        '  <td class="mailbox-date " style="">'+all_users[i]['categorie']+'</td>'+
                        '</tr>');
                      }

                      // Draw table
                      $('#table_users_').DataTable({
                        "paging": false,
                        "destroy": true,
                        "processing": true,
                        "deferRender": true,
                        "lengthChange": false,
                        "searching": true,
                         "scrollY": "350px",
                        "scrollCollapse": true,
                        "columnDefs": [
                          { "orderable": true,"targets":[1,2]},
                        ],
                        select: {
                          style:    'os',
                          selector: 'td:first-child'
                        },
                        "info": false,
                        "autoWidth": true,
                        "language": {
                          "paginate": {
                            "previous": "Précédent",
                            "next": "Suivant",
                          },
                          "sSearch": "Rechercher",
                          "sInfoEmpty": "",
                          "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                          "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                          "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau"
                        }
                      });





    </script>
    
    <!--Style Switcher -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
   
</body>

</html>
