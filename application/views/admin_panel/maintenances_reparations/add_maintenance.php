<div class=" col-md-12 animated" id="maintenance_panel" style="display:none;" >
  <div class="panel " style="background-color: #BEBEBE;" >
    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
    ENTRETIENS  </a>
    <div class="pull-right">
      <a href="javascript;" data-perform="panel-collapse">
      <i class="ti-plus"></i></a>
    </div>
  </div>
  <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
    <div class="row">
      <div class="col-md-9">
        <h5 class="m-t-30 m-b-10"><b>TYPE ENTRETIEN (S)</b></h5>
        <select class="selectpicker show-tick entretien" multiple  title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="type_entretien" disabled="true" size="true" data-size="4">
          <?php foreach ($entretiens as $entretien)
          {
          echo '<option';
            //echo ' value = "'.$entretien['Id'];
            echo '> ';
          echo $entretien['nom'].'</option>';
          } ?>
        </select>
      </div>
      <div class=" col-sm-3">
        <h5 class="m-t-30 m-b-10"><b>COÛT</b></h5>
        
        <input class="form-control entretien cout" type="text" id="cout" data-error="Veuillez renseigner ce champ." required disabled="true">
        <div class="help-block with-errors"></div>
      </div>
      <div class=" col-sm-4">
        <h5 class="m-t-30 m-b-10"><b>DATE</b></h5>
        
        <input type="text" class="form-control entretien datepicker-autoclose" placeholder="jj/mm/aaaa" disabled="true" id="date_entretien">
        
        
        
      </div>
      <div class=" col-sm-4">
        <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
        
        <input class="form-control entretien numero" type="text" id="kilometrage_entretien" data-error="Veuillez renseigner ce champ." required disabled="true">
        <div class="help-block with-errors"></div>
      </div>
      
      
    </div>
    
    <hr>
    <div class="row ">
      <div class=" col-sm-12 ">
        <div class="text-right">
          
          
          <button class="fcbtn btn btn-danger btn-outline btn-1c "
          style="width:20%" id="cancel_maintenance_btn" onclick="enable_elements('entretien');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
          <button class="entretien fcbtn btn btn-success btn-outline btn-1c "
          style="width:20%" id="add_driver_btn" onclick="add_maintenance()" disabled="true">Sauvgarder</button>
        </div>
        
      </div>
      
    </div>
  </div>
</div>

</div>