 <?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
       <title>CNAS | Gestion PARC  AUTO</title>
    <!-- Bootstrap Core CSS -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

    <link href="<?php echo base_url(); ?>plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/js/sweetalert2.css">

    <!-- Date picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo base_url(); ?>plugins/js/jquery.filer.css" rel="stylesheet">

    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme" rel="stylesheet">
           <link href="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
            #btn-close-modal {
                width:100%;
                text-align: center;
                cursor:pointer;
                color:#fff;
            }

        </style>
</head>

<body class="fix-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">



        <div id="idle-timeout-dialog" data-backdrop="static" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Your session is expiring soon</h4> </div>
                                        <div class="modal-body">
                                            <p>
                                                <i class="fa fa-warning font-red"></i> You session will be locked in
                                                <span id="idle-timeout-counter"></span> seconds.</p>
                                            <p> Do you want to continue your session? </p>
                                        </div>
                                        <div class="modal-footer text-center">
                                            <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-outline btn-success" data-dismiss="modal">Yes, Keep Working</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="accueil"><b><!--This is dark logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><!--This is dark logo text--><img src="<?php echo base_url(); ?>plugins/images/parcautom-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-text-darkl.png" alt="home" class="light-logo" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <!-- <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form> -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-settings"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="<?php echo base_url(); ?>plugins/images/user-me.jpg" alt="user-img" class="img-circle"></div>
                        <a href="#" class="dropdown-toggle u-dropdown text-success" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username'); ?><span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="javascript:void(0);"><i class="ti-settings"></i> Options du compte</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                       
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                       
                    </li> -->
                    <li class="nav-small-cap m-t-10 text-danger">--- Menu principal</li>
                    <li> <a href="accueil" class="waves-effect "><i class="icon-home text-danger" ></i> <span class="hide-menu text-danger"> Accueil </span></a></li>
                   <li> <a href="account_settings" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="P"></i> <span class="hide-menu text-danger"> Options du compte </span></a></li>


                    <li class="nav-small-cap text-warning">--- Gestion du parc</li>
                    <li><a href="javascript:void(0);" class="waves-effect "><i class="linea-icon linea-basic fa fa-car text-warning" ></i> <span class="hide-menu text-warning" >Véhicules<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_vehicles" >Tous les véhicules</a></li>
                            <li> <a href="add_vehicle_page">Ajouter un véhicule</a></li>
                            <li> <a href="edit_vehicle_page">Modifier un véhicule</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect "><i class="icon-speedometer text-warning" ></i> <span class="hide-menu text-warning">Chauffeurs<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_drivers" class="">Tous les chauffeurs</a></li>
                            <li> <a href="add_driver_page">Ajouter un chaufeur</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect active"><i class="icon-equalizer text-warning" ></i> <span class="hide-menu text-warning">Entretiens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_maintenances_page" class="active">Liste des entretiens</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="wi wi-fire  text-warning" ></i> <span class="hide-menu text-warning">Accidents<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_accidents_page" class="">Liste des accidents</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-wrench text-warning" ></i> <span class="hide-menu text-warning">Reparations<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparations_page" class="">Liste des Reparations</a></li>
                        </ul>
                    </li>

                    <li class="nav-small-cap text-blue">--- Gestion des tables</li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-shield text-blue" ></i> <span class="hide-menu text-blue">Assurances<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="compagnies_assurances">Liste des compagnies</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);" class="waves-effect"><i class=" ti-hummer text-blue" ></i> <span class="hide-menu text-blue">Mécaniciens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparateurs_page">Liste des mécaniciens</a></li>
                        </ul>
                    </li>
                 

                    
                    
                    
                    <li><a href="logout" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>
            
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

<!-- Page title goes here --> <h4 class="page-title">Entretiens</h4>

                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
 
                        <ol class="breadcrumb">
                                                <li ><a href="accueil">Accueil</a></li>
                                                <li class="active">Liste des entretiens</li>
<!-- Breadcrumb goes here : Accueil/.....  -->                            
                       
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <div class="row">

 <!-- Page content starts here -->

                    <div class="col-md-12">

                        <div class="panel panel-default">
                            <div class="panel-heading">Liste des entretiens
                                <div class="panel-action">
                                              <a class="text-success" data-toggle="modal" data-target="#modal_maintenance" href="javascript:void(0)" >
                            <i class="ti-plus" style="font-size: 13px"></i>&nbsp;Ajouter </a>
                                </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                     <div class="table table-hover ">
                                <table id="example23" class="table hover-table table-default" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="btn-default">
                                            <th class="">MARQUE</th>
                                            <th class="text-center">MODELE</th>
                                            <th style="width: 45%">N° IMMATRICULATION</th>
                                            <!-- <th class="text-center">MOTORISATION</th> -->
                                            <th class="text-center">KILOMETRAGE</th>
                                            <th class="">ENTRETIENS</th>
                                            <th class="text-center">DATE</th>
                                            <th class="">COÛT</th>
                                            <th class="">MÉCANICIEN</th>

                                        </tr>
                                    </thead>
                                
                                    <tbody id="tbl" style="cursor: pointer;">
                                        <?php foreach ($maintenances as $maintenance) {
                                            // echo '<div class="dropdown">'; 
                                                echo '<tr>';

                                                // echo '<td data-toggle="dropdown">' ;
                                                //   echo '<ul class="dropdown-menu" >';
                                                //    echo '<li><a href="#">HTML</a></li>';
                                                //      echo' <li><a href="#">CSS</a></li>';
                                                //       echo'<li><a href="#">JavaScript</a></li>';
                                                //   echo ' </ul></div></td>';

     

                 // echo '<td style="display:none;">' . $chauffeur['id']. '</td>' ;
                 echo '<td >' . $maintenance['marque']. '</td>' ;
                 echo '<td >' . $maintenance['modele']. '</td>' ;
                 echo '<td class="">' . $maintenance['numero_immatriculation']. '</td>' ;
                 echo '<td >' . $maintenance['kilometrage']. '</td>' ;
                 echo '<td >' . $maintenance['entretien']. '</td>' ;
                 echo '<td >' . $maintenance['date_entretien']. '</td>' ;
                 echo '<td style="width:150px">' . $maintenance['cout']. '</td>' ;
                 echo '<td >' . $maintenance['reparateur']. '</td>' ;
                                             
                                             echo '</tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                                </div>
                                <!-- <div class="panel-footer"> Panel Footer </div> -->
                            </div>
                        </div>





                            <br>
                           

                            <br>
                            
                            
                                


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" id="modal_maintenance" data-animation="true" >
                                <div class="modal-dialog modal-md" >
                                    <div class="modal-content" style="width: 115%; margin-left: -7%; margin-top: 15%">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myLargeModalLabel">Ajouter un entretien</h4>
                                        </div>
                                        <div class="modal-body">
                                    <div class="row"> 
                         <div class="col-lg-12">
                <h5 class="m-t-30 m-b-10"><b>VEHICULE</b></h5>

        <select class="selectpicker show-tick entretien"  title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="id_vehicle" data-live-search="true"  >




                        <?php foreach ($vehicles as $vehicle)
                                                {
                          echo '<option';
                          echo ' value = "'.$vehicle['ID'];
                          echo '"> ';
                          echo $vehicle['marque'].' - '; 
                          echo $vehicle['modele'].' - '; 
                          echo $vehicle['numero_immatriculation'].' - '; 
                          echo $vehicle['motorisation']; 
                          echo '</option>';
                                                } ?>
                                    </select>
                                </div>

                         <div class="col-lg-12">
                <h5 class="m-t-30 m-b-10"><b>TYPE ENTRETIEN (S)</b></h5>

        <select class="selectpicker show-tick entretien" multiple  title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="type_entretien"  size="true" data-size="4">




                        <?php foreach ($type_entretiens as $type_entretien)
                                                {
                          echo '<option';
                          //echo ' value = "'.$type_entretien['Id'];
                          echo '> ';
                          echo $type_entretien['nom'].'</option>';
                                                } ?>
                                    </select>
                                </div>
                             <div class="col-lg-12">
                <h5 class="m-t-30 m-b-10"><b>REPARATEUR/MÉCANICIEN</b></h5>

        <select class="selectpicker show-tick entretien" multiple  title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="id_reparateur"  size="true" data-size="4">




                        <?php foreach ($reparateurs as $reparateur)
                                                {
                          echo '<option';
                          echo ' value = "'.$reparateur['id'];
                          echo '"> ';
                          echo $reparateur['nom'].' - ';
                          echo $reparateur['telephone'];
                          echo '</option>';
                                                } ?>
                                    </select>
                                </div>                            
                        <div class=" col-lg-4">
                         <h5 class="m-t-30 m-b-10"><b>COÛT</b></h5> 
                         
                          <input class="form-control entretien credit" type="text" id="cout" data-error="Veuillez renseigner ce champ." required >
                        <div class="help-block with-errors"></div>                        
                  </div> 
                <div class=" col-lg-4">
                         <h5 class="m-t-30 m-b-10"><b>DATE</b></h5> 
                         
            <input type="text" class="form-control entretien datepicker-autoclose" placeholder="jj/mm/aaaa"  id="date_entretien">
       
                                        
                          
                  </div> 
                  <div class=" col-lg-4">
                         <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5> 
                         
                          <input class="form-control entretien numero" type="text" id="kilometrage_entretien" data-error="Veuillez renseigner ce champ." required >
                        <div class="help-block with-errors"></div>                        
                  </div>

                  
         
                </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Annuler</button> 
                                            <button type="button" class="btn btn-success waves-effect text-left"  onclick="add_maintenance()">Ajouter</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>





          


                  
                </div>




                </div>


                </div>
<!-- / Page content ends here -->                
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>
                                <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center text-info"> 2018 &copy; www.cnas.dz </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
<!-- jQuery -->
<script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.phone.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/jquery.inputmask.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>



<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/plugins/js/sweetalert2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/jquery.filer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/validator.js"></script>
       <script src="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <!-- <script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
    <script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>-->
        <script src="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>



<script>



    $("#decision_form").submit(function(){

    var posting =    $.post($(this).attr("action"), $(this).serialize());

    // Put the results in a div
    posting.done(function( data ) {
      swal("Succès!", "Informations enregistrées avec succès.", "success");

    //  $('#filer_input_modal').prop("jFiler").reset();
    //  $("#__compose_users").select2('val', '');
     
     // back_to_msgs();

    });
  });

    
    
    


$(document).ready(function() {
        //$('#example23').footable();
    var table = $('#example23').DataTable({
        "language": {
            "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
            "zeroRecords": "Aucun résultat trouvé",
            "info": "Affichage du la page _PAGE_ de _PAGES_",
            "infoEmpty": "Table vide!",
            "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
            "sSearch" : "Rechercher",
              "oPaginate": {
            "sFirst":    "Premier",
            "sLast":    "Dernier",
            "sNext":    "Suivant",
            "sPrevious": "Précédent",
            "responsive": true
        }
        } ,
        dom: 'Bfrtip',
         buttons: [
            {
                extend: 'print',
                text: 'IMPRIMER',
                orientation: 'landscape'
                
            },
            {
                extend: 'excelHtml5',
                text:      'EXCEL'
                
               
                 
            },
            {
                extend: 'pdfHtml5',
                text: 'PDF',
                orientation: 'landscape'
                
                
            },
        ]
    });
 
$('#example23 tbody').on( 'click', 'tr', function () {
    console.log( table.row( this ).data()[3] );
} );


    // enable fileuploader plugin
    $('#filer_input').filer({
    showThumbs: true,
    addMore: false,
    allowDuplicates: false,
    limit:1,
    extensions: ["pdf","png","jpg","bmp","jpeg","tiff"],
    captions: {
      button: "Parcourir",
      feedback: "Sélectionner la décision scannée..",
      feedback2: "fichier a été choisi",
      drop: "Drop file here to Upload",
      size: "taille",
      removeConfirmation: "Voulez-vous vraiment supprimer ce fichier?",
      errors: {
        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
        filesType: "Veuillez sélectionner uniquement des fichiers imgaes ou pdf.",
        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
        folderUpload: "Vous n'êtes pas autorisé à sélectionner des dossiers."
      }
    }
  });
     
 
    
});


    jQuery(document).ready(function() {
    
        // For mask

        $('.immatriculation').inputmask('99999-999-99');
        $('.immatriculation_old').inputmask('99999-99-99');
        $('.kilometrage').inputmask({ regex: "[0-9]*" });
        $('.naftal').inputmask('9999-9999-9999-9999');
        $('.credit').inputmask({regex : '[0-9]*(.)?[0-9][0-9] DA'});

        $('.debit').inputmask({regex : '[0-9]* DA'});
        $('.numero').inputmask({regex : '[0-9]*'});

        
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
        // For select 2


        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }

        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });

        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        // For multiselect

        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });

        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });


     // Add fleet function
     


 });
</script>

<script>
function edit_driver(nom,id,prenom,type,validation_permis) {

        $('#modal_driver').find('#nom_chauffeur').val(nom);
        $('#modal_driver').find('#prenom_chauffeur').val(prenom);
        $('#modal_driver').find('#validation_permis').val(validation_permis);
        $('#selected_driver').val(id); 
        type = type.split(",");
        $('#modal_driver').find('#type_permis').selectpicker('val',type);
        
}

    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'

    });

    $('.clockpicker').clockpicker({
        donetext: 'Done',

    })
    .find('input').change(function() {
        console.log(this.value);
    });

    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
        .clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }

    // Date Picker
    jQuery('.mydatepicker, #datepicker, #date_entretien').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',

    });
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
         format: 'dd/mm/yyyy',
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        format: 'dd/mm/yyyy'
    });
    jQuery('.myrange').datepicker({
        toggleActive: true,
        text : 'au'
    });
    jQuery('#datepicker-inline').datepicker({

        todayHighlight: true
    });

    // Daterange picker

    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
</script> 


 
    
    <!--Style Switcher -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
   
</body>

</html>
                     




             <script type="text/javascript">
                function disable_elements(_class) {
                   // $('.'+_class).attr('disabled',true);
                    $('.'+_class).val('');
                    $('.'+_class).selectpicker('val','');
                    $('.selectpicker').selectpicker('refresh');
                }
               function add_maintenance() {


         var id_vehicle = $('#id_vehicle').selectpicker('val');           
                    
        // Get values from inputs
            
            
            var type_entretien = $('#type_entretien').val();
            var date_entretien = $('#date_entretien').val();
            var cout = $('#cout').val();
            var kilometrage = $('#kilometrage_entretien').val();
            var id_reparateur = $('#id_reparateur').selectpicker('val');
            
            if (type_entretien != "" &&  date_entretien != "" && cout != ""  ) {

                        if (cout.indexOf("_") < 0) {

                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"type_entretien":"' +type_entretien+ 
                                        '","date_entretien":"' +date_entretien+ 
                                        '","kilometrage":"' +kilometrage+ 
                                        '","id_vehicle":"' +id_vehicle+ 
                                        '","id_reparateur":"' +id_reparateur+ 
                                        '","cout":"' +cout+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "add_maintenance";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'data inserted') {
                                             // swal({
                                             //      title: "Succès!",
                                             //      text: "Information ajoutée avec succès",
                                             //      type: "success",
                                             //      confirmButtonColor: "#DD6B55",
                                             //      confirmButtonText: "Terminer",
                                             //       closeOnConfirm: true
                                             //            });
                                                   
                                                swal({
  title: "Succès",
  text: "Information ajoutée avec succès!",
  type: "success",
  showCancelButton: false,
  confirmButtonClass: "btn-success",
  confirmButtonText: "Terminer",
  cancelButtonText: "No, cancel plx!",
  closeOnConfirm: false,
  closeOnCancel: false
},
function(isConfirm) {
  if (isConfirm) {
    disable_elements('entretien');
    location.reload();
  } else {
    swal("Cancelled", "Your imaginary file is safe :)", "error");
  }
});
                                                
                                                                                       //  $('#modal_maintenance').modal('hide');
                                                  //  
                                             
                                             

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       

     }else {alert('Format du champ coût : 9999,00')}                                 
    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");   
    }    
             </script>