

<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
    <title>CNAS | Gestion PARC  AUTO</title>
    <!-- Bootstrap Core CSS -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />
>
    <link href="<?php echo base_url(); ?>plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/js/sweetalert2.css">

    <!-- Date picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo base_url(); ?>plugins/js/jquery.filer.css" rel="stylesheet">

    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme" rel="stylesheet">


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="fix-sidebar" >
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper" >
        <div id="idle-timeout-dialog" data-backdrop="static" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Your session is expiring soon</h4> </div>
                        <div class="modal-body">
                            <p>
                                <i class="fa fa-warning font-red"></i> You session will be locked in
                                <span id="idle-timeout-counter"></span> seconds.</p>
                                <p> Do you want to continue your session? </p>
                            </div>
                            <div class="modal-footer text-center">
                                <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-outline btn-success" data-dismiss="modal">Yes, Keep Working</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Top Navigation -->
                <nav class="navbar navbar-default navbar-static-top m-b-0">
                    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                        <div class="top-left-part"><a class="logo" href="accueil"><b><!--This is dark logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><!--This is dark logo text--><img src="<?php echo base_url(); ?>plugins/images/parcautom-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-text-darkl.png" alt="home" class="light-logo" /></span></a></div>
                        <ul class="nav navbar-top-links navbar-left hidden-xs">
                            <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                            <li>
                        <!-- <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form> -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-settings"></i>
                      <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                  </a>
                  <ul class="dropdown-menu animated flipInY">
                    <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                    <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                    <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                    <!-- <li role="separator" class="divider"></li> -->
                    <!-- <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                    <li role="separator" class="divider"></li>
                    <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="<?php echo base_url(); ?>plugins/images/user-me.jpg" alt="user-img" class="img-circle"></div>
                        <a href="#" class="dropdown-toggle u-dropdown text-success" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username'); ?><span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="javascript:void(0);"><i class="ti-settings"></i> Options du compte</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                       
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
        </span> </div></li> -->



        <!-- Change active menu : default accueil is active --> 


        <li class="nav-small-cap m-t-10 text-danger">--- Menu principal</li>
        <li> <a href="accueil" class="waves-effect"><i class="icon-home text-danger" ></i> <span class="hide-menu text-danger"> Accueil </span></a></li>
        <li> <a href="account_settings" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="P"></i> <span class="hide-menu text-danger"> Options du compte </span></a></li>


                    <li class="nav-small-cap text-warning">--- Gestion du parc</li>
                    <li><a href="javascript:void(0);" class="waves-effect active "><i class="linea-icon linea-basic fa fa-car text-warning" ></i> <span class="hide-menu text-warning" >Véhicules<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_vehicles" >Tous les véhicules</a></li>
                            <li> <a href="add_vehicle_page" class="active">Ajouter un véhicule</a></li>
                            <li> <a href="edit_vehicle_page">Modifier un véhicule</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect "><i class="icon-speedometer text-warning" ></i> <span class="hide-menu text-warning">Chauffeurs<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_drivers" class="">Tous les chauffeurs</a></li>
                            <li> <a href="add_driver_page">Ajouter un chaufeur</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-equalizer text-warning" ></i> <span class="hide-menu text-warning">Entretiens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_maintenances_page" class="">Liste des entretiens</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="wi wi-fire  text-warning" ></i> <span class="hide-menu text-warning">Accidents<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_accidents_page" class="">Liste des accidents</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-wrench text-warning" ></i> <span class="hide-menu text-warning">Reparations<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparations_page" class="">Liste des Reparations</a></li>
                        </ul>
                    </li>

                    <li class="nav-small-cap text-blue">--- Gestion des tables</li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-shield text-blue" ></i> <span class="hide-menu text-blue">Assurances<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="compagnies_assurances">Liste des compagnies</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);" class="waves-effect"><i class=" ti-hummer text-blue" ></i> <span class="hide-menu text-blue">Mécaniciens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparateurs_page">Liste des mécaniciens</a></li>
                        </ul>
                    </li>



        <li><a href="logout" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>

    </ul>
</div>
</div>
<!-- Left navbar-header end -->
<!-- Page Content -->
<div id="page-wrapper" style="background-color: #E5E5E5">

    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

                <!-- Page title goes here --> <h4 class="page-title">Ajouter un véhicule</h4>
                    
            </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">

                <ol class="breadcrumb">
                    <li ><a href="accueil">Accueil</a></li>
                    <li class="active">Véhicules</li>
                    <li class="active">Ajouter</li>
                    <!-- Breadcrumb goes here : Accueil/.....  -->                            

                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <div class="row">


           <!-- Page content starts here -->


  <div class="col-sm-12">
    <div class="white-box" style="border-color: #FFE7C2;border-style: dashed;">
        <h3 class="box-title m-b-0"><b>RESEIGNEMENTS DU VEHICULE</b></h3><small style="color:red"> (*) champs obligatoires</small>
        <!-- <form data-toggle="validator" > -->
    <hr>
        <div class="row ">
<!--             <div class="col-sm-4 form-group">
             <h5 class="m-t-30 m-b-10"><b>MARQUE</b></h5> 
             <div id="marques">
              <input class="typeahead form-control" type="text" placeholder="HYUNDAI, BMW,..." id="marque" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>
          </div>
      </div> -->

      <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>MARQUE</b><small style="color:red"> *</small></h5>
                                    <select class="selectpicker show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control" id="marque"
                                    onchange="" size="true" data-size="5">
                                            <?php foreach ($marques as $marque)
                                                {
                                                  echo '<option >'. $marque['brand']. '</option>';
                                                } ?>
                                    </select>
                                </div>

      <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>MODELE</b><small style="color:red"> *</small></h5>
        <input class="form-control" type="text" placeholder="ELANTRA, ACCENT,..." id="modele" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>   </div>
    <div class="col-md-4">
        <h5 class="m-t-30 m-b-10"><b>MOTORISATION</b><small style="color:red"> *</small></h5>
        <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="motorisation" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div> 
            <option>DIESEL</option>
            <option>ESSENCE</option>
            <option>GPL</option>
            <option>ESSENCE / GPL</option>
        </select>
    </div>
    <div class="col-md-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>TYPE</b><small style="color:red"> *</small></h5>
        <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="type_vehicle" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>           <div class=""></div>
            <option>AMBULANCE</option>
            <option>ENGIN</option>
            <option>CAMION</option>
            <option>VEHICULE TOURISTIQUE</option>
            <option>VEHICULE UTILITAIRE</option>
            <option>CAMION</option>
        </select>
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>VEHICULE TRACKER PAR GPS</b><small style="color:red"> *</small></h5>
        <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control" id="gps_tracker" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>           <div class=""></div>
            <option>OUI</option>
            <option>NON</option>

        </select>
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>ETAT GENERAL DU VEHICULE</b><small style="color:red"> *</small></h5>
        <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control" id="etat" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>           
            <option class="text-success">NEUF</option>
            <option style="color :#D2FF00 ">BON ETAT</option>
            <option class="text-warning">ETAT MOYEN</option>
            <option class="text-danger">MAUVAIS ETAT</option>
        </select>
    </div>

                     <!--            <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input class="form-control kilometrage" id="kilometrage" >
                                </div>  -->   
                            </div>
                            <div class="row ">
                                <div class="col-sm-4 form-group">
                                 <h5 class="m-t-30 m-b-10"><b>N° CHASSIS</b><small style="color:red"> *</small></h5> 
                                 
                                 <input class=" form-control" type="text"  maxlength="50" id="numero_chassis" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>                             </div>
                             <div class="col-sm-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION</b><small style="color:red"> *</small></h5>
                                <input class="form-control "  
                                id="numero_immatriculation" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>                           </div>
                            <div class="col-md-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION (Ancienne/Provisoire)</b><small style="color:red"> *</small></h5>
                                <input class="form-control "  
                                id="numero_immatriculation_old" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>                           </div>


                        </div>  
                        <hr style='background-color:#FFCD94;'> 
<h3 class="box-title m-b-0 "><b>DETAIL CARTE NAFTAL</b></h3>
                   <div class="row"> 

                        <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>NUMERO CARTE</b></h5> 
                         
                          <input class="form-control information numero" type="text" id="numero_carte" data-error="Veuillez renseigner ce champ." required >
                        <div class="help-block with-errors"></div>                        
                  </div> 
                <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>DATE EXPIRATION</b></h5> 
                         
                          <input class="form-control datepicker" type="text" id="expiration_carte" data-error="Veuillez renseigner ce champ." required >
                        <div class="help-block with-errors"></div>                        
                  </div>
                   <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>DEBIT</b></h5> 
                         
                      <input class="form-control information cout" type="text" id="debit_carte" data-error="Veuillez renseigner ce champ." required >
                        <div class="help-block with-errors"></div> 
                  </div> 
                   <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>CREDIT</b></h5> 
                         
                      <input class="form-control information cout" type="text" id="credit_carte" data-error="Veuillez renseigner ce champ." required >
                        <div class="help-block with-errors"></div> 
                  </div> 
                  
         
                </div>
                        <br>
                        <hr style='background-color:#FFCD94;'> 
             <h3 class="box-title m-b-0"><b>ETAT DETAILLE DU VEHICULE</b></h3>
        <!-- <form data-toggle="validator" > -->
   
        <div class="row " id="row_exterior_status">
            <div class="col-sm-12 form-group">
                <hr>
                <h4 class="box-title m-b-0 text-muted">ETAT EXTERIEUR</h4> 
            </div>
        <br>
        <?php foreach ($exterior_parts as $exterior_part) {
           echo '<div class="col-sm-6 form-group" id="'.$exterior_part['id'].'_div_exterior_status">';
        echo '<h5 class="m-t-30 m-b-10" id="'.$exterior_part['id'].'_exterior_part_name"><b>'.$exterior_part['part'].'</b></h5>';
        echo '<input class="form-control" type="text" placeholder="Insérer détail..." id="exterior_part_status_'.$exterior_part['id'].'" data-error="Veuillez renseigner ce champ." required><div class="help-block with-errors"></div></div>' ;
        } ?>
      <!-- <div class="col-sm-6 form-group" id="div_exterior_status">
        <h5 class="m-t-30 m-b-10" id="_exterior_part_name"><b>CAPOT</b></h5>
        <input class="form-control" type="text" placeholder="Insérer détail..." id="_exterior_part_status" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>  
      </div> -->


    
      </div>

      <div class="row ">
            <div class="col-sm-12 form-group">
            <hr style='background-color:#FFCD94;'> 
                <h4 class="box-title m-b-0 text-muted">ETAT INTERIEUR</h4> 
            </div>
        <br>
         <?php foreach ($interior_parts as $interior_part) {
           echo '<div class="col-sm-6 form-group" id="'.$interior_part['id'].'_div_interior_status">';
        echo '<h5 class="m-t-30 m-b-10" id="'.$interior_part['id'].'_interior_part_name"><b>'.$interior_part['part'].'</b></h5>';
        echo '<input class="form-control" type="text" placeholder="Insérer détail..." id="interior_part_status_'.$interior_part['id'].'" data-error="Veuillez renseigner ce champ." required><div class="help-block with-errors"></div></div>' ;
        } ?>


      
  </div>                   
                        <div class="row "> 
                         <div class=" col-sm-12 form-group">
                            <div class="text-right">

                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:25%" id="cancel_add_vehicle_btn" onclick="window.location='<?php echo base_url(); ?>all_vehicles';">Retourner à la page Véhicles</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                <button class="fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_vehicle_btn" onclick="add_vehicle()">Ajouter</button>
                            </div> 
                        </div>
                    </div>
<!-- </form> -->
                </div>  

                </div>
   

             
            <div class=" col-md-12 " id="assurance_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'assurance du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
      <div class="col-md-6">
        <h5 class="m-t-30 m-b-10"><b>COMPAGNIE D'ASSURANCE</b></h5>

        <select class="selectpicker show-tick assurance" title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="compagnie_assurance" onchange="$('#adresse').val($('#compagnie_assurance').find('option:selected').attr('adresse'));
        $('#telephone').val($('#compagnie_assurance').find('option:selected').attr('telephone'));
        $('#numero_police').val($('#compagnie_assurance').find('option:selected').attr('numero_police'));"
        >


                                            <?php foreach ($compagnies as $compagnie)
                                                {
                          echo '<option';
                          echo ' value = "'.$compagnie['id'];
                          echo '" adresse = "'.$compagnie['adresse'] ;
                          echo  '" telephone='. $compagnie['telephone'];
                          echo  '" numero_police='. $compagnie['numero_police'];
                          echo ' >';
                          echo $compagnie['nom'].'</option>';
                                                } ?>
                                    </select>
                                </div>
                  <div class="col-sm-3">
                     <h5 class="m-t-30 m-b-10"><b>NUMERO POLICE D'ASSURANCE</b></h5> 
                     <div id="car-brands">
                      <input class="form-control numero " type="text" id="numero_police" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                 </div>   
              </div>
                <div class="col-sm-3">
                     <h5 class="m-t-30 m-b-10"><b>NUMERO TELEPHONE</b></h5> 
                     <div id="car-brands">
                      <input class="form-control numero " type="text" id="telephone" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                 </div>   
              </div>
                <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>ADRESSE</b></h5> 
                         
                          <input class="form-control " type="text" id="adresse" data-error="Veuillez renseigner ce champ." required readonly="true">
                        <div class="help-block with-errors"></div>                        
                  </div>
              <div class="col-sm-6">
                <h5 class="m-t-30 m-b-10 "><b>DATE D'ECHEANCE</b></h5>
                <div class="input-daterange input-group" id="date-range">
                    <span class="input-group-addon bg-info b-0 text-white ">Du</span>
                    <input type="text" class="form-control assurance" name="start" id="date_debut" />
                    <span class="input-group-addon bg-info b-0 text-white">au</span>
                    <input type="text" class="form-control assurance" name="end" id="date_fin" />
                </div>
                    </div> 
                </div>
                
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_inssurance_btn" data-perform="panel-collapse">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                <button class="fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_inssurance_btn" onclick="add_inssurance()">Ajouter</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div>   

             <div class=" col-md-12 " id="affectation_panel" style="display: none;">
                <div class="panel " style="background-color: #BEBEBE;">

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'affectation du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;" id="affectation_collapse">
                       <div class="row"> 

                               <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>DESIGNATION</b></h5>
                                    <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="designation"
                                    onchange="show_structure(this.value)">
                                            <?php foreach ($types as $type)
                                                {
                                                  echo '<option >'. $type['nom']. '</option>';
                                                } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>NOM</b></h5>
                                    <select class="selectpicker show-tick " data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="structure_affectation" data-hide-disabled= 'true' onchange="get_wilaya()">
                                                                                   
                                        <?php foreach ($types as $type)
                                                {
                                                    echo '<optgroup disabled = "true" label="' .$type['nom']. '">';  
                                                  foreach ($structures as $structure) {
                                                      if ($structure['type'] == $type['nom'] ) {
                                                        
                                                        echo '<option value ="' .$structure['id']. '" code_wilaya ="' .$structure['code_wilaya']. '" >'. $structure['nom'] . '</option>';
                                                          
                                                      }
                                                  }
                                                  
                                                } echo '</optgroup>'; ?>
                                    </select>

                                    <script type="text/javascript">
                                        function show_structure(type) {
                                            $('#code_wilaya').val('');    
                                           $('.selectpicker').find('[label]').attr('disabled',true);              
                                           $('.selectpicker').find("[label='"+type+"']").attr('disabled',false);
                                           $('.selectpicker').selectpicker('refresh');}

                                        function get_wilaya() {
                                            $('#code_wilaya').val('');
                                            var value = $('#structure_affectation').val();
                                            var code_wilaya = $('.selectpicker').find("[value='"+value+"']" ).attr('code_wilaya'); 
                                               $('#code_wilaya').val(code_wilaya);
                                           }   
                                       </script>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>CODE WILAYA</b></h5>
                                    <input type="text" class="form-control" id="code_wilaya" readonly="true" />
                                    <input type="text" class="form-control" id="id_inserted_vehicle" readonly="true" style="display: none;" />
                                </div>
                                <div class="col-md-9">
                                    <h5 class="m-t-30 m-b-10"><b>DETAIL AFFECTATION</b></h5>
                                    <select class="selectpicker test show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="detail_affectation" data-hide-disabled= 'true'>
                                                 <?php foreach ($types as $type)
                                                {
                                                    echo '<optgroup disabled = "true" label="' .$type['nom']. '">';  
                                                  foreach ($priorietaires as $priorietaire) {
                                                      if ($priorietaire['type'] == $type['nom'] ) {
                                                        
                                                        echo '<option value ="' .$priorietaire['id']. '" >'.
                                                         $priorietaire['nom'] . '</option>';
                                                          
                                                      }
                                                  }
                                                  
                                                } echo '</optgroup>'; ?>

                                    </select>
                                   
                                 
                                </div> 
                                <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input type="text" class="form-control numero" id="kilometrage" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>
                                </div>
                                 <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>TYPE AFFECTATION</b></h5>
                                    
                                    <select class="selectpicker show-tick" title="Sélectionner.." data-style="form-control"  id="type_affectation" required="true">

                                        <option>PROVISOIRE</option>
                                        <option>PERMANENT</option>
                                       </select>   
                                </div> 

                               
                                <div class="col-md-2">
                                    <h5 class="m-t-30 m-b-10"><b>NUMERO DECISION</b></h5>
                                    <input type="text" class="form-control numero" id="numero_decision" data-error="Veuillez renseigner ce champ." required>
              <div class="help-block with-errors"></div>
                                </div> 
                                <div class="col-md-7">
                                    <h5 class="m-t-30 m-b-10"><b>DECISION SCANNEE</b></h5>
                                    <iframe name="hiddenFrame" style="position:absolute; top:-1px; left:-1px; width:1px; height:1px;display: none;"></iframe>
               <form action="upload_decision" method="post" enctype="multipart/form-data" target="hiddenFrame" id="decision_form">
                
              <input type="file" name="files" id="filer_input" single>
              <input type="text" name="id_inserted_affectation" id="id_inserted_affectation" style="display: none;">
               <!-- <input type="submit" value="Submit">  -->
         
        </form>
                                </div> 
                      </div>
                     
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_affectation_btn" data-perform="panel-collapse">Annuler</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                <button class="fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_affectation_btn" onclick="add_affectation()">Affecter</button>
                            </div> 
                        </div>
                    </div>
                    </div> 

                        
                       
                    
                    
                </div> 
                 
            </div>
</div>
</div>
<!-- / Page content ends here -->                
<!-- .right-sidebar -->
<div class="right-sidebar">
    <div class="slimscrollright">
        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
        <div class="r-panel-body">
            <ul>
                <li><b>Layout Options</b></li>
                <li>
                    <div class="checkbox checkbox-info">
                        <input id="checkbox1" type="checkbox" class="fxhdr">
                        <label for="checkbox1"> Fix Header </label>
                    </div>
                </li>
                <li>
                    <div class="checkbox checkbox-warning">
                        <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                        <label for="checkbox2"> Fix Sidebar </label>
                    </div>
                </li>
                <li>
                    <div class="checkbox checkbox-success">
                        <input id="checkbox4" type="checkbox" class="open-close">
                        <label for="checkbox4"> Toggle Sidebar </label>
                    </div>
                </li>
            </ul>
            <ul id="themecolors" class="m-t-20">
                <li><b>With Light sidebar</b></li>
                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                <li><b>With Dark sidebar</b></li>
                <br/>
                <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>
                <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
            </ul>
            <ul class="m-t-20 all-demos">
                <li><b>Choose other demos</b></li>
            </ul>
            <ul class="m-t-20 chatonline">
                <li><b>Chat option</b></li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                </li>
                <li>
                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- /.right-sidebar -->
</div>
<!-- /.container-fluid -->
<footer class="footer text-center text-info"> 2018 &copy; www.cnas.dz </footer>
</div>
<!-- /#page-wrapper -->
</div>
<!-- /#wrapper -->

<!-- jQuery -->
<script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.phone.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/jquery.inputmask.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>

<!-- Typehead Plugin JavaScript -->



<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/plugins/js/sweetalert2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/jquery.filer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/validator.js"></script>



<script>
    $("#decision_form").submit(function(){

    var posting =    $.post($(this).attr("action"), $(this).serialize());

    // Put the results in a div
    posting.done(function( data ) {
      swal("Succès!", "Informations enregistrées avec succès.", "success");

    //  $('#filer_input_modal').prop("jFiler").reset();
    //  $("#__compose_users").select2('val', '');
     
     // back_to_msgs();

    });
  });

    function add_affectation() {

                

         // id of inserted vehicle
         var id_vehicle = $('#id_inserted_vehicle').val();           
                    
        // Get values from inputs
            
            
            var etat = $('#etat').val();
            var type_affectation = $('#type_affectation').val();
            var numero_decision = $('#numero_decision').val();
            var kilometrage = $('#kilometrage').val();
            var id_structure = $('#structure_affectation').selectpicker('val');
            var id_priorietaire = $('#detail_affectation').selectpicker('val');

            if (etat != "" &&  type_affectation != "" && numero_decision != "" 
                && kilometrage != "" && id_structure != "" && id_priorietaire != "") {

 


                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"etat":"' +etat+ 
                                        '","type_affectation":"' +type_affectation+ 
                                        '","numero_decision":"' +numero_decision+ 
                                        '","kilometrage":"' +kilometrage+ 
                                        '","id_structure":"' +id_structure+ 
                                        '","id_priorietaire":"' +id_priorietaire+ 
                                        '","id_vehicle":"' +id_vehicle+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "add_affectation";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'data inserted') {
                                        $('#id_inserted_affectation').val(reply['id_inserted_affectation']);
                                        $('#decision_form').trigger('submit');
                                             swal({
                                                  title: "Succès!",
                                                  text: "Véhicule affecté avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                             $('#affectation_collapse').removeClass('in');
                                             $('#affectation_collapse').attr('aria-expanded',false);
                                             $('#affectation_panel').hide();

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'affectation du véhicule", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       

    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");
    }
    function add_inssurance() {

                

         // id of inserted vehicle
         var id_vehicle = $('#id_inserted_vehicle').val();           
                    
        // Get values from inputs
            
            
            var id_compagnie = $('#compagnie_assurance').selectpicker('val');
            var date_debut = $('#date_debut').val();
            var date_fin = $('#date_fin').val();
            var numero_police = $('#numero_police').val();
            console.log($('#date_fin').val());
            console.log($('#date_fin').text());
            if (compagnie_assurance != "" &&  date_debut != "" && date_fin != "" 
                && numero_police != "" ) {



                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"id_compagnie":"' +id_compagnie+ 
                                        '","date_debut":"' +date_debut+ 
                                        '","id_vehicle":"' +id_vehicle+ 
                                        '","date_fin":"' +date_fin+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "add_inssurance";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'data inserted') {
                                             swal({
                                                  title: "Succès!",
                                                  text: "Information ajoutée avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                             $('#assurance_panel').hide();

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       


    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");   
    }
    



$(document).ready(function() {
   
  
    // enable fileuploader plugin
    $('#filer_input').filer({
    showThumbs: true,
    addMore: false,
    allowDuplicates: false,
    limit:1,
    extensions: ["pdf","png","jpg","bmp","jpeg","tiff"],
    captions: {
      button: "Parcourir",
      feedback: "Sélectionner la décision scannée..",
      feedback2: "fichier a été choisi",
      drop: "Drop file here to Upload",
      size: "taille",
      removeConfirmation: "Voulez-vous vraiment supprimer ce fichier?",
      errors: {
        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
        filesType: "Veuillez sélectionner uniquement des fichiers imgaes ou pdf.",
        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
        folderUpload: "Vous n'êtes pas autorisé à sélectionner des dossiers."
      }
    }
  });
     
 
    
});


    jQuery(document).ready(function() {
    
        // For mask

        $('.immatriculation').inputmask('99999-999-99');
        $('.immatriculation_old').inputmask('99999-99-99');
        $('.kilometrage').inputmask({ regex: "[0-9]*" });
        $('.naftal').inputmask('9999-9999-9999-9999');
        $('.credit').inputmask({regex : '[0-9]* DA'});
        $('.debit').inputmask({regex : '[0-9]* DA'});
        $('.numero').inputmask({regex : '[0-9]*'});

        
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
        // For select 2


        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }

        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });

        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        // For multiselect

        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });

        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });


     // Add fleet function
     


 });
</script>

<script>


    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'

    });

    $('.clockpicker').clockpicker({
        donetext: 'Done',

    })
    .find('input').change(function() {
        console.log(this.value);
    });

    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
        .clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }

    // Date Picker
    jQuery('.mydatepicker, #datepicker, #expiration_carte').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',

    });
    jQuery('#datepicker-autoclose').datepicker({
        autoclose: true,
        todayHighlight: true,
         format: 'dd/mm/yyyy',
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        format: 'dd/mm/yyyy'
    });
    jQuery('.myrange').datepicker({
        toggleActive: true,
        text : 'au'
    });
    jQuery('#datepicker-inline').datepicker({

        todayHighlight: true
    });

    // Daterange picker

    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
</script> 

<!-- FUNCTIONS -->
<script type="text/javascript">


        /*
            function add_vehicle()
             (1) get values from inputs
             (2) validate values..
             (3) ask user if he wants to add other information to the vehicle meaning the inssurance and affectation
             (4) (a) if yes show affectation and inssurance forms
                 (b) else show success message and direct page to all_vehicles page
        */
                 function add_vehicle() {

         // id of inserted vehicle
         var id_vehicle;           
        // Get values from inputs
            
            var marque = $('#marque').val();
            var modele = $('#modele').val();
            var motorisation = $('#motorisation').val();
            var numero_immatriculation = $('#numero_immatriculation').val();
            var gps_tracker = $('#gps_tracker').val();
            var numero_immatriculation_old = $('#numero_immatriculation_old').val();
            var etat = $('#etat').val();
            var type_vehicle = $('#type_vehicle').val();
            var numero_chassis = $('#numero_chassis').val();

            // carte naftal
            var numero = $('#numero_carte').val();
            var expiration_carte = $('#expiration_carte').val();
            var debit_carte = $('#debit_carte').val();
            var credit_carte = $('#credit_carte').val();

            var exterior_parts  =  <?php echo json_encode($exterior_parts); ?>;
            var exterior_part_status = [];
            for (var i = exterior_parts.length - 1; i >= 0; i--) {
            exterior_part_status[i] =  exterior_parts[i]['id']+'_'+$('#exterior_part_status_'+exterior_parts[i]['id']).val();
   }
           var interior_parts  =  <?php echo json_encode($interior_parts); ?>;
                    var interior_part_status = [];
                    for (var i = interior_parts.length - 1; i >= 0; i--) {
                    interior_part_status[i] =  interior_parts[i]['id']+'_'+$('#interior_part_status_'+interior_parts[i]['id']).val();
           }

             if (marque != "" &&  modele != "" && motorisation != "" 
                 && numero_immatriculation != "" && gps_tracker != "" && gps_tracker != ""
                 && etat != "" && type_vehicle != "" && numero_chassis != "") {

//             // add validation to inputs

//             // end validation...
//                                     // insert data to database
//                                     //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"marque":"' +marque+ 
                                        '","modele":"' +modele+ 
                                        '","motorisation":"' +motorisation+ 
                                        '","numero_immatriculation":"' +numero_immatriculation+ 
                                        '","numero_immatriculation_old":"' +numero_immatriculation_old+ 
                                        '","gps_tracker":"' +gps_tracker+ 
                                        '","etat":"' +etat+ 
                                        '","numero_chassis":"' +numero_chassis+ 
                                        '","type_vehicle":"' +type_vehicle+
                                        
                                        '","numero":"' + numero+
                                        '","date_expiration":"' + expiration_carte+
                                        '","debit":"' + debit_carte+
                                        '","credit":"' + credit_carte+

                                        '","interior_status":"' + interior_part_status+
                                        '","exterior_status":"' + exterior_part_status + '"}'  
                                        ); 


                                    console.log(data2db);
                                       var url = "add_vehicle";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(data.responseText);
                                        console.log(reply['message'] );
                                        console.log(reply['ids'] );
                                        if (reply['message'] == 'data inserted') {
                                             swal({
              title: "Véhicule ajouté avec succès!",
              html: "Voullez-vous ajouter d'autes information ? <br> Assurance ? <br> Structure d'affectation ?",
              type: "success",
              showCancelButton: true,
              confirmButtonColor: "#DD6B55",
              confirmButtonText: "Oui, Ajouter!",
              cancelButtonText: "Non",
              closeOnConfirm: true
              
            },
            function(){
                     
                    $('#assurance_panel').show();
                    $('#affectation_panel').show();  
                    $('#add_vehicle_btn').prop("disabled",true);
                   // $('#cancel_add_vehicle_btn').hide();


                    document.getElementById ('affectation_panel').scrollIntoView();
                     id_vehicle = reply['ids']['inserted_vehicle_id'];
                     $('#id_inserted_vehicle').val(id_vehicle);

            });
                                   
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout de véhicule", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","lool", "error");
                                      });
                                   
 }
     else swal("Erreur!","Veuillez remplir tous les champs obligatoire (*) <small style='color:red'>(*)</small>",html:true, "error");   

                                    } 






</script>   

<!--Style Switcher -->
<script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>

</body>

</html>
