<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
        <title>CNAS | Gestion PARC  AUTO</title>
        <!-- Bootstrap Core CSS -->

       <link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
        <!-- Menu CSS -->
        <link href="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
        <!-- animation CSS -->
        <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css" rel="stylesheet">
        <!-- Custom CSS -->
        <link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
        <!-- color CSS -->
        <link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme" rel="stylesheet">
        
        <link href="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    
    

        <!-- <link href="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
        
        <link href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css" rel="stylesheet" type="text/css" /> -->
       <!--  <link href="<?php echo base_url(); ?>plugins/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" /> -->
         <link href="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
        #btn-close-modal {
        width:100%;
        text-align: center;
        cursor:pointer;
        color:#fff;
        }
        </style>
    </head>
    <body class="fix-sidebar">
        <!-- Preloader -->
        <div class="preloader">
            <div class="cssload-speeding-wheel"></div>
        </div>
        <div id="wrapper">
		           <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" id="modal_history" data-animation="true">
                                    <div class="modal-dialog modal-lg" >
                                        <div class="modal-content" style="width: 115%; margin-left: -7%; margin-top: 15%">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                <h4 class="modal-title" id="myLargeModalLabel">Historique Affectation</h4>
                                            </div>
                                            <div class="modal-body">
                                                <div class="table-responsive table-bordered" id="div_affectation_table"
                                                    style="display: none;">
                                                    <table class="table hover-table table-default" id="affectation_table">
                                                        <thead>
                                                            <tr>
                                                                <th>Structure</th>
                                                                <th>Responsable</th>
                                                                <th>Type</th>
                                                                <th>Date</th>
                                                                <th>Etat véhicule</th>
                                                                <th>KM</th>
                                                                <th>N° décision</th>
                                                                <th>Decision</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="table-responsive table-bordered" id="div_assurance_table"
                                                    style="display: none;">
                                                    <table class="table hover-table table-default" id="assurance_table">
                                                        <thead>
                                                            <tr>
                                                                <th>Compagnie d'assurance</th>
                                                                <th>Date début d'échéance</th>
                                                                <th>Date fin d'échéance</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="table-responsive table-bordered" id="div_accidents_table" style="display: none;">
                                                    <table class="table hover-table table-default" id="accidents_table"
                                                        >
                                                        <thead>
                                                            <tr>
                                                                <th> Sinistre</th>
                                                                <th> Date sinistre</th>
                                                                <th> Heure</th>
                                                                <th> Chauffeur</th>
                                                                <th> Constat</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Fermer</button>
                                            </div>
                                        </div>
                                        <!-- /.modal-content -->
                                    </div>
                                    <!-- /.modal-dialog -->

                                </div>

            <div id="idle-timeout-dialog" data-backdrop="static" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h4 class="modal-title">Your session is expiring soon</h4> </div>
                        <div class="modal-body">
                            <p>
                                <i class="fa fa-warning font-red"></i> You session will be locked in
                                <span id="idle-timeout-counter"></span> seconds.</p>
                                <p> Do you want to continue your session? </p>
                            </div>
                            <div class="modal-footer text-center">
                                <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-outline btn-success" data-dismiss="modal">Yes, Keep Working</button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Top Navigation -->
                <nav class="navbar navbar-default navbar-static-top m-b-0">
                    <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                    <div class="top-left-part"><a class="logo" href="accueil"><b><!--This is dark logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><!--This is dark logo text--><img src="<?php echo base_url(); ?>plugins/images/parcautom-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-text-darkl.png" alt="home" class="light-logo" /></span></a></div>
                    <ul class="nav navbar-top-links navbar-left hidden-xs">
                        <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                        <li>
                            <!-- <form role="search" class="app-search hidden-xs">
                                <input type="text" placeholder="Search..." class="form-control">
                                <a href=""><i class="fa fa-search"></i></a>
                            </form> -->
                        </li>
                    </ul>
                    <ul class="nav navbar-top-links navbar-right pull-right">
                        <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-settings"></i>
                            <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
                        </a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-header -->
                    <!-- /.navbar-top-links -->
                    <!-- /.navbar-static-side -->
                </nav>
                <!-- End Top Navigation -->
                <!-- Left navbar-header -->
                <div class="navbar-default sidebar" role="navigation">
                    <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                        <div class="user-profile">
                            <div class="dropdown user-pro-body">
                                <div><img src="<?php echo base_url(); ?>plugins/images/user-me.jpg" alt="user-img" class="img-circle"></div>
                                <a href="#" class="dropdown-toggle u-dropdown text-success" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username'); ?><span class="caret"></span></a>
                                <ul class="dropdown-menu animated flipInY">
                                    <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                                    <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                                    <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                                    <!-- <li role="separator" class="divider"></li> -->
                                    <li><a href="javascript:void(0);"><i class="ti-settings"></i> Options du compte</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                                </ul>
                            </div>
                        </div>
                        <ul class="nav" id="side-menu">
                            <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                                
                                <div class="input-group custom-search-form">
                                    <input type="text" class="form-control" placeholder="Search...">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
                                    </span> </div>
                                    
                                </li> -->
                                <li class="nav-small-cap m-t-10 text-danger">--- Menu principal</li>
                                <li> <a href="accueil" class="waves-effect "><i class="icon-home text-danger" ></i> <span class="hide-menu text-danger"> Accueil </span></a></li>
                               <li> <a href="account_settings" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="P"></i> <span class="hide-menu text-danger"> Options du compte </span></a></li>


                    <li class="nav-small-cap text-warning">--- Gestion du parc</li>
                    <li><a href="javascript:void(0);" class="waves-effect active "><i class="linea-icon linea-basic fa fa-car text-warning" ></i> <span class="hide-menu text-warning" >Véhicules<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_vehicles" class="active">Tous les véhicules</a></li>
                            <li> <a href="add_vehicle_page">Ajouter un véhicule</a></li>
                            <li> <a href="edit_vehicle_page">Modifier un véhicule</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect "><i class="icon-speedometer text-warning" ></i> <span class="hide-menu text-warning">Chauffeurs<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_drivers" class="">Tous les chauffeurs</a></li>
                            <li> <a href="add_driver_page">Ajouter un chaufeur</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-equalizer text-warning" ></i> <span class="hide-menu text-warning">Entretiens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_maintenances_page" class="">Liste des entretiens</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class=" wi wi-fire text-warning" ></i> <span class="hide-menu text-warning">Accidents<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_accidents_page" class="">Liste des accidents</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-wrench text-warning" ></i> <span class="hide-menu text-warning">Reparations<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparations_page" class="">Liste des Reparations</a></li>
                        </ul>
                    </li>

                    <li class="nav-small-cap text-blue">--- Gestion des tables</li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-shield text-blue" ></i> <span class="hide-menu text-blue">Assurances<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_assurances_page">Liste des compagnies</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);" class="waves-effect"><i class=" ti-hummer text-blue" ></i> <span class="hide-menu text-blue">Mécaniciens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparateurs_page">Liste des mécaniciens</a></li>
                        </ul>
                    </li>
                 

                
                
                
                <li><a href="logout" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>
                
            </ul>
        </div>
    </div>
    <!-- Left navbar-header end -->
    <!-- Page Content -->
    <div id="page-wrapper">
        
        <div class="container-fluid">
            <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <!-- Page title goes here --> <h4 class="page-title">Liste des véhicules</h4>
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    
                    <ol class="breadcrumb">
                        <li ><a href="accueil">Accueil</a></li>
                        <li class="active">Liste des véhicules</li>
                        <!-- Breadcrumb goes here : Accueil/.....  -->
                        
                    </ol>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="row">
                <!-- Page content starts here -->
                <div class="col-md-12 animated" id="vehicles_list">

                        <div class="white-box" style="border-color: #FFE7C2;border-style: dashed;">
        <h4 class="box-title m-b-0"><b>Liste des véhicules</b></h4>
        <!-- <form data-toggle="validator" > -->
                        <br>
                        <div class="table table-hover ">
                                <table id="example23" class="table hover-table table-default responsive no-wrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="btn-default">
                                        <th class="text-center">#</th>
                                        <th class="text-center">Marque</th>
                                        <th class="text-center">Modèle</th>
                            
                                        <th class="text-center">Motorisation</th>
                                        <th class="text-center">Immatriculation</th>
                                        <th class="text-center">Type</th>
                                        <th class="text-center">GPS</th>
                                        <th class="text-center">Etat général</th>
                                    </tr>
                                </thead>
                                
                                <tbody id="tbl" style="cursor: pointer;">
                                    <?php foreach ($vehicles as $vehicle) {
                                    // echo '<div class="dropdown">';
                                        echo '<tr>';
                                            // echo '<td data-toggle="dropdown">' ;
                                                //   echo '<ul class="dropdown-menu" >';
                                                    //    echo '<li><a href="#">HTML</a></li>';
                                                    //      echo' <li><a href="#">CSS</a></li>';
                                                    //       echo'<li><a href="#">JavaScript</a></li>';
                                                //   echo ' </ul></div></td>';
                                                echo '<td style="width:10px"><div class="btn-group m-r-10" data-placement="top" title="HISTORIQUE" data-toggle="tooltip">';
                                                    echo  '
         <button aria-expanded="false" data-toggle="dropdown" class="btn btn-default btn-outline dropdown-toggle waves-effect waves-light" type="button"> <i class="fa fa-gear m-r-5" ></i>
                                                    <span class="caret" ></span>
                                                    </button>';
                                                    echo  '<ul role="menu" class="dropdown-menu animated flipInX" style="border-color:gray">';
                                                        echo  ' <li><a data-toggle="modal" data-target="#modal_history" href="javascript:void(0)" onclick=get_history("affectation",'.$vehicle["ID"].')>AFFECTATIONS</a></li>';
                                                        echo  ' <li><a data-toggle="modal" data-target="#modal_history" href="javascript:void(0)" onclick=get_history("assurance",'.$vehicle["ID"].')>ASSURANCES</a></li>';
                                                        echo  '<li><a data-toggle="modal" data-target="#modal_history" href="javascript:void(0)" onclick=get_history("accidents",'.$vehicle["ID"].')>ACCIDENTS</a></li>';
                                                        echo  '<li class="divider"></li>';
     echo  ' <li><a href="javascript:void(0)" onclick="get_vehicle_info('.$vehicle['ID'].')">Tous détails</a></li>';
                                                    echo '</ul>';
                                                echo  ' </div></td>';
                                                
                                                echo '<td >' . $vehicle['marque']. '</td>' ;
                                                echo '<td >' . $vehicle['modele']. '</td>' ;
                                  
                                                echo '<td >' . $vehicle['motorisation']. '</td>' ;
                                                echo '<td class="text-center">' . $vehicle['numero_immatriculation']. '</td>' ;
                                                echo '<td >' . $vehicle['type_vehicle']. '</td>' ;
                                                echo '<td class="text-center">' . $vehicle['gps_tracker']. '</td>' ;
                                                echo '<td >';
                                                    
                                                    switch ($vehicle['etat']) {
                                                    case 'NEUF':
                                                    echo '<span class="label label-table label-success">NEUF</span>';
                                                    break;
                                                    case 'BON ETAT':
                                                    echo '<span class="label label-table" style="background-color :#D2FF00">BON ETAT</span>';
                                                    break;
                                                    case 'ETAT MOYEN':
                                                    echo '<span class="label label-table label-warning">ETAT MOYEN</span>';
                                                    break;
                                                    case 'MAUVAIS ETAT':
                                                    echo '<span class="label label-table label-danger">MAUVAIS ETAT</span>';
                                                    break;
                                                    }
                                                echo '</td>' ;
                                            echo '</tr>';
                                            } ?>
                                        </tbody>
                                    </table>
                                </div>
                                <br>
                                
                                
                                
                     
                 
                                <script type="text/javascript">
                                function get_history(table_name, id) {
                                //  $('#affectation_table').reload();
                                //   $('#affectation_table').DataTable().ajax.reload();
                                var url;
                                switch (table_name) {
                                case "affectation" :
                                url = "get_affectations";
                                $('#myLargeModalLabel').text('Historique affectations');
                                break;
                                case  "accidents" :
                                url = "get_accidents";
                                $('#myLargeModalLabel').text('Historique accidents');
                                break;
                                case "assurance" :
                                url = "get_assurances";
                                $('#myLargeModalLabel').text('Historique assurances');
                                break;
                                }
                                var id_vehicle = id;
                                
                                var data2db = JSON.parse('{"id_vehicle":"' +id_vehicle+  '"}' );
                                console.log(data2db);
                                
                                $.post(url,data2db).done(function( data ) {
                                var reply = JSON.parse(data);
                                console.log(reply);
                                var table = reply['table_name'];
                                switch (table) {
                                case "affectations" :
                                var affectations  = reply['affectation_history'];
                                
                                // for (var j = 0; j < affectations.length; j++) {
                                //      (function(j) {
                                //        // using javascript closure muhahaha :
                                
                                //    $("#affectation_table tbody").append(
                                //            '<tr><td>'+affectations[j]['structure']+ '</td>'+
                                //             '<td>'+affectations[j]['priorietaire']+ '</td>'+
                                //             '<td>'+affectations[j]['type_affectation']+ '</td>'+
                                //             '<td>'+affectations[j]['date_affectation']+ '</td>'+
                                //             '<td>'+affectations[j]['etat']+ '</td>'+
                                //             '<td>'+affectations[j]['kilometrage']+ '</td>'+
                                //             '<td>'+affectations[j]['numero_decision']+ '</td>'+
                                //             '<td>'+affectations[j]['document_decision']+  '</td></tr>');
                                //      })(j);
                                //    }
                                
                                
                                
                                
                                // Draw table
                                // Draw table
                                var table =  $('#affectation_table').DataTable({
                                "paging": true,
                                "pageLength": 10,
                                "destroy": true,
                                "processing": true,
                                "deferRender": true,
                                "lengthChange": false,
                                "searching": true,
                                "serverSide" : false,
                                
                                "columnDefs": [
                                { "orderable": false, "targets": 7},
                                {"className": "dt-center", "targets": 7},
                                {
                                // The `data` parameter refers to the data for the cell (defined by the
                                // `data` option, which defaults to the column being worked with, in
                                // this case `data: 0`.
                                "render": function ( data, type, row ) {
                                if (data ) {
                                return "<div id ='image-popups'> <a  href='<?php echo base_url(); ?>" + data +"' data-effect='mfp-3d-unfold' target='_blank' class='image-popup-no-margins' ><i class= 'fa  fa-file-pdf-o'></i></a></div>";
                                } else return "";
                                
                                
                                },
                                "targets": 7
                                },
                                ],
                                "data" : affectations,
                                "columns" : [
                                { "data" : "structure" },
                                { "data" : "priorietaire" },
                                { "data" : "type_affectation" },
                                { "data" : "date_affectation" },
                                { "data" : "etat" },
                                { "data" : "kilometrage" },
                                { "data" : "numero_decision" },
                                { "data": "document_decision" }
                                ],
                                
                                select: {
                                style:    'os',
                                selector: 'td:first-child'
                                },
                                "info": true,
                                "autoWidth": false,
                                "language": {
                                "paginate": {
                                "previous": "Précédent",
                                "next": "Suivant",
                                },
                                "sSearch": "Rechercher",
                                "sInfoEmpty": "",
                                "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                                "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                                "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau"
                                }
                                });
                                $('#affectation_table tbody').on( 'click', 'button', function () {
                                var data = table.row( $(this).parents('tr') ).data();
                                alert( data[ 1 ] );
                                } );
                                $('#affectation_table').on( 'draw.dt', function () {
                                console.log( 'Redraw occurred at: '+new Date().getTime() );
                                } );
                                $('#div_affectation_table').attr('style','display:initial;');
                                $('#div_assurance_table').attr('style','display:none;');
                                $('#div_accidents_table').attr('style','display:none;');
                                
                                break;
                                case  "accidents" :
                                var accidents  = reply['accidents_history'];
                                
                                // for (var j = 0; j < affectations.length; j++) {
                                //      (function(j) {
                                //        // using javascript closure muhahaha :
                                
                                //    $("#affectation_table tbody").append(
                                //            '<tr><td>'+affectations[j]['structure']+ '</td>'+
                                //             '<td>'+affectations[j]['priorietaire']+ '</td>'+
                                //             '<td>'+affectations[j]['type_affectation']+ '</td>'+
                                //             '<td>'+affectations[j]['date_affectation']+ '</td>'+
                                //             '<td>'+affectations[j]['etat']+ '</td>'+
                                //             '<td>'+affectations[j]['kilometrage']+ '</td>'+
                                //             '<td>'+affectations[j]['numero_decision']+ '</td>'+
                                //             '<td>'+affectations[j]['document_decision']+  '</td></tr>');
                                //      })(j);
                                //    }
                                
                                
                                
                                
                                // Draw table
                                // Draw table
                                var table =  $('#accidents_table').DataTable({
                                "paging": true,
                                "pageLength": 10,
                                "destroy": true,
                                "processing": true,
                                "deferRender": true,
                                "lengthChange": false,
                                "searching": true,
                                "serverSide" : false,
                                
                                "columnDefs": [
                                { "orderable": false, "targets": 4},
                                {"className": "dt-center", "targets": 4},
                                {
                                // The `data` parameter refers to the data for the cell (defined by the
                                // `data` option, which defaults to the column being worked with, in
                                // this case `data: 0`.
                                "render": function ( data, type, row ) {
                                if (data ) {
                                return "<div id ='image-popups'> <a  href='<?php echo base_url(); ?>" + data +"' data-effect='mfp-3d-unfold' target='_blank' class='image-popup-no-margins' ><i class= 'fa  fa-file-pdf-o'></i></a></div>";
                                } else return "";
                                
                                
                                },
                                "targets": 4
                                },
                                ],
                                "data" : accidents,
                                "columns" : [
                                { "data" : "details" },
                                { "data" : "accident_date" },
                                { "data" : "accident_time" },
                                { "data" : "Chauffeur" },
                                { "data" : "constat_accident" }
                                
                                ],
                                
                                select: {
                                style:    'os',
                                selector: 'td:first-child'
                                },
                                "info": true,
                                "autoWidth": false,
                                "language": {
                                "paginate": {
                                "previous": "Précédent",
                                "next": "Suivant",
                                },
                                "sSearch": "Rechercher",
                                "sInfoEmpty": "",
                                "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                                "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                                "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau"
                                }
                                });
                                $('#accidents_table tbody').on( 'click', 'button', function () {
                                var data = table.row( $(this).parents('tr') ).data();
                                alert( data[ 1 ] );
                                } );
                                $('#accidents_table').on( 'draw.dt', function () {
                                console.log( 'Redraw occurred at: '+new Date().getTime() );
                                } );
                                $('#div_affectation_table').attr('style','display:none;');
                                $('#div_assurance_table').attr('style','display:none;');
                                $('#div_accidents_table').attr('style','display:initial;');
                                break;
                                case "assurances" :
                                var assurances  = reply['assurance_history'];
                                //     for (var j = 0; j < assurances.length; j++) {
                                //   (function(j) {
                                //     // using javascript closure muhahaha :
                                
                                // $("#assurance_table tbody").append(
                                //         '<tr><td>'+assurances[j]['compagnie_assurance']+ '</td>'+
                                //          '<td>'+assurances[j]['date_debut']+ '</td>'+
                                //          '<td>'+assurances[j]['date_fin']+  '</td></tr>');
                                //   })(j);
                                // }
                                
                                
                                // Draw table
                                // Draw table
                                $('#assurance_table').DataTable({
                                "paging": true,
                                "pageLength": 10,
                                "destroy": true,
                                "processing": true,
                                "deferRender": true,
                                "lengthChange": false,
                                "searching": true,
                                "columnDefs": [
                                { "orderable": true, "targets": [0,1,2]}
                                ],
                                "data" : assurances,
                                "columns" : [
                                { "data" : "compagnie_assurance" },
                                { "data" : "date_debut" },
                                { "data" : "date_fin" }
                                ],
                                select: {
                                style:    'os',
                                selector: 'td:first-child'
                                },
                                "info": true,
                                "autoWidth": false,
                                "language": {
                                "paginate": {
                                "previous": "Précédent",
                                "next": "Suivant",
                                },
                                "sSearch": "Rechercher",
                                "sInfoEmpty": "",
                                "sZeroRecords": "Aucun &eacute;l&eacute;ment &agrave; afficher",
                                "sInfo": "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                                "sEmptyTable": "Aucune donn&eacute;e disponible dans le tableau"
                                }
                                });
                                $('#assurance_table').on( 'draw.dt', function () {
                                console.log( 'Redraw occurred at: '+new Date().getTime() );
                                } );
                                $('#div_affectation_table').attr('style','display:none;');
                                $('#div_assurance_table').attr('style','display:initial;');
                                $('#div_accidents_table').attr('style','display:none;');
                                
                                break;
                                
                                }
                                // $('#modal_history').modal('show');
                                //   if (reply['message'] == 'got data') {
                                //   $('#id_inserted_affectation').val(reply['id_inserted_affectation']);
                                //   $('#decision_form').trigger('submit');
                                //        swal({
                                //             title: "Succès!",
                                //             text: "Véhicule affecté avec succès",
                                //             type: "success",
                                //             confirmButtonColor: "#DD6B55",
                                //             confirmButtonText: "Terminer",
                                //             closeOnConfirm: true,
                                
                                // });
                                
                                //        $('#affectation_collapse').removeClass('in');
                                //        $('#affectation_collapse').attr('aria-expanded',false);
                                //        $('#affectation_panel').hide();
                                
                                //   }else if (reply == 'error') {
                                //     swal("Erreur...","Errur lors l'affectation du véhicule", "error");
                                //   }
                                //   // swal("ID CAS", mObj, "success");
                                }).fail(function(data, textStatus, errorThrown){
                                swal("Erreur...","", "error");
                                });
                                }
                                
                                function print_me() {
                                $("#print-iframe").get(0).contentWindow.print();
                                // $('#myModal').trigger('data-dismiss');
                                }
                                function myFunction() {
                                
                                $('#modal_history').modal('show');
                                
                                // var iframe = document.getElementById("print-iframe");
                                // var elmnt = iframe.contentWindow.document.getElementById("designation");
                                //  var iframe = $('#print-iframe').contents();
                                // var elmnt = iframe.find("#designation");
                                // elmnt.text('ohhhh')
                                // alert(elmnt.text());
                                }
                                </script>
                                
                            </div>
                        </div>
                    
                    <div class="col-sm-12 animated" style=" display: none;" id="vehicle_details">

<div class="panel panel-default">
                            <div class="panel-heading">RENSEIGNEMENTS DU VEHICULE
                                <div class="panel-action">
                                              <a class="text-success" onclick="back_to_list()" href="javascript:void(0)" >
                            <i class="ti-arrow-left" style="font-size: 13px"></i>&nbsp;Retour vers la liste </a>
                                </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">



        <!-- <form data-toggle="validator" > -->
    <hr>
        <div class="row ">
            <div class="col-sm-4 form-group">
             <h5 class="m-t-30 m-b-10"><b>MARQUE</b></h5> 
             
              <input class=" form-control information" type="text"  id="marque"  readonly="true" style="background-color: rgba(255, 255, 255)">
         
      </div>
      <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>MODELE</b></h5>
        <input class="form-control information" type="text" placeholder="ELANTRA, ACCENT,..." id="modele" data-error="Veuillez renseigner ce champ." readonly="true" style="background-color: rgba(255, 255, 255)">
                 </div>
    <div class="col-md-4">
        <h5 class="m-t-30 m-b-10"><b>MOTORISATION</b></h5>
        <input class="form-control information" id="motorisation" readonly="true" style="background-color: rgba(255, 255, 255)">
              
    </div>
    <div class="col-md-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>TYPE</b></h5>
        <input class="form-control information " id="type_vehicle"  readonly="true" style="background-color: rgba(255, 255, 255)">
              
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>VEHICULE TRACKER PAR GPS</b></h5>
       
<input class="form-control kilometrage information" id="gps_tracker" readonly="true" style="background-color: rgba(255, 255, 255)">
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>ETAT GENERAL DU VEHICULE</b></h5>
        <input class="form-control information" title="Sélectionner.."  id="etat"  readonly="true" style="background-color: rgba(255, 255, 255)"/>
           
    </div>

                     <!--            <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input class="form-control kilometrage" id="kilometrage" >
                                </div>  -->   
                            </div>
                            <div class="row ">
                                <div class="col-sm-4 form-group">
                                 <h5 class="m-t-30 m-b-10"><b>N° CHASSIS</b></h5> 
                                 
                                 <input class=" form-control information" type="text"  maxlength="17" id="numero_chassis" readonly="true" style="background-color: rgba(255, 255, 255)">
              <div class="help-block with-errors"></div>                             </div>
                             <div class="col-sm-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION</b></h5>
                                <input class="form-control immatriculation information"  readonly="true" style="background-color: rgba(255, 255, 255)"
                                id="numero_immatriculation" >
              <div class="help-block with-errors"></div>                           </div>
                            <div class="col-md-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION (Ancienne/Provisoire)</b></h5>
                                <input class="form-control immatriculation information"  
                                id="numero_immatriculation_old" readonly="true" style="background-color: rgba(255, 255, 255)">
              <div class="help-block with-errors"></div>                           </div>


                        </div>
                                                 
              
  </div>
  </div>
  </div>
  </div>
                    




            <div class=" col-md-12 animated" id="status_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'état du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
                  
   
            <div class="col-sm-12 form-group">
                
                <h4 class="box-title m-b-0 text-muted">ETAT EXTERIEUR</h4> 
            </div>
        <br>
        <?php foreach ($exterior_parts as $exterior_part) {
           echo '<div class="col-sm-6 form-group" id="'.$exterior_part['id'].'_div_exterior_status">';
        echo '<h5 class="m-t-30 m-b-10" id="'.$exterior_part['id'].'_exterior_part_name"><b>'.$exterior_part['part'].'</b></h5>';
        echo '<input class="form-control status" type="text" id="exterior_part_status_'.$exterior_part['id'].'" readonly="true" style="background-color: rgba(255, 255, 255)"><div class="help-block with-errors"></div></div>' ;
        } ?>

      
      </div>

      <div class="row ">
            <div class="col-sm-12 form-group">
                <hr>
                <h4 class="box-title m-b-0 text-muted">ETAT INTERIEUR</h4> 
            </div>
            <br>
             <?php foreach ($interior_parts as $interior_part) {
           echo '<div class="col-sm-6 form-group" id="'.$interior_part['id'].'_div_interior_status">';
        echo '<h5 class="m-t-30 m-b-10" id="'.$interior_part['id'].'_interior_part_name"><b>'.$interior_part['part'].'</b></h5>';
        echo '<input class="form-control status" type="text" id="interior_part_status_'.$interior_part['id'].'"  readonly="true" style="background-color: rgba(255, 255, 255)"><div class="help-block with-errors" ></div></div>' ;
        } ?>


      
  </div> 
                
                       
            
            </div> 
         </div> 
               
             </div>

<!--                       <div class=" col-md-12 animated" id="maintenance_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    ENTRETIENS  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
                         <div class="col-md-9">
                <h5 class="m-t-30 m-b-10"><b>TYPE ENTRETIEN (S)</b></h5>

        <select class="selectpicker show-tick entretien" multiple  title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="type_entretien" disabled="true" size="true" data-size="4">




                        <?php foreach ($entretiens as $entretien)
                                                {
                          echo '<option';
                          //echo ' value = "'.$entretien['Id'];
                          echo '> ';
                          echo $entretien['nom'].'</option>';
                                                } ?>
                                    </select>
                                </div>
                        <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>COÛT</b></h5> 
                         
                          <input class="form-control entretien cout" type="text" id="cout" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div>                        
                  </div> 
                <div class=" col-sm-4">
                         <h5 class="m-t-30 m-b-10"><b>DATE</b></h5> 
                         
            <input type="text" class="form-control entretien datepicker-autoclose" placeholder="jj/mm/aaaa" disabled="true" id="date_entretien">
       
                                        
                          
                  </div> 
                  <div class=" col-sm-4">
                         <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5> 
                         
                          <input class="form-control entretien numero" type="text" id="kilometrage_entretien" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div>                        
                  </div>

                  
         
                </div>
                
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">
                      
                           
                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_maintenance_btn" onclick="enable_elements('entretien');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <button class="entretien fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_driver_btn" onclick="add_maintenance()" disabled="true">Sauvgarder</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div>  -->

            <div class=" col-md-12 animated" id="carte_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    Détails carte naftal  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                      <div class="row"> 

                        <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>NUMERO CARTE</b></h5> 
                         
                          <input class="form-control carte" type="text" id="numero_carte" readonly="true" style="background-color: rgba(255, 255, 255)">
                                             
                  </div> 
                <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>DATE EXPIRATION</b></h5> 
                         
                          <input class="form-control datepicker carte"  id="expiration_carte" readonly="true" style="background-color: rgba(255, 255, 255)">
                                                
                  </div>
                   <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>DEBIT</b></h5> 
                         
                      <input class="form-control carte cout" type="text" id="debit_carte" readonly="true" style="background-color: rgba(255, 255, 255)">
                       
                  </div> 
                   <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>CREDIT</b></h5> 
                         
                      <input class="form-control carte cout" type="text" id="credit_carte" readonly="true" style="background-color: rgba(255, 255, 255)">
                         
                  </div> 
                  
         
                </div>
                
                       
      
            </div> 
         </div> 
               
             </div>    
                      <div class=" col-md-12 animated" id="assurance_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'assurance du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 

        <div class="col-md-6">
        <h5 class="m-t-30 m-b-10"><b>COMPAGNIE D'ASSURANCE</b></h5>

        <input class="form-control assurance"    id="compagnie_assurance" readonly="true" style="background-color: rgba(255, 255, 255)" >


                       
                                </div>
                  <div class="col-sm-3">
                     <h5 class="m-t-30 m-b-10"><b>NUMERO POLICE D'ASSURANCE</b></h5> 
                     <div id="car-brands">
                      <input class="form-control numero assurance" type="text" id="numero_police" readonly="true" style="background-color: rgba(255, 255, 255)">
                              </div>   
              </div>
                <div class="col-sm-3">
                     <h5 class="m-t-30 m-b-10"><b>NUMERO TELEPHONE</b></h5> 
                     <div id="car-brands">
                      <input class="form-control numero assurance " type="text" id="telephone" readonly="true" style="background-color: rgba(255, 255, 255)">
                               </div>   
              </div>
                <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>ADRESSE</b></h5> 
                         
                          <input class="form-control assurance" type="text" id="adresse" readonly="true" style="background-color: rgba(255, 255, 255)">
                                                
                  </div>
              <div class="col-sm-6">
                <h5 class="m-t-30 m-b-10 "><b>DATE D'ECHEANCE</b></h5>
                <div class="input-daterange input-group" id="date-range">
                    <span class="input-group-addon bg-info b-0 text-white ">Du</span>
                    <input type="text" class="form-control assurance" name="start" id="date_debut" readonly="true" style="background-color: rgba(255, 255, 255)"/>
                    <span class="input-group-addon bg-info b-0 text-white">au</span>
                    <input type="text" class="form-control assurance" name="end" id="date_fin" readonly="true" style="background-color: rgba(255, 255, 255)" />
                </div>
                    </div> 
                    </div> 
                       
            
            </div> 
         </div> 
               
             </div>   

             <div class=" col-md-12 animated" id="affectation_panel" style="display: none;">
                <div class="panel " style="background-color: #BEBEBE;">

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'affectation du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;" id="affectation_collapse">
                       <div class="row"> 

 <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>DESIGNATION</b></h5>

                                    <input class="form-control " id="designation"
                                    readonly="true" style="background-color: rgba(255, 255, 255)" >
                                       
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>NOM</b></h5>
         <input type="text" class="form-control" id="structure_affectation" readonly="true" style="background-color: rgba(255, 255, 255)"/>
<!--                                     <select class="selectpicker show-tick " data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="structure_affectation" data-hide-disabled= 'true' onchange="get_wilaya()">
                                                                                   
                                        <?php foreach ($types as $type)
                                                {
                                                    echo '<optgroup disabled = "true" label="' .$type['nom']. '">';  
                                                  foreach ($structures as $structure) {
                                                      if ($structure['type'] == $type['nom'] ) {
                                                        
                                                        echo '<option value ="' .$structure['id']. '" code_wilaya ="' .$structure['code_wilaya']. '" >'. $structure['nom'] . '</option>';
                                                          
                                                      }
                                                  }
                                                  
                                                } echo '</optgroup>'; ?>
                                    </select> -->

                                    <script type="text/javascript">
                                        function show_structure(type) {
                                            $('#code_wilaya').val('');    
                                           $('.selectpicker').find('[label]').attr('disabled',true);              
                                           $('.selectpicker').find("[label='"+type+"']").attr('disabled',false);
                                           $('.selectpicker').selectpicker('refresh');}

                                        function get_wilaya() {
                                            $('#code_wilaya').val('');
                                            var value = $('#structure_affectation').val();
                                            var code_wilaya = $('.selectpicker').find("[value='"+value+"']" ).attr('code_wilaya'); 
                                               $('#code_wilaya').val(code_wilaya);
                                           }   
                                       </script>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>CODE WILAYA</b></h5>
                                    <input type="text" class="form-control affectation" id="code_wilaya" readonly="true" style="background-color: rgba(255, 255, 255)" />
                                    <input type="text" class="form-control" id="id_selected_vehicle" readonly="true" style="display: none;" />
                                </div>
                                <div class="col-md-6">
                                    <h5 class="m-t-30 m-b-10"><b>DETAIL AFFECTATION</b></h5>
        <input type="text" class="form-control affectation" id="detail_affectation_" readonly="true" style="background-color: rgba(255, 255, 255)"/>
                                   <div id="detail_affectation__" style="display: none;">
                                    <input class="form-control"  id="detail_affectation"  style="display: none;">
 
                                   </div> 
                                 
                                </div>  
                                    <div class="col-sm-3 form-group">
                                        <h5 class="m-t-30 m-b-10"><b>ETAT DU VEHICULE</b></h5>
                                        <input class="form-control affectation"  id="etat_vehicle"  readonly="true" style="background-color: rgba(255, 255, 255)">
                                              
                               </div>
<!--                                 <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input type="text" class="form-control numero affectation" id="kilometrage" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>
                                </div> -->
                                 <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>TYPE AFFECTATION</b></h5>
                                    
                                    <input class="form-control affectation" id="type_affectation" readonly="true" style="background-color: rgba(255, 255, 255)">
  
                                </div> 

                               
                                <div class="col-md-2">
                                    <h5 class="m-t-30 m-b-10"><b>NUMERO DECISION</b></h5>
                                    <input type="text" class="form-control numero affectation" id="numero_decision" data-error="Veuillez renseigner ce champ." readonly="true" style="background-color: rgba(255, 255, 255)">
              
                                </div> 
                                <div class="col-md-7">
                                    <h5 class="m-t-30 m-b-10"><b>DECISION SCANNEE</b></h5>
                                    <iframe name="hiddenFrame" style="position:absolute; top:-1px; left:-1px; width:1px; height:1px;display: none;"></iframe>
                                    <a href="" target="_blank" id="decision_document"><i class="fa fa-file-pdf-o"  style="font-size:35px"></i></a>
               <form action="upload_decision" method="post" enctype="multipart/form-data" target="hiddenFrame" id="decision_form" style="display: none;">
                
              <input type="file" name="files" id="filer_input" single >
              
              <input type="text" name="id_inserted_affectation" id="id_inserted_affectation" style="display: none;">
               <!-- <input type="submit" value="Submit">  -->
         
        </form>
                                </div> 
                      </div>
                    
                       
         
                    </div> 

                        
                       
                    
                    
                </div> 
                 
            </div> 
        <div class=" col-md-12 animated" id="driver_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    CHAUFFEUR  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
                         <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>CHAUFFEUR</b></h5>

        <input class="form-control chauffeur"  required disabled="true" id="chauffeur_" onchange="$('#type_permis').val($('#chauffeur_').find('option:selected').attr('type_permis'));
        $('#validite_permis').val($('#chauffeur_').find('option:selected').attr('validite_permis'));" readonly="true" style="background-color: rgba(255, 255, 255)" >




                                            
                                </div>
                        <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>TYPE PERMIS CONDUIRE</b></h5> 
                         
                          <input class="form-control chauffeur" type="text" id="type_permis" data-error="Veuillez renseigner ce champ." readonly="true" style="background-color: rgba(255, 255, 255)">
                                               
                  </div> 
                <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>VALIDITE PERMIS CONDUIRE</b></h5> 
                         
                          <input class="form-control chauffeur" type="text" id="validite_permis" data-error="Veuillez renseigner ce champ." readonly="true" style="background-color: rgba(255, 255, 255)">
                                              
                  </div>
                   <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>TYPE CHAUFFEUR</b></h5> 
                         
                       <input class="form-control chauffeur"  id="type_chauffeur" readonly="true" style="background-color: rgba(255, 255, 255)" >     
                  </div> 
                  
         
                </div>
                
                        
            
            </div> 
         </div> 
               
             </div> 
                    </div>
                    <!-- / Page content ends here -->
                    <!-- .right-sidebar -->
                    <div class="right-sidebar">
                        <div class="slimscrollright">
                            <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                            <div class="r-panel-body">
                                <ul>
                                    <li><b>Layout Options</b></li>
                                    <li>
                                        <div class="checkbox checkbox-info">
                                            <input id="checkbox1" type="checkbox" class="fxhdr">
                                            <label for="checkbox1"> Fix Header </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox checkbox-warning">
                                            <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                            <label for="checkbox2"> Fix Sidebar </label>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="checkbox checkbox-success">
                                            <input id="checkbox4" type="checkbox" class="open-close">
                                            <label for="checkbox4"> Toggle Sidebar </label>
                                        </div>
                                    </li>
                                </ul>
                                <ul id="themecolors" class="m-t-20">
                                    <li><b>With Light sidebar</b></li>
                                    <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                    <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                    <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                    <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                    <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                    <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                    <li><b>With Dark sidebar</b></li>
                                    <br/>
                                    <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                                    <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                    <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>
                                    <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                    <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                    <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                                </ul>
                                <ul class="m-t-20 all-demos">
                                    <li><b>Choose other demos</b></li>
                                </ul>
                                <ul class="m-t-20 chatonline">
                                    <li><b>Chat option</b></li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <!-- /.right-sidebar -->
                </div>
                <!-- /.container-fluid -->
            <footer class="footer text-center text-info"> 2018 &copy; www.cnas.dz </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
     <!-- jQuery -->
     <script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>plugins/js/inputmask.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.phone.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/jquery.inputmask.js"></script>
       <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!-- Session-timeout-idle -->
<!--     <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/jquery.idletimeout.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/jquery.idletimer.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/session-timeout-idle-init.js"></script> -->
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>plugins/js/animatedModal.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

       <script src="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->

    <script src="<?php echo base_url(); ?>/plugins/js/buttons.colVis.min.js"></script>
	
    <script src="<?php echo base_url(); ?>/plugins/js/dataTables.responsive.min.js"></script>
	
	<script src="<?php echo base_url(); ?>/plugins/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/buttons.print.min.js"></script>
	
	
	
        <script src="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/plugins/js/sweetalert2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/jquery.filer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/validator.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/moment/moment.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>

function get_vehicle_info(vehicle_id) {
         $('#vehicles_list').addClass('slideOutLeft'); 
            $('#vehicles_list').removeClass('slideInLeft'); 
            $('#vehicles_list').attr('style','position:absolute'); 
             

             
             


            $('#vehicle_details').removeClass('slideOutRight');
            $('#vehicle_details').show(); 
            $('#vehicle_details').addClass('slideInRight');
            $('#vehicle_details').attr('style','position:all'); 


          


            $('#assurance_panel').removeClass('slideOutRight');
            $('#assurance_panel').show(); 
            $('#assurance_panel').addClass('slideInRight');  
            $('#assurance_panel').attr('style','position:all'); 

            
            $('#affectation_panel').removeClass('slideOutRight');
            $('#affectation_panel').show(); 
            $('#affectation_panel').addClass('slideInRight');
            $('#affectation_panel').attr('style','position:all');   


            $('#status_panel').removeClass('slideOutRight');
            $('#status_panel').show(); 
            $('#status_panel').addClass('slideInRight');
            $('#status_panel').attr('style','position:all');

            $('#driver_panel').removeClass('slideOutRight');
            $('#driver_panel').show(); 
            $('#driver_panel').addClass('slideInRight');
            $('#driver_panel').attr('style','position:all'); 

            $('#maintenance_panel').removeClass('slideOutRight');
            $('#maintenance_panel').show(); 
            $('#maintenance_panel').addClass('slideInRight');
            $('#maintenance_panel').attr('style','position:all'); 

            $('#carte_panel').removeClass('slideOutRight');
            $('#carte_panel').show(); 
            $('#carte_panel').addClass('slideInRight');
            $('#carte_panel').attr('style','position:all'); 
            var data2db = JSON.parse(
                                        '{"id_vehicle":"' +vehicle_id+'"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "get_vehicle_info";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'vehicle exists') {

                                        console.log(reply['all_info']['vehicle']['numero_immatriculation']);
if (reply['all_info']['affectation'] != null) {

                    $('#etat_vehicle').val(reply['all_info']['affectation']['etat']);
                     $('#designation').val(reply['all_info']['affectation']['type_structure']);  
                     
                    $('#structure_affectation').val(reply['all_info']['affectation']['structure_affectation']);
                    $('#structure_affectation').attr(reply['all_info']['affectation']['id_structure_affectation']);  

      $('#detail_affectation_').attr(reply['all_info']['affectation']['id_priorietaire']);
                    
      $('#detail_affectation_').val(reply['all_info']['affectation']['detail_affectation']);
        $('#code_wilaya').val(reply['all_info']['affectation']['code_wilaya']);
                    $('#type_affectation').val(reply['all_info']['affectation']['type_affectation']);
                    $('#numero_decision').val(reply['all_info']['affectation']['numero_decision']);
                    if (reply['all_info']['affectation']['document_decision'] != null) {
$('#decision_document').attr('href',"<?php echo base_url(); ?>"+reply['all_info']['affectation']['document_decision']);  } else {
    $('#decision_document').removeAttr('href');
}
}


    // vehicle assurance
            if (reply['all_info']['assurance'] != null) {

            $('#compagnie_assurance').val(reply['all_info']['assurance']['nom']);
           
            $('#adresse').val(reply['all_info']['assurance']['adresse']);
            $('#telephone').val(reply['all_info']['assurance']['telephone']);
            $('#numero_police').val(reply['all_info']['assurance']['numero_police']);
            $('#date_debut').val(reply['all_info']['assurance']['date_debut']);
            $('#date_fin').val(reply['all_info']['assurance']['date_fin']);
            
                                                                }

if (reply['all_info']['vehicle'] != null) {

            $('#marque').val(reply['all_info']['vehicle']['marque']);
            $('#modele').val(reply['all_info']['vehicle']['modele']);
            $('#motorisation').val(reply['all_info']['vehicle']['motorisation']);
            $('#numero_immatriculation').val(reply['all_info']['vehicle']['numero_immatriculation']);
            $('#gps_tracker').val(reply['all_info']['vehicle']['gps_tracker']);
    $('#numero_immatriculation_old').val(reply['all_info']['vehicle']['numero_immatriculation_old']);
            $('#etat').val(reply['all_info']['vehicle']['etat']);
            $('#type_vehicle').val(reply['all_info']['vehicle']['type_vehicle']);
            $('#numero_chassis').val(reply['all_info']['vehicle']['numero_chassis']);
}

            if (reply['all_info']['exterior_status'] != null) {
            var exterior_parts  =  <?php echo json_encode($exterior_parts); ?>;
            var exterior_part_status = [];
               
   for (var i = 0; i < exterior_parts.length; i++) {

              (function(i) {

                // using javascript closure muhahaha :
        if (reply['all_info']['exterior_status'][i] != null ) {
         if (exterior_parts[i]['id'] == reply['all_info']['exterior_status'][i]['id_part'] ) {
                          $('#exterior_part_status_'+exterior_parts[i]['id']).val(reply['all_info']['exterior_status'][i]['status']);  
                        }
                        }
                      })(i);
                    }
                   } 

            if (reply['all_info']['interior_status'] != null) {
            var interior_parts  =  <?php echo json_encode($interior_parts); ?>;
            var interior_part_status = [];
               
   for (var i = 0; i < interior_parts.length; i++) {

              (function(i) {

                // using javascript closure muhahaha :
          if (reply['all_info']['interior_status'][i] != null ) {
          if (interior_parts[i]['id'] == reply['all_info']['interior_status'][i]['id_part'] ) {
                          $('#interior_part_status_'+interior_parts[i]['id']).val(reply['all_info']['interior_status'][i]['status']);  

                        }
                        }

                      })(i);
                    }
                   } 

            if (reply['all_info']['carte_naftal'] != null) {
            $('#numero_carte').val(reply['all_info']['carte_naftal']['numero']);
            $('#debit_carte').val(reply['all_info']['carte_naftal']['debit']);
            $('#credit_carte').val(reply['all_info']['carte_naftal']['credit']);
            $('#expiration_carte').val(reply['all_info']['carte_naftal']['date_expiration']);

                                                                } 
           // vehicle driver info
            if (reply['all_info']['drivers'] != null) {

            $('#chauffeur_').val(reply['all_info']['drivers']['nom'] +' '+reply['all_info']['drivers']['prenom']);
            $('#type_chauffeur').val(reply['all_info']['drivers']['type_chauffeur']);
           
            $('#type_permis').val(reply['all_info']['drivers']['type_permis']);
            $('#validite_permis').val(reply['all_info']['drivers']['validite_permis']);
                                                                }
           
                      
                                   

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Erreur véhicule non trouvé", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
        }
       function back_to_list() {
         $('#example23').DataTable().$('tr.selected').removeClass('selected');
         $("input:radio").attr('checked', false);
            clear_disable_elements('information');
            clear_disable_elements('status');
            clear_disable_elements('assurance');
            clear_disable_elements('affectation');
            clear_disable_elements('chauffeur');


          
            $('#id_selected_vehicle').val('');




            $('#vehicles_list').removeClass('slideOutLeft');
            $('#vehicles_list').addClass('slideInLeft');
            $('#vehicles_list').show();
            $('#vehicles_list').attr('style','position:all');

            $('#vehicle_details').removeClass('slideInLeft');
            $('#vehicle_details').addClass('slideOutRight');
            $('#vehicle_details').attr('style','position:absolute');  

            

            $('#assurance_panel').removeClass('slideInLeft');
            $('#assurance_panel').addClass('fadeOut'); 

            $('#carte_panel').removeClass('slideInLeft');
            $('#carte_panel').addClass('fadeOut');
          //  $('#assurance_panel').attr('style','position:'); 

            $('#affectation_panel').removeClass('slideInLeft');
            $('#affectation_panel').addClass('fadeOut');

            $('#maintenance_panel').removeClass('slideInLeft');
            $('#maintenance_panel').addClass('fadeOut'); 

            $('#driver_panel').removeClass('slideInLeft');
            $('#driver_panel').addClass('fadeOut');        

            $('#status_panel').removeClass('slideInLeft');
            $('#status_panel').addClass('fadeOut');
          //  $('#affectation_panel').attr('style','position:absolute');



            setTimeout(function() {
                $('#vehicle_details').hide();
                $('#affectation_panel').hide();
                $('#assurance_panel').hide();
                $('#carte_panel').hide();
                $('#maintenance_panel').hide();
                $('#driver_panel').hide();
                $('#status_panel').hide();
            }, 800);
            



    }
     function clear_disable_elements(_class) {
                    // $('.'+_class).attr('disabled',true);
                    $('.'+_class).val('');
                    $('.'+_class).selectpicker('val','');
                    $('.selectpicker').selectpicker('refresh');
                } 

    $(document).ready(function() {

        
            
    $(document).on('hide.bs.modal','#modal_history', function () {
    // $('#history_table thead').html('');
    //  $('#history_table');
    //Do stuff here
    });
    $("#demo01").animatedModal({
    animatedIn:'zoomIn',
    animatedOut:'bounceOut',
    color:'#FFFFFF',
    height: 860,
    width: 970,
    beforeOpen: function() {
    var children = $(".thumb");
    var index = 0;
    function addClassNextChild() {
    if (index == children.length) return;
    children.eq(index++).show().velocity("transition.expandIn", { opacity:1, stagger: 250 });
    window.setTimeout(addClassNextChild, 200);
    }
    addClassNextChild();
    },
    afterClose: function() {
    $(".thumb").hide();
    
    }
    });
    $('#myTable').DataTable();
    $(document).ready(function() {
    var table = $('#example').DataTable({
    "columnDefs": [{
    "visible": false,
    "targets": 2
    }],
    "order": [
    [2, 'asc']
    ],
    "displayLength": 25,
    "drawCallback": function(settings) {
    var api = this.api();
    var rows = api.rows({
    page: 'current'
    }).nodes();
    var last = null;
    api.column(2, {
    page: 'current'
    }).data().each(function(group, i) {
    if (last !== group) {
    $(rows).eq(i).before(
    '<tr class="group"><td colspan="5">' + group + '</td></tr>'
    );
    last = group;
    }
    });
    }
    });
    // Order by the grouping
    $('#example tbody').on('click', 'tr.group', function() {
    var currentOrder = table.order()[0];
    if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
    table.order([2, 'desc']).draw();
    } else {
    table.order([2, 'asc']).draw();
    }
    });
    });
    });
    //$('#example23').footable();
    $('#example23').DataTable({
    "language": {
    "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
    "zeroRecords": "Aucun résultat trouvé",
    "info": "Affichage du la page _PAGE_ de _PAGES_",
    "infoEmpty": "Table vide!",
    "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
    "sSearch" : "Rechercher",
    "oPaginate": {
    "sFirst":    "Premier",
    "sLast":    "Dernier",
    "sNext":    "Suivant",
    "sPrevious": "Précédent"
    }
    }, 
   
    responsive: {
            details: {
                type: 'column',
                target: 'tr'
            }
        },
        
    dom: 'Bfrtip',
    
    buttons: [
        
    {
    extend: 'print',
    text: 'IMPRIMER',
    exportOptions: {
        columns: ':visible'
                }
    },
    {
    extend: 'excelHtml5',
    text:      'EXCEL',
        exportOptions: {
            columns: ':visible'
                }
    },
    {
    extend: 'pdfHtml5',
    text: 'PDF',
    
    exportOptions: {
        columns: ':visible'
                }
    },
    {
            extend: 'colvis',
            text:'AFFICHAGE',
            columnText: function ( dt, idx, title ) {
                return (idx+1)+': '+title;
            }},
    
    ],
     
    columnDefs: [
     {columns: ':gt(0)', orthogonal: 'export'},
    { "orderable": false, "targets": [0]},
    { "searchable": false, "targets": [0]},
    { "printable": false, "targets": [0]},
    

    { "width": "10px", "targets": [0] },
    { "width": "20px", "targets": [6] },
    { "width": "100%", "targets": [5] },
    { "width": "5%", "targets": [3] }
    ],
    });
    $('#example24').DataTable({
    "language": {
    "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
    "zeroRecords": "Aucun résultat trouvé",
    "info": "Affichage du la page _PAGE_ de _PAGES_",
    "infoEmpty": "Table vide!",
    "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
    "sSearch" : "Rechercher",
    "oPaginate": {
    "sFirst":    "Premier",
    "sLast":    "Dernier",
    "sNext":    "Suivant",
    "sPrevious": "Précédent"
    }
    } ,
    dom: 'Bfrtip',
    buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    });       $('#example25').DataTable({
    "language": {
    "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
    "zeroRecords": "Aucun résultat trouvé",
    "info": "Affichage du la page _PAGE_ de _PAGES_",
    "infoEmpty": "Table vide!",
    "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
    "sSearch" : "Rechercher",
    "oPaginate": {
    "sFirst":    "Premier",
    "sLast":    "Dernier",
    "sNext":    "Suivant",
    "sPrevious": "Précédent"
    }
    } ,
    dom: 'Bfrtip',
    buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    });       $('#example26').DataTable({
    "language": {
    "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
    "zeroRecords": "Aucun résultat trouvé",
    "info": "Affichage du la page _PAGE_ de _PAGES_",
    "infoEmpty": "Table vide!",
    "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
    "sSearch" : "Rechercher",
    "oPaginate": {
    "sFirst":    "Premier",
    "sLast":    "Dernier",
    "sNext":    "Suivant",
    "sPrevious": "Précédent"
    }
    } ,
    dom: 'Bfrtip',
    buttons: [
    'copy', 'csv', 'excel', 'pdf', 'print'
    ]
    });
    </script>
    
    <!--Style Switcher -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    
</body>
</html>