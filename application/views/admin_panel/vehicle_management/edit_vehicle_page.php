<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

 <link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
    <title>CNAS | Gestion PARC  AUTO</title>
    <!-- Bootstrap Core CSS -->
    <!-- Bootstrap Core CSS -->
    <link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- animation CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/css/font-awesome-animation.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">

    <!-- Menu CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- page CSS -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.css" rel="stylesheet" />

    <link href="<?php echo base_url(); ?>plugins/bower_components/multiselect/css/multi-select.css" rel="stylesheet" type="text/css" />
  <link rel="stylesheet" href="<?php echo base_url(); ?>plugins/js/sweetalert2.css">

    <!-- Date picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    
    <link href="<?php echo base_url(); ?>plugins/js/jquery.filer.css" rel="stylesheet">

    <!-- Daterange picker plugins css -->
    <link href="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- animation CSS -->
   
    <!-- Custom CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme" rel="stylesheet">






    
       <link href="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url(); ?>plugins/css/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />


    


   
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
            #btn-close-modal {
                width:100%;
                text-align: center;
                cursor:pointer;
                color:#fff;
            }

        </style>
</head>

<body class="fix-sidebar">
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">



        <div id="idle-timeout-dialog" data-backdrop="static" class="modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;" >
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title">Your session is expiring soon</h4> </div>
                                        <div class="modal-body">
                                            <p>
                                                <i class="fa fa-warning font-red"></i> You session will be locked in
                                                <span id="idle-timeout-counter"></span> seconds.</p>
                                            <p> Do you want to continue your session? </p>
                                        </div>
                                        <div class="modal-footer text-center">
                                            <button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-outline btn-success" data-dismiss="modal">Yes, Keep Working</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
        <!-- Top Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="accueil"><b><!--This is dark logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo.png" alt="home" class="dark-logo" /><!--This is light logo icon--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-logo-dark.png" alt="home" class="light-logo" /></b><span class="hidden-xs"><!--This is dark logo text--><img src="<?php echo base_url(); ?>plugins/images/parcautom-text.png" alt="home" class="dark-logo" /><!--This is light logo text--><img src="<?php echo base_url(); ?>plugins/images/eliteadmin-text-darkl.png" alt="home" class="light-logo" /></span></a></div>
                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    <li>
                        <!-- <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control">
                            <a href=""><i class="fa fa-search"></i></a>
                        </form> -->
                    </li>
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown"> <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-settings"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <!-- <li><a href="#"><i class="ti-settings"></i> Account Setting</a></li> -->
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <div class="user-profile">
                    <div class="dropdown user-pro-body">
                        <div><img src="<?php echo base_url(); ?>plugins/images/user-me.jpg" alt="user-img" class="img-circle"></div>
                        <a href="#" class="dropdown-toggle u-dropdown text-success" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata('username'); ?><span class="caret"></span></a>
                        <ul class="dropdown-menu animated flipInY">
                            <li><a href="javascript:void(0);"><i class="ti-user"></i> Mon Profile</a></li>
                            <!-- <li><a href="#"><i class="ti-wallet"></i> My Balance</a></li> -->
                            <!-- <li><a href="#"><i class="ti-email"></i> Inbox</a></li> -->
                            <!-- <li role="separator" class="divider"></li> -->
                            <li><a href="javascript:void(0);"><i class="ti-settings"></i> Options du compte</a></li>
                            <li role="separator" class="divider"></li>
                            <li><a href="logout"><i class="fa fa-power-off"></i> Déconnexion</a></li>
                        </ul>
                    </div>
                </div>
                <ul class="nav" id="side-menu">
                    <!-- <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                       
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search...">
                            <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                       
                    </li> -->
                    <li class="nav-small-cap m-t-10 text-danger">--- Menu principal</li>
                    <li> <a href="accueil" class="waves-effect "><i class="icon-home text-danger" ></i> <span class="hide-menu text-danger"> Accueil </span></a></li>
                    <li> <a href="account_settings" class="waves-effect"><i class="linea-icon linea-basic fa-fw text-danger" data-icon="P"></i> <span class="hide-menu text-danger"> Options du compte </span></a></li>


                    <li class="nav-small-cap text-warning">--- Gestion du parc</li>
                    <li><a href="javascript:void(0);" class="waves-effect active"><i class="linea-icon linea-basic fa fa-car text-warning" ></i> <span class="hide-menu text-warning" >Véhicules<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_vehicles" >Tous les véhicules</a></li>
                            <li> <a href="add_vehicle_page">Ajouter un véhicule</a></li>
                            <li> <a href="edit_vehicle_page" class="active">Modifier un véhicule</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect "><i class="icon-speedometer text-warning" ></i> <span class="hide-menu text-warning">Chauffeurs<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_drivers" class="">Tous les chauffeurs</a></li>
                            <li> <a href="add_driver_page">Ajouter un chaufeur</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-equalizer text-warning" ></i> <span class="hide-menu text-warning">Entretiens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_maintenances_page" class="">Liste des entretiens</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="wi wi-fire  text-warning" ></i> <span class="hide-menu text-warning">Accidents<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_accidents_page" class="">Liste des accidents</a></li>
                        </ul>
                    </li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="icon-wrench text-warning" ></i> <span class="hide-menu text-warning">Reparations<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparations_page" class="">Liste des Reparations</a></li>
                        </ul>
                    </li>

            <li class="nav-small-cap text-blue">--- Gestion des tables</li>
                    <li><a href="javascript:void(0);" class="waves-effect"><i class="ti-shield text-blue" ></i> <span class="hide-menu text-blue">Assurances<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="compagnies_assurances">Liste des compagnies</a></li>
                        </ul>
                    </li>

                    <li><a href="javascript:void(0);" class="waves-effect"><i class=" ti-hummer text-blue" ></i> <span class="hide-menu text-blue">Mécaniciens<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="all_reparateurs_page">Liste des mécaniciens</a></li>
                        </ul>
                    </li>   
                 

                    
                    
                    
                    <li><a href="logout" class="waves-effect"><i class="icon-logout fa-fw"></i> <span class="hide-menu">Déconnexion</span></a></li>
            
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->
        <!-- Page Content -->
        <div id="page-wrapper">
            
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">

<!-- Page title goes here --> <h4 class="page-title">Modifer un véhicule</h4>

                    </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
 
                        <ol class="breadcrumb">
                                                <li ><a href="accueil">Accueil</a></li>
                                                <li class="active">Véhicules</li>
                                                <li class="active">Modifier</li>
<!-- Breadcrumb goes here : Accueil/.....  -->                            
                       
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>

                <div class="row ">




 <!-- Page content starts here -->
                    <div class="col-md-12 animated  " id="vehicles_list">
                       <div class="white-box ">
                       <h4 class="box-title m-b-0"><b>Liste des véhicules</b></h4>
                            <br>
                            <div class="table table-hover ">
                                <table id="example23" class="table hover-table table-default" cellspacing="0" width="100%">
                                    <thead>
                                        <tr class="btn-default">
                                            <th class="text-center">#</th>
                                            <th class="text-center">Marque</th>
                                            <th class="text-center">Modèle</th>
                                            <th class="text-center">Motorisation</th>
                                            <th class="text-center">Immatriculation</th>
                                            <th class="text-center">Type</th>
                                            <th class="text-center">GPS</th>
                                            <th class="text-center">Etat général</th>
                                        </tr>
                                    </thead>
                                
                                    <tbody id="tbl" style="cursor: pointer;">
                                        <?php foreach ($vehicles as $vehicle) {
                                            // echo '<div class="dropdown">'; 
                                                echo '<tr id="tr_'.$vehicle["ID"]. '">';

                     

                                    echo '<td><div class="radio radio-info">';
                                       echo  '<input class = "radios" type="radio" name="radio" id="radio_' .$vehicle["ID"]. '" value="'.$vehicle["ID"]. '"  >';
                                       echo  '<label for="radio_' .$vehicle["ID"]. '"> </label>';
                                   echo  '</div></td>';
                                              

                                             echo '<td >' . $vehicle['marque']. '</td>' ;
                                             echo '<td >' . $vehicle['modele']. '</td>' ;
                                             echo '<td >' . $vehicle['motorisation']. '</td>' ;
                                             echo '<td class="text-center">' . $vehicle['numero_immatriculation']. '</td>' ;
                                             echo '<td >' . $vehicle['type_vehicle']. '</td>' ;
                                             echo '<td class="text-center">' . $vehicle['gps_tracker']. '</td>' ;
                                             echo '<td >';
                                             
                                                 switch ($vehicle['etat']) {
                                                 case 'NEUF':
                                                 echo '<span class="label label-table label-success">NEUF</span>';   
                                                     break; 
                                                 case 'BON ETAT':
                                                 echo '<span class="label label-table" style="background-color :#D2FF00">BON ETAT</span>';  
                                                     break;
                                                 case 'ETAT MOYEN':
                                                 echo '<span class="label label-table label-warning">ETAT MOYEN</span>';   
                                                     break;
                                                 case 'MAUVAIS ETAT':
                                                 echo '<span class="label label-table label-danger">MAUVAIS ETAT</span>';  
                                                     break; 
												 case 'A REFORMER':
                                                 echo '<span class="label label-table" style="background-color :#979785">A REFORMER</span>';  
                                                 break;
                                             }
                                             echo '</td>' ;
                                             echo '</tr>';
                                        } ?>
                                    </tbody>
                                </table>
                            </div>

                            <br>
                            
                            
                                


<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" id="modal_history" data-animation="true">
                                <div class="modal-dialog modal-lg" >
                                    <div class="modal-content" style="width: 115%; margin-left: -7%; margin-top: 15%">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                            <h4 class="modal-title" id="myLargeModalLabel">Historique Affectation</h4>
                                        </div>
                                        <div class="modal-body">
                                                <div class="table-responsive table-bordered" id="div_affectation_table"
                                        style="display: none;">
                                            <table class="table hover-table table-default" id="affectation_table">
                                                <thead>
                                                    <tr>
                                                     <th>Structure</th>   
                                                     <th>Responsable</th>   
                                                     <th>Type</th>   
                                                     <th>Date</th>   
                                                     <th>Etat véhicule</th>   
                                                     <th>KM</th>   
                                                     <th>N° décision</th>   
                                                     <th>Decision</th>   
                                                    </tr>
                                                </thead>
                                                <tbody>
                                         
                                                </tbody>
                                            </table>
                                        </div>  
                                        <div class="table-responsive table-bordered" id="div_assurance_table"
                                        style="display: none;">
                                            <table class="table hover-table table-default" id="assurance_table">
                                                <thead>
                                                    <tr>
                                                     <th>Compagnie d'assurance</th>   
                                                     <th>Date début d'échéance</th>   
                                                     <th>Date fin d'échéance</th>   
                                                    </tr>
                                                </thead>
                                                <tbody>
                                         
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="table-responsive table-bordered" id="div_accidents_table">
                                            <table class="table hover-table table-default" id="accidents_table"
                                            style="display: none;">
                                                <thead>
                                                    <tr>
                                                     <th> Sinistre</th>   
                                                     <th> Date sinistre</th>   
                                                     <th> Décalaration</th>   
                                                    </tr>
                                                </thead>
                                                <tbody>
                                         
                                                </tbody>
                                            </table>
                                        </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Fermer</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>

                  
                </div>
                </div>

     <div class="col-sm-12 animated" style=" display: none;" id="vehicle_details">
     <div class="panel panel-default">
                            <div class="panel-heading">RENSEIGNEMENTS DU VEHICULE
                           
                                <div class="panel-action">
   
                            <a href="javascript:void(0)" onclick="delete_vehicle()" data-placement="top" title="Supprimer" data-toggle="tooltip">
                            <i class="text-danger fa fa-trash faa-bounce animated" style="font-size: 20px" tooltip="lol"></i>
                            </a>  
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;               
                            <a href="javascript:void(0)" onclick="back_to_list()"><i class="ti-arrow-left" style="font-size: 17px"></i></a>  
                                </div>
                            </div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">


        <div class="row ">
            <div class="col-sm-4 form-group">
             <h5 class="m-t-30 m-b-10"><b>MARQUE</b></h5> 
             <div id="marques">
              <input class=" form-control information" type="text" placeholder="HYUNDAI, BMW,..." id="marque" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>
          </div>
      </div>
      <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>MODELE</b></h5>
        <input class="form-control information" type="text" placeholder="ELANTRA, ACCENT,..." id="modele" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>   </div>
    <div class="col-md-4">
        <h5 class="m-t-30 m-b-10"><b>MOTORISATION</b></h5>
        <select class="selectpicker show-tick information" data-live-search="false" title="Sélectionner.." data-style="form-control" id="motorisation" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div> 
            <option>DIESEL</option>
            <option>ESSENCE</option>
            <option>GPL</option>
            <option>ESSENCE / GPL</option>
        </select>
    </div>
    <div class="col-md-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>TYPE</b></h5>
        <select class="selectpicker show-tick information" data-live-search="false" title="Sélectionner.." data-style="form-control" id="type_vehicle" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>           <div class=""></div>
            <option>AMBULANCE</option>
            <option>ENGIN</option>
            <option>CAMION</option>
            <option>VEHICULE TOURISTIQUE</option>
            <option>VEHICULE UTILITAIRE</option>
            <option>CAMION</option>
        </select>
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>VEHICULE TRACKER PAR GPS</b></h5>
        <select class="selectpicker show-tick information" title="Sélectionner.." data-style="form-control" id="gps_tracker" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>           <div class=""></div>
            <option>OUI</option>
            <option>NON</option>

        </select>
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>ETAT GENERAL DU VEHICULE</b></h5>
        <select class="selectpicker show-tick information" title="Sélectionner.." data-style="form-control" id="etat" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>           
            <option class="text-success">NEUF</option>
            <option style="color :#D2FF00 ">BON ETAT</option>
            <option class="text-warning">ETAT MOYEN</option>
            <option class="text-danger">MAUVAIS ETAT</option>
			<option class="text-danger">A REFORMER</option>
        </select>
    </div>

                     <!--            <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input class="form-control kilometrage" id="kilometrage" >
                                </div>  -->   
                            </div>
                            <div class="row ">
                                <div class="col-sm-4 form-group">
                                 <h5 class="m-t-30 m-b-10"><b>N° CHASSIS</b></h5> 
                                 
                                 <input class=" form-control information" type="text"   id="numero_chassis" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                             </div>
                             <div class="col-sm-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION</b></h5>
                                <input class="form-control  information"  placeholder="_______-___-__"
                                id="numero_immatriculation" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                           </div>
                            <div class="col-md-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION (Ancienne/Provisoire)</b></h5>
                                <input class="form-control  information"  placeholder="_______-___-__"
                                id="numero_immatriculation_old" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                           </div>


                        </div>
                                                <hr>    
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                                    <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_inssurance_btn" onclick="enable_elements('information');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                <button class=" information fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="update_vehicle_btn" onclick="update_vehicle()" disabled="true">Sauvgarder</button>
                            </div> 
                                      </div>    
                    </div>   
  </div>
  </div>                  
  </div>                  
  </div>                  




            <div class=" col-md-12 animated" id="status_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'état du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
                  
   
            <div class="col-sm-12 form-group">
                
                <h4 class="box-title m-b-0 text-muted">ETAT EXTERIEUR</h4> 
            </div>
        <br>
        <?php foreach ($exterior_parts as $exterior_part) {
           echo '<div class="col-sm-6 form-group" id="'.$exterior_part['id'].'_div_exterior_status">';
        echo '<h5 class="m-t-30 m-b-10" id="'.$exterior_part['id'].'_exterior_part_name"><b>'.$exterior_part['part'].'</b></h5>';
        echo '<input class="form-control status" type="text" placeholder="Insérer détail..." id="exterior_part_status_'.$exterior_part['id'].'" data-error="Veuillez renseigner ce champ." required disabled="true"><div class="help-block with-errors"></div></div>' ;
        } ?>

      
      </div>

      <div class="row ">
            <div class="col-sm-12 form-group">
                <hr>
                <h4 class="box-title m-b-0 text-muted">ETAT INTERIEUR</h4> 
            </div>
            <br>
             <?php foreach ($interior_parts as $interior_part) {
           echo '<div class="col-sm-6 form-group" id="'.$interior_part['id'].'_div_interior_status">';
        echo '<h5 class="m-t-30 m-b-10" id="'.$interior_part['id'].'_interior_part_name"><b>'.$interior_part['part'].'</b></h5>';
        echo '<input class="form-control status" type="text" placeholder="Insérer détail..." id="interior_part_status_'.$interior_part['id'].'" data-error="Veuillez renseigner ce champ." required disabled="true"><div class="help-block with-errors" ></div></div>' ;
        } ?>


      
  </div> 
                
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                           
                                      <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_inssurance_btn" onclick="enable_elements('status');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <button class="status fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="update_status_btn" onclick="update_status()" disabled="true">Sauvgarder</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div>

<!--                       <div class=" col-md-12 animated" id="maintenance_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    ENTRETIENS  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
                         <div class="col-md-9">
                <h5 class="m-t-30 m-b-10"><b>TYPE ENTRETIEN (S)</b></h5>

        <select class="selectpicker show-tick entretien" multiple  title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required  id="type_entretien" disabled="true" size="true" data-size="4">




                        <?php foreach ($entretiens as $entretien)
                                                {
                          echo '<option';
                          //echo ' value = "'.$entretien['Id'];
                          echo '> ';
                          echo $entretien['nom'].'</option>';
                                                } ?>
                                    </select>
                                </div>
                        <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>COÛT</b></h5> 
                         
                          <input class="form-control entretien cout" type="text" id="cout" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div>                        
                  </div> 
                <div class=" col-sm-4">
                         <h5 class="m-t-30 m-b-10"><b>DATE</b></h5> 
                         
            <input type="text" class="form-control entretien datepicker-autoclose" placeholder="jj/mm/aaaa" disabled="true" id="date_entretien">
       
                                        
                          
                  </div> 
                  <div class=" col-sm-4">
                         <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5> 
                         
                          <input class="form-control entretien numero" type="text" id="kilometrage_entretien" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div>                        
                  </div>

                  
         
                </div>
                
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">
                      
                           
                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_maintenance_btn" onclick="enable_elements('entretien');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <button class="entretien fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_driver_btn" onclick="add_maintenance()" disabled="true">Sauvgarder</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div>  -->

            <div class=" col-md-12 animated" id="carte_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    Détails carte naftal  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                      <div class="row"> 

                        <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>NUMERO CARTE</b></h5> 
                         
                          <input class="form-control carte" type="text" id="numero_carte" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div>                        
                  </div> 
                <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>DATE EXPIRATION</b></h5> 
                         
                          <input class="form-control datepicker carte"  id="expiration_carte" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div>                        
                  </div>
                   <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>DEBIT</b></h5> 
                         
                      <input class="form-control carte cout" type="text" id="debit_carte" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div> 
                  </div> 
                   <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>CREDIT</b></h5> 
                         
                      <input class="form-control carte cout" type="text" id="credit_carte" data-error="Veuillez renseigner ce champ." required disabled="true">
                        <div class="help-block with-errors"></div> 
                  </div> 
                  
         
                </div>
                
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                           
                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_carte_btn" onclick="enable_elements('carte');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <button class="carte fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_carte_btn" onclick="edit_carte_naftal()" disabled="true">Sauvgarder</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div>    
                      <div class=" col-md-12 animated" id="assurance_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'assurance du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 

        <div class="col-md-6">
        <h5 class="m-t-30 m-b-10"><b>COMPAGNIE D'ASSURANCE</b></h5>

        <select class="selectpicker show-tick assurance" title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required disabled="true" id="compagnie_assurance" onchange="$('#adresse').val($('#compagnie_assurance').find('option:selected').attr('adresse'));
        $('#telephone').val($('#compagnie_assurance').find('option:selected').attr('telephone'));
        $('#numero_police').val($('#compagnie_assurance').find('option:selected').attr('numero_police'));"
        >


                                            <?php foreach ($compagnies as $compagnie)
                                                {
                          echo '<option';
                          echo ' value = "'.$compagnie['id'];
                          echo '" adresse = "'.$compagnie['adresse'] ;
                          echo  '" telephone="'. $compagnie['telephone'];
                          echo  '" numero_police="'. $compagnie['numero_police'];
                          echo '" >';
                          echo $compagnie['nom'].'</option>';
                                                } ?>
                                    </select>
                                </div>
                  <div class="col-sm-3">
                     <h5 class="m-t-30 m-b-10"><b>NUMERO POLICE D'ASSURANCE</b></h5> 
                     <div id="car-brands">
                      <input class="form-control numero " type="text" id="numero_police" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                 </div>   
              </div>
                <div class="col-sm-3">
                     <h5 class="m-t-30 m-b-10"><b>NUMERO TELEPHONE</b></h5> 
                     <div id="car-brands">
                      <input class="form-control numero " type="text" id="telephone" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                 </div>   
              </div>
                <div class=" col-sm-6">
                         <h5 class="m-t-30 m-b-10"><b>ADRESSE</b></h5> 
                         
                          <input class="form-control " type="text" id="adresse" data-error="Veuillez renseigner ce champ." required readonly="true">
                        <div class="help-block with-errors"></div>                        
                  </div>
              <div class="col-sm-6">
                <h5 class="m-t-30 m-b-10 "><b>DATE D'ECHEANCE</b></h5>
                <div class="input-daterange input-group" id="date-range">
                    <span class="input-group-addon bg-info b-0 text-white ">Du</span>
                    <input type="text" class="form-control assurance" name="start" id="date_debut" disabled="true"/>
                    <span class="input-group-addon bg-info b-0 text-white">au</span>
                    <input type="text" class="form-control assurance" name="end" id="date_fin" disabled="true" />
                </div>
                    </div> 
                    </div> 
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                           
                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_inssurance_btn" onclick="enable_elements('assurance');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <button class="assurance fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_inssurance_btn" onclick="add_inssurance()" disabled="true">Sauvgarder</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div>   

             <div class=" col-md-12 animated" id="affectation_panel" style="display: none;">
                <div class="panel " style="background-color: #BEBEBE;">

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    L'affectation du véhicule.  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;" id="affectation_collapse">
                       <div class="row"> 

 <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>DESIGNATION</b></h5>

                                    <select class="selectpicker show-tick" data-live-search="false" title="Sélectionner.." data-style="form-control" id="designation"
                                    onchange="show_structure(this.value)"  >
                                            <?php foreach ($types as $type)
                                                {
                                                  echo '<option >'. $type['nom']. '</option>';
                                                } ?>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>NOM</b></h5>
       <!--   <input type="text" class="form-control" id="structure_affectation" readonly="true"
          id_structure_affectation =" "/>-->
                                    <select class="selectpicker show-tick " data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="structure_affectation" data-hide-disabled= 'true' onchange="get_wilaya()">
                                                                                   
                                        <?php foreach ($types as $type)
                                                {
                                                    echo '<optgroup disabled = "true" label="' .$type['nom']. '">';  
                                                  foreach ($structures as $structure) {
                                                      if ($structure['type'] == $type['nom'] ) {
                                                        
                                                        echo '<option value ="' .$structure['id']. '" code_wilaya ="' .$structure['code_wilaya']. '" >'. $structure['nom'] . '</option>';
                                                          
                                                      }
                                                  }
                                                  
                                                } echo '</optgroup>'; ?>
                                    </select> 

                                    <script type="text/javascript">
                                        function show_structure(type) {
                                            $('#code_wilaya').val('');    
                                           $('.selectpicker').find('[label]').attr('disabled',true);              
                                           $('.selectpicker').find("[label='"+type+"']").attr('disabled',false);
                                           $('.selectpicker').selectpicker('refresh');}

                                        function get_wilaya() {
                                            $('#code_wilaya').val('');
                                            var value = $('#structure_affectation').val();
                                            var code_wilaya = $('.selectpicker').find("[value='"+value+"']" ).attr('code_wilaya'); 
                                               $('#code_wilaya').val(code_wilaya);
                                           }   
                                       </script>
                                </div>
                                <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>CODE WILAYA</b></h5>
                                    <input type="text" class="form-control" id="code_wilaya" readonly="true" />
                                    <input type="text" class="form-control" id="id_selected_vehicle" readonly="true" style="display: none;" />
                                </div>
                                <div class="col-md-6">
                                    <h5 class="m-t-30 m-b-10"><b>DETAIL AFFECTATION</b></h5>
        <input type="text" class="form-control" id="detail_affectation_" readonly="true"
          id_priorietaire =" "/>
                                   <div id="detail_affectation__" style="display: none;">
                                    <select class="selectpicker test show-tick" data-live-search="true" title="Sélectionner.." data-style="form-control"  size="true" data-size="7" id="detail_affectation" data-hide-disabled= 'true'>
                                                 <?php foreach ($types as $type)
                                                {
                                                    echo '<optgroup disabled = "true" label="' .$type['nom']. '">';  
                                                  foreach ($priorietaires as $priorietaire) {
                                                      if ($priorietaire['type'] == $type['nom'] ) {
                                                        
                                                        echo '<option value ="' .$priorietaire['id']. '" >'.
                                                         $priorietaire['nom'] . '</option>';
                                                          
                                                      }
                                                  }
                                                  
                                                } echo '</optgroup>'; ?>

                                    </select>
                                   </div> 
                                 
                                </div>  
                                    <div class="col-sm-3 form-group">
                                        <h5 class="m-t-30 m-b-10"><b>ETAT DU VEHICULE</b></h5>
                                        <select class="selectpicker show-tick affectation" title="Sélectionner.." data-style="form-control" id="etat_vehicle" data-error="Veuillez renseigner ce champ." required disabled="true">
                                              <div class="help-block with-errors"></div>           
                                            <option class="text-success">NEUF</option>
                                            <option style="color :#D2FF00 ">BON ETAT</option>
                                            <option class="text-warning">ETAT MOYEN</option>
                                            <option class="text-danger">MAUVAIS ETAT</option>
                                            <option class="text-danger">A REFORMER</option>
                                        </select>
                               </div>
<!--                                 <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input type="text" class="form-control numero affectation" id="kilometrage" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>
                                </div> -->
                                 <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>TYPE AFFECTATION</b></h5>
                                    
                                    <select class="selectpicker show-tick affectation" title="Sélectionner.." data-style="form-control"  id="type_affectation" required="true" disabled="true">

                                        <option>PROVISOIRE</option>
                                        <option>PERMANENT</option>
                                       </select>   
                                </div> 

                               
                                <div class="col-md-2">
                                    <h5 class="m-t-30 m-b-10"><b>NUMERO DECISION</b></h5>
                                    <input type="text" class="form-control numero affectation" id="numero_decision" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>
                                </div> 
                                <div class="col-md-7">
                                    <h5 class="m-t-30 m-b-10"><b>DECISION SCANNEE</b></h5>
                                    <iframe name="hiddenFrame" style="position:absolute; top:-1px; left:-1px; width:1px; height:1px;display: none;"></iframe>
                                    <a href="" target="_blank" id="decision_document"><i class="fa fa-file-pdf-o"  style="font-size:35px"></i></a>
               <form action="upload_decision" method="post" enctype="multipart/form-data" target="hiddenFrame" id="decision_form" style="display: none;">
                
              <input type="file" name="files" id="filer_input" single >
              
              <input type="text" name="id_inserted_affectation" id="id_inserted_affectation" style="display: none;">
               <!-- <input type="submit" value="Submit">  -->
         
        </form>
                                </div> 
                      </div>
                    
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                                  <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_inssurance_btn" onclick="enable_elements('affectation');
                                $('#decision_document').hide();$('#decision_form').show();
                                $('#detail_affectation__').show();
                                $('#detail_affectation_').hide();">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                <button class="affectation fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_affectation_btn" onclick="add_affectation()" disabled="true">Sauvgarder</button>
                            </div> 
                        </div>
                    </div>
                    </div> 

                        
                       
                    
                    
                </div> 
                 
            </div> 
        <div class=" col-md-12 animated" id="driver_panel" style="display:none;" >

                <div class="panel " style="background-color: #BEBEBE;" >

                    <div class="panel-heading"><a href="javascript;" data-perform="panel-collapse">
                    CHAUFFEUR  </a> 
                    <div class="pull-right">
                        <a href="javascript;" data-perform="panel-collapse">
                            <i class="ti-plus"></i></a>
                        </div>
                    </div>

                    <div class="white-box panel-wrapper collapse" aria-expanded="false" style="border-color: #FFE7C2;border-style: dashed;">
                       <div class="row"> 
                         <div class="col-md-3">
                                    <h5 class="m-t-30 m-b-10"><b>CHAUFFEUR</b></h5>

        <select class="selectpicker show-tick chauffeur" title="Sélectionner.." data-style="form-control"  data-error="Veuillez renseigner ce champ." required disabled="true" id="chauffeur_" onchange="$('#type_permis').val($('#chauffeur_').find('option:selected').attr('type_permis'));
        $('#validite_permis').val($('#chauffeur_').find('option:selected').attr('validite_permis'));" disabled="true" >




                                            <?php foreach ($chauffeurs as $chauffeur)
                                                {
                          echo '<option';
                          echo ' value = "'.$chauffeur['id'];
                         // echo '" title = "'.$chauffeur['nom'].' '.$chauffeur['prenom'];
                          echo  '" type_permis='. $chauffeur['type_permis'];
                          echo  ' validite_permis='. $chauffeur['validite_permis'];
                          echo ' >';
                          echo $chauffeur['nom'].'</option>';
                                                } ?>
                                    </select>
                                </div>
                        <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>TYPE PERMIS CONDUIRE</b></h5> 
                         
                          <input class="form-control chauffeur" type="text" id="type_permis" data-error="Veuillez renseigner ce champ." required readonly="true">
                        <div class="help-block with-errors"></div>                        
                  </div> 
                <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>VALIDITE PERMIS CONDUIRE</b></h5> 
                         
                          <input class="form-control chauffeur" type="text" id="validite_permis" data-error="Veuillez renseigner ce champ." required readonly="true">
                        <div class="help-block with-errors"></div>                        
                  </div>
                   <div class=" col-sm-3">
                         <h5 class="m-t-30 m-b-10"><b>TYPE CHAUFFEUR</b></h5> 
                         
                       <select class="selectpicker show-tick chauffeur" data-live-search="false" title="Sélectionner.." data-style="form-control" id="type_chauffeur" disabled="true" >                        <option> OCCASIONNEL</option>
                                               <option> HABITUEL</option>
                         </select>
                  </div> 
                  
         
                </div>
                
                        <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">
                      
                           
                                <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_driver_btn" onclick="enable_elements('chauffeur');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <button class="chauffeur fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="add_driver_btn" onclick="add_driver()" disabled="true">Sauvgarder</button>
                            </div> 
                      
                        </div>
                      
                    </div>
            </div> 
         </div> 
               
             </div> 
                 
            </div>
</div>
</div>
<!-- / Page content ends here -->                
                <!-- .right-sidebar -->
                <div class="right-sidebar">
                    <div class="slimscrollright">
                        <div class="rpanel-title"> Service Panel <span><i class="ti-close right-side-toggle"></i></span> </div>
                        <div class="r-panel-body">
                            <ul>
                                <li><b>Layout Options</b></li>
                                <li>
                                    <div class="checkbox checkbox-info">
                                        <input id="checkbox1" type="checkbox" class="fxhdr">
                                        <label for="checkbox1"> Fix Header </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-warning">
                                        <input id="checkbox2" type="checkbox" checked="" class="fxsdr">
                                        <label for="checkbox2"> Fix Sidebar </label>
                                    </div>
                                </li>
                                <li>
                                    <div class="checkbox checkbox-success">
                                        <input id="checkbox4" type="checkbox" class="open-close">
                                        <label for="checkbox4"> Toggle Sidebar </label>
                                    </div>
                                </li>
                            </ul>
                            <ul id="themecolors" class="m-t-20">
                                <li><b>With Light sidebar</b></li>
                                <li><a href="javascript:void(0)" theme="default" class="default-theme">1</a></li>
                                <li><a href="javascript:void(0)" theme="green" class="green-theme">2</a></li>
                                <li><a href="javascript:void(0)" theme="gray" class="yellow-theme">3</a></li>
                                <li><a href="javascript:void(0)" theme="blue" class="blue-theme">4</a></li>
                                <li><a href="javascript:void(0)" theme="purple" class="purple-theme">5</a></li>
                                <li><a href="javascript:void(0)" theme="megna" class="megna-theme">6</a></li>
                                <li><b>With Dark sidebar</b></li>
                                <br/>
                                <li><a href="javascript:void(0)" theme="default-dark" class="default-dark-theme">7</a></li>
                                <li><a href="javascript:void(0)" theme="green-dark" class="green-dark-theme">8</a></li>
                                <li><a href="javascript:void(0)" theme="gray-dark" class="yellow-dark-theme working">9</a></li>
                                <li><a href="javascript:void(0)" theme="blue-dark" class="blue-dark-theme">10</a></li>
                                <li><a href="javascript:void(0)" theme="purple-dark" class="purple-dark-theme">11</a></li>
                                <li><a href="javascript:void(0)" theme="megna-dark" class="megna-dark-theme">12</a></li>
                            </ul>
                            <ul class="m-t-20 all-demos">
                                <li><b>Choose other demos</b></li>
                            </ul>
                            <ul class="m-t-20 chatonline">
                                <li><b>Chat option</b></li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/varun.jpg" alt="user-img" class="img-circle"> <span>Varun Dhavan <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/genu.jpg" alt="user-img" class="img-circle"> <span>Genelia Deshmukh <small class="text-warning">Away</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/ritesh.jpg" alt="user-img" class="img-circle"> <span>Ritesh Deshmukh <small class="text-danger">Busy</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/arijit.jpg" alt="user-img" class="img-circle"> <span>Arijit Sinh <small class="text-muted">Offline</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/govinda.jpg" alt="user-img" class="img-circle"> <span>Govinda Star <small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/hritik.jpg" alt="user-img" class="img-circle"> <span>John Abraham<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/john.jpg" alt="user-img" class="img-circle"> <span>Hritik Roshan<small class="text-success">online</small></span></a>
                                </li>
                                <li>
                                    <a href="javascript:void(0)"><img src="<?php echo base_url(); ?>plugins/images/users/pawandeep.jpg" alt="user-img" class="img-circle"> <span>Pwandeep rajan <small class="text-success">online</small></span></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- /.right-sidebar -->
            </div>
            <!-- /.container-fluid -->
            <footer class="footer text-center text-info"> 2018 &copy; www.cnas.dz </footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>

<script src="<?php echo base_url(); ?>plugins/js/inputmask.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.numeric.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/inputmask.phone.extensions.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/jquery.inputmask.js"></script>
       <!-- Bootstrap Core JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!-- Session-timeout-idle -->
<!--     <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/jquery.idletimeout.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/jquery.idletimer.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/session-timeout/idle/session-timeout-idle-init.js"></script> -->
    <!--slimscroll JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
    <script src="<?php echo base_url(); ?>plugins/js/animatedModal.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

       <script src="<?php echo base_url(); ?>plugins/bower_components/datatables/jquery.dataTables.min.js"></script>
    <!-- start - This is for export functionality only -->
    <script src="<?php echo base_url(); ?>/plugins/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/buttons.flash.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/jszip.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/pdfmake.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/vfs_fonts.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/buttons.html5.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/js/buttons.print.min.js"></script>
        <script src="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js"></script>
    <script src="<?php echo base_url(); ?>plugins/bower_components/custom-select/custom-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-tagsinput/dist/bootstrap-tagsinput.min.js"></script>


<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.min.js" type="text/javascript"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<!-- Date range Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="<?php echo base_url(); ?>/plugins/js/sweetalert2.min.js"></script>

<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/bower_components/multiselect/js/jquery.multi-select.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/jquery.filer.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>/plugins/js/validator.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/moment/moment.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/jquery-asColorPicker-master/libs/jquery-asGradient.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="<?php echo base_url(); ?>/plugins/bower_components/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="<?php echo base_url(); ?>/plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script type="text/javascript">

    function delete_vehicle() {
    
    
        var id_vehicle = $('#id_selected_vehicle').val();
    
    
    // add validation to inputs
    // end validation...
    // insert data to database
    //ex : '{"key":"val","key1":"val1"}'
    
    swal({
    title:"Attention!...",
    text:"Voullez-vous vraiment supprimer cet véhicule !",
    type:"warning",
    showCancelButton: true,
    confirmButtonColor: "#DD6B55",
    confirmButtonText: "Oui, Supprimer!",
    cancelButtonText: "Non",
    closeOnConfirm: true
    },
    function(){
    var data2db = JSON.parse(
    '{"ID":"' +id_vehicle+ '"}'
    );
    console.log(data2db);
    var url = "delete_vehicle";
    $.post(url,data2db).done(function( data ) {
    var reply = JSON.parse(data);
    console.log(reply);
    if (reply['message'] == 'data deleted') {
    swal({
    title:"Succès...",
    text:"Informations supprimées avec succès",
    type:"success"
    },
    function(){
    location.reload();
    });
    }else if (reply == 'error') {
    swal("Erreur...","Erreur lors la suppression du véhicule", "error");
    }
    // swal("ID CAS", mObj, "success");
    }).fail(function(data, textStatus, errorThrown){
    swal("Erreur...","erreur", "error");
    });
    });
    
    
    
    
    
    
    }


    function back_to_list() {
         $('#example23').DataTable().$('tr.selected').removeClass('selected');
         $("input:radio").attr('checked', false);
            clear_disable_elements('information');
            clear_disable_elements('status');
            clear_disable_elements('assurance');
            clear_disable_elements('affectation');
            clear_disable_elements('chauffeur');


          
            $('#id_selected_vehicle').val('');




            $('#vehicles_list').removeClass('slideOutLeft');
            $('#vehicles_list').addClass('slideInLeft');
            $('#vehicles_list').show();
            $('#vehicles_list').attr('style','position:all');

            $('#vehicle_details').removeClass('slideInLeft');
            $('#vehicle_details').addClass('slideOutRight');
            $('#vehicle_details').attr('style','position:absolute');  

            

            $('#assurance_panel').removeClass('slideInLeft');
            $('#assurance_panel').addClass('fadeOut'); 

            $('#carte_panel').removeClass('slideInLeft');
            $('#carte_panel').addClass('fadeOut');
          //  $('#assurance_panel').attr('style','position:'); 

            $('#affectation_panel').removeClass('slideInLeft');
            $('#affectation_panel').addClass('fadeOut');

            $('#maintenance_panel').removeClass('slideInLeft');
            $('#maintenance_panel').addClass('fadeOut'); 

            $('#driver_panel').removeClass('slideInLeft');
            $('#driver_panel').addClass('fadeOut');        

            $('#status_panel').removeClass('slideInLeft');
            $('#status_panel').addClass('fadeOut');
          //  $('#affectation_panel').attr('style','position:absolute');



            setTimeout(function() {
                $('#vehicle_details').hide();
                $('#affectation_panel').hide();
                $('#assurance_panel').hide();
                $('#carte_panel').hide();
                $('#maintenance_panel').hide();
                $('#driver_panel').hide();
                $('#status_panel').hide();
            }, 800);
            



    }
</script>

    <!-- end - This is for export functionality only -->
<script type="text/javascript">


                function enable_elements(_class) {
                    $('.'+_class).removeAttr('disabled');
                    $('.selectpicker').selectpicker('refresh');
                } 

                  function clear_disable_elements(_class) {
                    $('.'+_class).attr('disabled',true);
                    $('.'+_class).val('');
                    $('.'+_class).selectpicker('val','');
                    $('.selectpicker').selectpicker('refresh');
                }    
                      function disable_elements(_class) {
                    $('.'+_class).attr('disabled',true);
                    $('.selectpicker').selectpicker('refresh');
                }

        function add_inssurance() {


         var id_vehicle = $('#id_selected_vehicle').val();           
                    
        // Get values from inputs
            
            
            var id_compagnie = $('#compagnie_assurance').selectpicker('val');
            console.log(id_compagnie);
            var date_debut = $('#date_debut').val();
            var date_fin = $('#date_fin').val();
            var numero_police = $('#numero_police').val();
            if (compagnie_assurance != "" &&  date_debut != "" && date_fin != "" 
                && numero_police != "" ) {



                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"id_compagnie":"' +id_compagnie+ 
                                        '","date_debut":"' +date_debut+ 
                                        '","id_vehicle":"' +id_vehicle+ 
                                        '","date_fin":"' +date_fin+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "add_inssurance";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'data inserted') {
                                             swal({
                                                  title: "Succès!",
                                                  text: "Information ajoutée avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                             $('#assurance_panel').hide();

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       


    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");   
    }
    function edit_carte_naftal() {


         var id_vehicle = $('#id_selected_vehicle').val();           
                    
        // Get values from inputs
            
            
            var numero = $('#numero_carte').val();
            var debit = $('#debit_carte').val();
            var credit = $('#credit_carte').val();
            var expiration_carte = $('#expiration_carte').val();
            if (numero != "" &&  expiration_carte != "" && debit != "" 
                && credit != "" ) {



                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"numero":"' +numero+ 
                                        '","date_expiration":"' +expiration_carte+ 
                                        '","id_vehicle":"' +id_vehicle+ 
                                        '","debit":"' +debit+ 
                                        '","credit":"' +credit+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "update_carte_naftal";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'carte updated') {
                                             swal({
                                                  title: "Succès!",
                                                  text: "Information ajoutée avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                             $('#carte_panel').hide();

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Erreur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       


    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");   
    }
    
        function add_driver() {


         var id_vehicle = $('#id_selected_vehicle').val();           
                    
        // Get values from inputs
            
            
            var type_chauffeur= $('#type_chauffeur').val();
            var id_chauffeur = $('#chauffeur_').selectpicker('val');

            
           
            if (type_chauffeur != "" && id_chauffeur  != "") {



                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"type_chauffeur":"' +type_chauffeur+ 
                                        '","id_chauffeur":"' +id_chauffeur+ 
                                        '","id_vehicle":"' +id_vehicle+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "add_driver_vehicle";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'data inserted') {
                                             swal({
                                                  title: "Succès!",
                                                  text: "Information ajoutée avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                             //$('#assurance_panel').hide();
                                             disable_elements('driver');

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       


    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");   
    }

    function update_status() {


         var id_vehicle = $('#id_selected_vehicle').val();           
                    
        // Get values from inputs
          var exterior_parts  =  <?php echo json_encode($exterior_parts); ?>;
            var exterior_part_status = [];
            for (var i = exterior_parts.length - 1; i >= 0; i--) {
            exterior_part_status[i] =  exterior_parts[i]['id']+'_'+$('#exterior_part_status_'+exterior_parts[i]['id']).val();
   }
           var interior_parts  =  <?php echo json_encode($interior_parts); ?>;
                    var interior_part_status = [];
                    for (var i = interior_parts.length - 1; i >= 0; i--) {
                    interior_part_status[i] =  interior_parts[i]['id']+'_'+$('#interior_part_status_'+interior_parts[i]['id']).val();
           }  
            



                                    var data2db = JSON.parse(
                                        '{"id_vehicle":"' +id_vehicle+ 
                                         '","interior_status":"' + interior_part_status+
                                        '","exterior_status":"' + exterior_part_status + '"}' 
                                        ); 
                                    console.log(data2db);
                                       var url = "update_status";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'status updated') {
                                             swal({
                                                  title: "Succès!",
                                                  text: "Information ajoutée avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                            disable_elements('status');

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       


     
    }
    function update_vehicle() {


         var id_vehicle = $('#id_selected_vehicle').val();           
                    
        // Get values from inputs
            
            
            var marque = $('#marque').val();
            var modele = $('#modele').val();
            var motorisation = $('#motorisation').val();
            var numero_immatriculation = $('#numero_immatriculation').val();
            var gps_tracker = $('#gps_tracker').val();
            var numero_immatriculation_old = $('#numero_immatriculation_old').val();
            var etat = $('#etat').val();
            var type_vehicle = $('#type_vehicle').val();
            var numero_chassis = $('#numero_chassis').val();
             if (marque != "" &&  modele != "" && motorisation != "" 
                && numero_immatriculation != "" && gps_tracker != "" && gps_tracker != ""
                 && etat != "" && type_vehicle != "" && numero_chassis != "") {

                                    var data2db = JSON.parse(
                                        '{"marque":"' +marque+ 
                                        '","modele":"' +modele+ 
                                        '","id_vehicle":"' +id_vehicle+ 
                                        '","type_vehicle":"' +type_vehicle+ 
                                        '","motorisation":"' +motorisation+ 
                                        '","numero_immatriculation":"' +numero_immatriculation+ 
                                        '","numero_immatriculation_old":"' +numero_immatriculation_old+ 
                                        '","gps_tracker":"' +gps_tracker+ 
                                        '","etat":"' +etat+ 
                                        '","numero_chassis":"' +numero_chassis+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "update_vehicle";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'vehicle updated') {
                                             swal({
                                                  title: "Succès!",
                                                  text: "Information ajoutée avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });
                                             
                                            disable_elements('information');

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'ajout d'informations", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       


    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");   
    }

        function add_affectation() {

                

         // id of inserted vehicle
         var id_vehicle = $('#id_selected_vehicle').val();            
                    
        // Get values from inputs
            
            
            var etat = $('#etat_vehicle').val();
            var type_affectation = $('#type_affectation').val();
            var numero_decision = $('#numero_decision').val();
            // var kilometrage = $('#kilometrage').val();
            var id_structure = $('#structure_affectation').selectpicker('val');
            var id_priorietaire = $('#detail_affectation').selectpicker('val');

            if (etat != "" &&  type_affectation != "" && numero_decision != "" 
               && id_structure != "" && id_priorietaire != "") {

 


                                    // insert data to database
                                    //ex : '{"key":"val","key1":"val1"}'
                                    var data2db = JSON.parse(
                                        '{"etat":"' +etat+ 
                                        '","type_affectation":"' +type_affectation+ 
                                        '","numero_decision":"' +numero_decision+ 
                                        // '","kilometrage":"' +kilometrage+ 
                                        '","id_structure":"' +id_structure+ 
                                        '","id_priorietaire":"' +id_priorietaire+ 
                                        '","id_vehicle":"' +id_vehicle+ '"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "add_affectation";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'data inserted') {
                                        $('#id_inserted_affectation').val(reply['id_inserted_affectation']);
                                        $('#decision_form').trigger('submit');
                                             swal({
                                                  title: "Succès!",
                                                  text: "Véhicule affecté avec succès",
                                                  type: "success",
                                                  confirmButtonColor: "#DD6B55",
                                                  confirmButtonText: "Terminer",
                                                  closeOnConfirm: true,
                                                
                                                });

                                             
                                             $('#affectation_collapse').removeClass('in');
                                             $('#affectation_collapse').attr('aria-expanded',false);
                                             $('#affectation_panel').hide();
                                             disable_elements('affectation');
                                             $('#decision_document').show();
                                             $('#decision_form').hide();
                                             $('#detail_affectation__').hide();
                                             $('#detail_affectation_').show();
                                        
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Errur lors l'affectation du véhicule", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
                        
                       

    }
    else swal("Erreur!","Veuillez remplir tous les champs", "error");
    } 



            function print_me() {
               $("#print-iframe").get(0).contentWindow.print();
               // $('#myModal').trigger('data-dismiss');
            }

            function myFunction() {
                 
$('#modal_history').modal('show');
               

  // var iframe = document.getElementById("print-iframe");
  // var elmnt = iframe.contentWindow.document.getElementById("designation"); 

  //  var iframe = $('#print-iframe').contents();
  // var elmnt = iframe.find("#designation");
  // elmnt.text('ohhhh')

  // alert(elmnt.text());
}


        </script>


    <script>
    $(document).ready(function() {
     
            var table = $('#example23').DataTable();
 
    $('#example23 tbody').on( 'click', 'tr', function () {
        
        if ( $(this).hasClass('selected') ) {
            $(this).removeClass('selected');
            $("input:radio").attr('checked', false);
            $('#assurance_panel').hide();
            $('#status_panel').hide();
            $('#affectation_panel').hide(); 
            $('#maintenance_panel').hide(); 
            $('#driver_panel').hide(); 
            $('#id_selected_vehicle').val('');
             
            
        }
        else {


            table.$('tr.selected').removeClass('selected');
            $(this).addClass('selected');
            $('#vehicles_list').addClass('slideOutLeft'); 
            $('#vehicles_list').removeClass('slideInLeft'); 
            $('#vehicles_list').attr('style','position:absolute'); 
             

             
             


            $('#vehicle_details').removeClass('slideOutRight');
            $('#vehicle_details').show(); 
            $('#vehicle_details').addClass('slideInRight');
            $('#vehicle_details').attr('style','position:all'); 


          


            $('#assurance_panel').removeClass('slideOutRight');
            $('#assurance_panel').show(); 
            $('#assurance_panel').addClass('slideInRight');  
            $('#assurance_panel').attr('style','position:all'); 

            
            $('#affectation_panel').removeClass('slideOutRight');
            $('#affectation_panel').show(); 
            $('#affectation_panel').addClass('slideInRight');
            $('#affectation_panel').attr('style','position:all');   


            $('#status_panel').removeClass('slideOutRight');
            $('#status_panel').show(); 
            $('#status_panel').addClass('slideInRight');
            $('#status_panel').attr('style','position:all');

            $('#driver_panel').removeClass('slideOutRight');
            $('#driver_panel').show(); 
            $('#driver_panel').addClass('slideInRight');
            $('#driver_panel').attr('style','position:all'); 

            $('#maintenance_panel').removeClass('slideOutRight');
            $('#maintenance_panel').show(); 
            $('#maintenance_panel').addClass('slideInRight');
            $('#maintenance_panel').attr('style','position:all'); 

            $('#carte_panel').removeClass('slideOutRight');
            $('#carte_panel').show(); 
            $('#carte_panel').addClass('slideInRight');
            $('#carte_panel').attr('style','position:all'); 




           
            $('#id_selected_vehicle').val('');
            $('#id_selected_vehicle').val($(this).attr('id').split('_')[1]);
            get_vehicle_info($('#id_selected_vehicle').val());
          //  alert($('#id_selected_vehicle').val());
       setTimeout(function() {
          $('#vehicles_list').hide();
       }, 800);
            $("#radio_" +$(this).attr('id').split('_')[1]).attr('checked',true);
            
        }
    } );

 $(document).on('hide.bs.modal','#modal_history', function () {
               // $('#history_table thead').html('');
              //  $('#history_table');
 //Do stuff here

});

        function get_vehicle_info(vehicle_id) {
            var data2db = JSON.parse(
                                        '{"id_vehicle":"' +vehicle_id+'"}'
                                        ); 
                                    console.log(data2db);
                                       var url = "get_vehicle_info";
                                      $.post(url,data2db).done(function( data ) {
                                        var reply = JSON.parse(data);
                                        console.log(reply);
                                        if (reply['message'] == 'vehicle exists') {

                                        console.log(reply['all_info']['vehicle']['numero_immatriculation']);
if (reply['all_info']['affectation'] != null) {

                    $('#etat_vehicle').selectpicker('val',reply['all_info']['affectation']['etat']);
                     $('#designation').selectpicker('val',reply['all_info']['affectation']['type_structure']).change();  
                     $('.selectpicker').selectpicker('refresh');
                    //$('#structure_affectation').val(reply['all_info']['affectation']['structure_affectation']);
                    $('#structure_affectation').selectpicker('val',reply['all_info']['affectation']['id_structure_affectation']);  

      $('#detail_affectation_').attr(reply['all_info']['affectation']['id_priorietaire']);
                    
      $('#detail_affectation_').val(reply['all_info']['affectation']['detail_affectation']);
        $('#code_wilaya').val(reply['all_info']['affectation']['code_wilaya']);
                    $('#type_affectation').selectpicker('val',reply['all_info']['affectation']['type_affectation']);
                    $('#numero_decision').val(reply['all_info']['affectation']['numero_decision']);
                    if (reply['all_info']['affectation']['document_decision'] != null) {
$('#decision_document').attr('href',"<?php echo base_url(); ?>"+reply['all_info']['affectation']['document_decision']);  } else {
    $('#decision_document').removeAttr('href');
}
}


    // vehicle assurance
            if (reply['all_info']['assurance'] != null) {
                console.log(reply['all_info']['assurance']['nom']);
            $('#compagnie_assurance').selectpicker('val',reply['all_info']['assurance']['id']).change();
           
            $('#compagnie_assurance').attr('adresse',reply['all_info']['assurance']['adresse']);
            $('#compagnie_assurance').attr('telephone',reply['all_info']['assurance']['telephone']);
            $('#compagnie_assurance').attr('numero_police',reply['all_info']['assurance']['numero_police']);
            $('#date_debut').val(reply['all_info']['assurance']['date_debut']);
            $('#date_fin').val(reply['all_info']['assurance']['date_fin']);
            
                                                                }

if (reply['all_info']['vehicle'] != null) {
            $('#id_vehicle').val(reply['all_info']['vehicle']['ID']);
            $('#marque').val(reply['all_info']['vehicle']['marque']);
            $('#modele').val(reply['all_info']['vehicle']['modele']);
            $('#motorisation').selectpicker('val',reply['all_info']['vehicle']['motorisation']);
            $('#numero_immatriculation').val(reply['all_info']['vehicle']['numero_immatriculation']);
            $('#gps_tracker').selectpicker('val',reply['all_info']['vehicle']['gps_tracker']);
    $('#numero_immatriculation_old').val(reply['all_info']['vehicle']['numero_immatriculation_old']);
            $('#etat').selectpicker('val',reply['all_info']['vehicle']['etat']);
            $('#type_vehicle').selectpicker('val',reply['all_info']['vehicle']['type_vehicle']);
            $('#numero_chassis').val(reply['all_info']['vehicle']['numero_chassis']);
}

            if (reply['all_info']['exterior_status'] != null) {
            var exterior_parts  =  <?php echo json_encode($exterior_parts); ?>;
            var exterior_part_status = [];
               
   for (var i = 0; i < exterior_parts.length; i++) {

              (function(i) {

                // using javascript closure muhahaha :
        if (reply['all_info']['exterior_status'][i] != null ) {
         if (exterior_parts[i]['id'] == reply['all_info']['exterior_status'][i]['id_part'] ) {
                          $('#exterior_part_status_'+exterior_parts[i]['id']).val(reply['all_info']['exterior_status'][i]['status']);  
                        }
                        }
                      })(i);
                    }
                   } 

            if (reply['all_info']['interior_status'] != null) {
            var interior_parts  =  <?php echo json_encode($interior_parts); ?>;
            var interior_part_status = [];
               
   for (var i = 0; i < interior_parts.length; i++) {

              (function(i) {

                // using javascript closure muhahaha :
          if (reply['all_info']['interior_status'][i] != null ) {
          if (interior_parts[i]['id'] == reply['all_info']['interior_status'][i]['id_part'] ) {
                          $('#interior_part_status_'+interior_parts[i]['id']).val(reply['all_info']['interior_status'][i]['status']);  

                        }
                        }

                      })(i);
                    }
                   } 

            if (reply['all_info']['carte_naftal'] != null) {
            $('#numero_carte').val(reply['all_info']['carte_naftal']['numero']);
            $('#debit_carte').val(reply['all_info']['carte_naftal']['debit']);
            $('#credit_carte').val(reply['all_info']['carte_naftal']['credit']);
            $('#expiration_carte').val(reply['all_info']['carte_naftal']['date_expiration']);

                                                                } 
           // vehicle driver info
            if (reply['all_info']['drivers'] != null) {

            $('#chauffeur_').selectpicker('val',reply['all_info']['drivers']['id']).change();
            $('#type_chauffeur').selectpicker('val',reply['all_info']['drivers']['type_chauffeur']);
           
            $('#chauffeur_').attr('type_permis',reply['all_info']['drivers']['type_permis']);
            $('#chauffeur_').attr('validite_permis',reply['all_info']['drivers']['validite_permis']);
                                                                }
           
                      
                                   

                                    
                                        }else if (reply == 'error') {
                                          swal("Erreur...","Erreur véhicule non trouvé", "error");
                                        }
                                        // swal("ID CAS", mObj, "success");
                                      }).fail(function(data, textStatus, errorThrown){
                                        swal("Erreur...","", "error");
                                      });
        }

        $("#demo01").animatedModal({
        animatedIn:'zoomIn',
        animatedOut:'bounceOut',
        color:'#FFFFFF',
        height: 860,
        width: 970,
        beforeOpen: function() {

            var children = $(".thumb");
            var index = 0;

            function addClassNextChild() {
                if (index == children.length) return;
                children.eq(index++).show().velocity("transition.expandIn", { opacity:1, stagger: 250 });
                window.setTimeout(addClassNextChild, 200);
            }

            addClassNextChild();

        },
        afterClose: function() {
          $(".thumb").hide();
              
        }
    });

        $('#myTable').DataTable();
        $(document).ready(function() {
            var table = $('#example').DataTable({
                "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],
                "order": [
                    [2, 'asc']
                ],
                "displayLength": 25,
                "drawCallback": function(settings) {
                    var api = this.api();
                    var rows = api.rows({
                        page: 'current'
                    }).nodes();
                    var last = null;

                    api.column(2, {
                        page: 'current'
                    }).data().each(function(group, i) {
                        if (last !== group) {
                            $(rows).eq(i).before(
                                '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                            );

                            last = group;
                        }
                    });
                }
            });

            // Order by the grouping
            $('#example tbody').on('click', 'tr.group', function() {
                var currentOrder = table.order()[0];
                if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                    table.order([2, 'desc']).draw();
                } else {
                    table.order([2, 'asc']).draw();
                }
            });
        });
    });
    //$('#example23').footable();
    $('#example23').DataTable({
        "language": {
            "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
            "zeroRecords": "Aucun résultat trouvé",
            "info": "Affichage du la page _PAGE_ de _PAGES_",
            "infoEmpty": "Table vide!",
            "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
            "sSearch" : "Rechercher",
              "oPaginate": {
            "sFirst":    "Premier",
            "sLast":    "Dernier",
            "sNext":    "Suivant",
            "sPrevious": "Précédent"
        }
        } ,
        "columnDefs": [
          { "orderable": false, "targets": [0]},
          { "searchable": false, "targets": [0]},
          { "printable": false, "targets": [0]},
          { "width": "10px", "targets": [0] },
          { "width": "20px", "targets": [6] },
          { "width": "100%", "targets": [5] },
          { "width": "5%", "targets": [3] }
        ],

        
    }); 
       $('#example24').DataTable({
        "language": {
            "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
            "zeroRecords": "Aucun résultat trouvé",
            "info": "Affichage du la page _PAGE_ de _PAGES_",
            "infoEmpty": "Table vide!",
            "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
            "sSearch" : "Rechercher",
              "oPaginate": {
            "sFirst":    "Premier",
            "sLast":    "Dernier",
            "sNext":    "Suivant",
            "sPrevious": "Précédent"
        }
        } ,


    }); 
    
          $('#example25').DataTable({
        "language": {
            "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
            "zeroRecords": "Aucun résultat trouvé",
            "info": "Affichage du la page _PAGE_ de _PAGES_",
            "infoEmpty": "Table vide!",
            "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
            "sSearch" : "Rechercher",
              "oPaginate": {
            "sFirst":    "Premier",
            "sLast":    "Dernier",
            "sNext":    "Suivant",
            "sPrevious": "Précédent"
        }
        } ,

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });       $('#example26').DataTable({
        "language": {
            "lengthMenu": "Affichage _MENU_ d'enregistrement par page",
            "zeroRecords": "Aucun résultat trouvé",
            "info": "Affichage du la page _PAGE_ de _PAGES_",
            "infoEmpty": "Table vide!",
            "infoFiltered": "(filtrage depuis _MAX_ enregistrements.)",
            "sSearch" : "Rechercher",
              "oPaginate": {
            "sFirst":    "Premier",
            "sLast":    "Dernier",
            "sNext":    "Suivant",
            "sPrevious": "Précédent"
        }
        } ,

        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });



    </script>
    <script>


    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'

    });

    $('.clockpicker').clockpicker({
        donetext: 'OK',

    })
    .find('input').change(function() {
        console.log(this.value);
    });

    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
        .clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }

    // Date Picker
    jQuery('.mydatepicker, #datepicker, #expiration_carte').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy',

    });
    jQuery('#datepicker-autoclose, #accident_date, #date_entretien').datepicker({
        autoclose: true,
        todayHighlight: true,
        format: 'dd/mm/yyyy'
    });

    jQuery('#date-range').datepicker({
        toggleActive: true,
        format: 'dd/mm/yyyy'
    });
    jQuery('.myrange').datepicker({
        toggleActive: true,
        text : 'au'
    });
    jQuery('#datepicker-inline').datepicker({

        todayHighlight: true
    });

    // Daterange picker

    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
    $(document).ready(function() {
    
    // enable fileuploader plugin
    $('#filer_input').filer({
    showThumbs: true,
    addMore: false,
    allowDuplicates: false,
    limit:1,
    extensions: ["pdf","png","jpg","bmp","jpeg","tiff"],
    captions: {
      button: "Parcourir",
      feedback: "Sélectionner la décision scannée..",
      feedback2: "fichier a été choisi",
      drop: "Drop file here to Upload",
      size: "taille",
      removeConfirmation: "Voulez-vous vraiment supprimer ce fichier?",
      errors: {
        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
        filesType: "Veuillez sélectionner uniquement des fichiers imgaes ou pdf.",
        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
        folderUpload: "Vous n'êtes pas autorisé à sélectionner des dossiers."
      }
    }
  }); 
     // enable fileuploader plugin
    $('#filer_input2').filer({
    showThumbs: true,
    addMore: false,
    allowDuplicates: false,
    limit:1,
    extensions: ["pdf","png","jpg","bmp","jpeg","tiff"],
    captions: {
      button: "Parcourir",
      feedback: "Sélectionner constat d'accident..",
      feedback2: "fichier a été choisi",
      drop: "Drop file here to Upload",
      size: "taille",
      removeConfirmation: "Voulez-vous vraiment supprimer ce fichier?",
      errors: {
        filesLimit: "Only {{fi-limit}} files are allowed to be uploaded.",
        filesType: "Veuillez sélectionner uniquement des fichiers imgaes ou pdf.",
        filesSize: "{{fi-name}} is too large! Please upload file up to {{fi-fileMaxSize}} MB.",
        filesSizeAll: "Files you've choosed are too large! Please upload files up to {{fi-maxSize}} MB.",
        folderUpload: "Vous n'êtes pas autorisé à sélectionner des dossiers."
      }
    }
  });
     
 
    
});
    jQuery(document).ready(function() {
    
        // For mask

        $('.immatriculation').inputmask('99999-999-99');
        $('.kilometrage').inputmask({ regex: "[0-9]*" });
        $('.naftal').inputmask('9999-9999-9999-9999');
        $('.credit').inputmask({regex : '[0-9]* DA'});
        $('.debit').inputmask({regex : '[0-9]* DA'});
        $('.numero').inputmask({regex : '[0-9]*'});
        $('.cout').inputmask({regex : ' [0-9]*[,][0-9]{2} DA'});
        // $('.cout').inputmask({ 'alias': 'decimal', 'groupSeparator': ',', 'autoGroup': true, 'digits': 2, 'digitsOptional': false, })
        
        // Switchery
        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
            new Switchery($(this)[0], $(this).data());

        });
        // For select 2


        $(".select2").select2();

        $('.selectpicker').selectpicker();

        //Bootstrap-TouchSpin
        $(".vertical-spin").TouchSpin({
            verticalbuttons: true,
            verticalupclass: 'ti-plus',
            verticaldownclass: 'ti-minus'
        });
        var vspinTrue = $(".vertical-spin").TouchSpin({
            verticalbuttons: true
        });
        if (vspinTrue) {
            $('.vertical-spin').prev('.bootstrap-touchspin-prefix').remove();
        }

        $("input[name='tch1']").TouchSpin({
            min: 0,
            max: 100,
            step: 0.1,
            decimals: 2,
            boostat: 5,
            maxboostedstep: 10,
            postfix: '%'
        });
        $("input[name='tch2']").TouchSpin({
            min: -1000000000,
            max: 1000000000,
            stepinterval: 50,
            maxboostedstep: 10000000,
            prefix: '$'
        });
        $("input[name='tch3']").TouchSpin();

        $("input[name='tch3_22']").TouchSpin({
            initval: 40
        });

        $("input[name='tch5']").TouchSpin({
            prefix: "pre",
            postfix: "post"
        });

        // For multiselect

        $('#pre-selected-options').multiSelect();
        $('#optgroup').multiSelect({
            selectableOptgroup: true
        });

        $('#public-methods').multiSelect();
        $('#select-all').click(function() {
            $('#public-methods').multiSelect('select_all');
            return false;
        });
        $('#deselect-all').click(function() {
            $('#public-methods').multiSelect('deselect_all');
            return false;
        });
        $('#refresh').on('click', function() {
            $('#public-methods').multiSelect('refresh');
            return false;
        });
        $('#add-option').on('click', function() {
            $('#public-methods').multiSelect('addOption', {
                value: 42,
                text: 'test 42',
                index: 0
            });
            return false;
        });


     // Add fleet function
     


 });
</script> 
<script>
    // Clock pickers
    $('#single-input').clockpicker({
        placement: 'bottom',
        align: 'left',
        autoclose: true,
        'default': 'now'

    });

    $('.clockpicker').clockpicker({
            donetext: 'Ok',

        })
        .find('input').change(function() {
            console.log(this.value);
        });

    $('#check-minutes').click(function(e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
            .clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
    // Colorpicker

    $(".colorpicker").asColorPicker();
    $(".complex-colorpicker").asColorPicker({
        mode: 'complex'
    });
    $(".gradient-colorpicker").asColorPicker({
        mode: 'gradient'
    });
    // Date Picker
    jQuery('.mydatepicker, #datepicker').datepicker();
    jQuery('#datepicker-autoclose, #accident_date, #date_entretien').datepicker({
        autoclose: true,
        todayHighlight: true
    });

    jQuery('#date-range').datepicker({
        toggleActive: true
    });
    jQuery('#datepicker-inline').datepicker({

        todayHighlight: true
    });

    // Daterange picker

    $('.input-daterange-datepicker').daterangepicker({
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-daterange-timepicker').daterangepicker({
        timePicker: true,
        format: 'MM/DD/YYYY h:mm A',
        timePickerIncrement: 30,
        timePicker12Hour: true,
        timePickerSeconds: false,
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse'
    });
    $('.input-limit-datepicker').daterangepicker({
        format: 'MM/DD/YYYY',
        minDate: '06/01/2015',
        maxDate: '06/30/2015',
        buttonClasses: ['btn', 'btn-sm'],
        applyClass: 'btn-danger',
        cancelClass: 'btn-inverse',
        dateLimit: {
            days: 6
        }
    });
    </script>

    <script>
    function testAnim(x) {
      $('#animationSandbox').removeClass().addClass(x + ' animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
      });
    };

    $(document).ready(function(){
      $('.js--triggerAnimation').click(function(e){
        e.preventDefault();
        var anim = $('.js--animations').val();
        testAnim(anim);
      });

      $('.js--animations').change(function(){
        var anim = $(this).val();
        testAnim(anim);
      });
    });

</script>

    <!--Style Switcher -->
    <script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
   
</body>

</html>
