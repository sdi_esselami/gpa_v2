                    <div class="col-sm-12 animated" style=" display: none;" id="vehicle_details">
    <div class="white-box" style="border-color: #FFE7C2;border-style: dashed; >
        <h3 class="box-title m-b-0"><b>RENSEIGNEMENTS DU VEHICULE</b></h3>
          <div class="pull-right">
                        <a class="text-info" href="javascript;" onclick="back_to_list()" data-perform="panel-collapse">
                            <i class="ti-arrow-left" style="font-size: 11px"></i>&nbsp;Retour vers la liste </a>
                        </div>




        <!-- <form data-toggle="validator" > -->
    <hr>
        <div class="row ">
            <div class="col-sm-4 form-group">
             <h5 class="m-t-30 m-b-10"><b>MARQUE</b></h5> 
             <div id="marques">
              <input class="typeahead form-control information" type="text" placeholder="HYUNDAI, BMW,..." id="marque" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>
          </div>
      </div>
      <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>MODELE</b></h5>
        <input class="form-control information" type="text" placeholder="ELANTRA, ACCENT,..." id="modele" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>   </div>
    <div class="col-md-4">
        <h5 class="m-t-30 m-b-10"><b>MOTORISATION</b></h5>
        <select class="selectpicker show-tick information" data-live-search="false" title="Sélectionner.." data-style="form-control" id="motorisation" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div> 
            <option>DIESEL</option>
            <option>ESSENCE</option>
            <option>GPL</option>
            <option>ESSENCE / GPL</option>
        </select>
    </div>
    <div class="col-md-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>TYPE</b></h5>
        <select class="selectpicker show-tick information" data-live-search="false" title="Sélectionner.." data-style="form-control" id="type_vehicle" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>           <div class=""></div>
            <option>AMBULANCE</option>
            <option>ENGIN</option>
            <option>CAMION</option>
            <option>VEHICULE TOURISTIQUE</option>
            <option>VEHICULE UTILITAIRE</option>
            <option>CAMION</option>
        </select>
    </div>

    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>VEHICULE TRACKER PAR GPS</b></h5>
        <select class="selectpicker show-tick information" title="Sélectionner.." data-style="form-control" id="gps_tracker" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>           <div class=""></div>
            <option>OUI</option>
            <option>NON</option>

        </select>
    </div>
    <div class="col-sm-4 form-group">
        <h5 class="m-t-30 m-b-10"><b>ETAT GENERAL DU VEHICULE</b></h5>
        <select class="selectpicker show-tick information" title="Sélectionner.." data-style="form-control" id="etat" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>           
            <option class="text-success">NEUF</option>
            <option style="color :#D2FF00 ">BON ETAT</option>
            <option class="text-warning">ETAT MOYEN</option>
            <option class="text-danger">MAUVAIS ETAT</option>
        </select>
    </div>

                     <!--            <div class="col-md-4">
                                    <h5 class="m-t-30 m-b-10"><b>KILOMETRAGE</b></h5>
                                    <input class="form-control kilometrage" id="kilometrage" >
                                </div>  -->   
                            </div>
                            <div class="row ">
                                <div class="col-sm-4 form-group">
                                 <h5 class="m-t-30 m-b-10"><b>N° CHASSIS</b></h5> 
                                 
                                 <input class=" form-control information" type="text"  maxlength="17" id="numero_chassis" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                             </div>
                             <div class="col-sm-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION</b></h5>
                                <input class="form-control immatriculation information"  placeholder="_____-___-__"
                                id="numero_immatriculation" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                           </div>
                            <div class="col-md-4 form-group">
                                <h5 class="m-t-30 m-b-10"><b>N° IMMATRICULATION (Ancienne/Provisoire)</b></h5>
                                <input class="form-control immatriculation information"  placeholder="_____-___-__"
                                id="numero_immatriculation_old" data-error="Veuillez renseigner ce champ." required disabled="true">
              <div class="help-block with-errors"></div>                           </div>


                        </div>
                                                <hr>
                        <div class="row "> 
                         <div class=" col-sm-12 ">
                            <div class="text-right">

                                    <button class="fcbtn btn btn-danger btn-outline btn-1c " 
                                style="width:20%" id="cancel_inssurance_btn" onclick="enable_elements('information');">Modifer</button>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                                <button class=" information fcbtn btn btn-success btn-outline btn-1c " 
                                style="width:20%" id="update_vehicle_btn" onclick="update_vehicle()" disabled="true">Sauvgarder</button>
                            </div> 
                        </div>
                    </div>   
  </div>
  </div>   