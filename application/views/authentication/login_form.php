<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
<title>CNAS | Gestion PARC AUTO</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
<!-- animation CSS -->
<link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="<?php echo base_url(); ?>plugins/css/style.css" rel="stylesheet">
<!-- color CSS -->
<link href="<?php echo base_url(); ?>plugins/css/colors/gray-dark.css" id="theme"  rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->

</head>
<body>
<!-- Preloader -->
<div class="preloader">
  <div class="cssload-speeding-wheel"></div>
</div>
<section id="wrapper" class="login-register">
  <div class="login-box">
    <div class="white-box">
      <div class="col-xs-12">
        <h3 class="box-title m-b-20 text-center"><b>- CONNEXION - </b></h3>
            
                <?php if (isset($error_message)) {
                  echo '<div class="alert alert-danger">';
                  echo $error_message;
                  echo '</div>';
                } ?>
             
                 <form id="loginform" data-toggle="validator" action="login" method='post'>
                                <div class="form-group">
                                    <label for="username" class="control-label">Nom d'utilisateur</label>
                                    <input type="text" class="form-control" name="username" placeholder="Nom d'utilisateur..." data-error="Veuillez renseigner ce champ." required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group">
                                    <label for="password" class="control-label">Mot  de passe</label>
                                    <input type="password" class="form-control" name="password" placeholder="Mot de passe..." data-error="Veuillez renseigner ce champ." required>
                                    <div class="help-block with-errors"></div>
                                </div>
                                <div class="form-group ">
                                    <button class="btn btn-block btn-outline btn-info" type="submit">CONNECTER</button>
                                </div>
                            </form>
      </div>

    </div>
  </div>
</section>
<!-- jQuery -->
<script src="<?php echo base_url(); ?>plugins/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap Core JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/tether.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
<!-- Menu Plugin JavaScript -->
<script src="<?php echo base_url(); ?>plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>

<!--slimscroll JavaScript -->
<script src="<?php echo base_url(); ?>plugins/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="<?php echo base_url(); ?>plugins/js/waves.js"></script>
<!-- Custom Theme JavaScript -->
<script src="<?php echo base_url(); ?>plugins/js/custom.min.js"></script>
<script src="<?php echo base_url(); ?>plugins/js/validator.js"></script>
<!--Style Switcher -->
<script src="<?php echo base_url(); ?>plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
</body>
</html>
