<?php
defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>  
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="icon" type="image/png" sizes="16x16" href="<?php echo base_url(); ?>plugins/images/favicon.png">
<title>CNAS | Gestion PARC AUTO</title>
<!-- Bootstrap Core CSS -->
<link href="<?php echo base_url(); ?>plugins/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
<link href="<?php echo base_url(); ?>plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
<!-- animation CSS -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/css/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/css/fonts/iconic/css/material-design-iconic-font.min.css">

<!--===============================================================================================-->

<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->

<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/vendor/select2/select2.min.css">
<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->


<link href="<?php echo base_url(); ?>plugins/css/animate.css" rel="stylesheet">
<!-- Custom CSS -->

<!-- color CSS -->

<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/css/login/util.css">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>plugins/css/login/main.css">
<style>
.hero {
	
	-webkit-animation-name: color-transition;
	animation-name: color-transition;
	-webkit-animation-duration: 20s;
	animation-duration: 20s;
	-webkit-animation-direction: alternate;
	animation-direction: alternate;
	-webkit-animation-iteration-count: infinite;
	animation-iteration-count: infinite;
	-webkit-animation-timing-function: linear;
	animation-timing-function: linear;
}


/*This is the animation used to change colors; In this instance, I made sure to have the same color at 0% as I did at 100% */

@-webkit-keyframes color-transition {
	0% {
		color: #4C6085;
		
	}
	33% {
		color: #80D39B;
		
	}
	66% {
		color: #BE3E82;
		
	}
	100% {
		color: #4C6085;
		
	}
}

@keyframes color-transition {
	0% {
		color: #4C6085;
		
	}
	33% {
		color: #80D39B;
		
	}
	66% {
		color: #BE3E82;
		
	}
	100% {
		color: #4C6085;
		
	}
}
</style>
</head>
<!--===============================================================================================-->


<body>

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100" >
				<form class="login100-form validate-form" id="loginform" action="login" method='post' >
					<span class="login100-form-title p-b-26 hero animated bounce" style="font-size:29px">
					GESTION <br> PARC AUTOMOBILE
					</span>
					<span class="login100-form-title p-b-48 animated fadeInDown">
					<img src="<?php echo base_url(); ?>plugins/images/cnas.png" width="150" height="100" alt=""/>
					</span>
					<?php if (isset($error_message)) {
                  echo '<div class="alert alert-danger">';
                  echo $error_message;
                  echo '</div>';
                } ?>
					<div style="width:300px" class="wrap-input100 validate-input " data-validate = "Champ obligatoire">
						<input class="input100" type="text" name="username" >
						<span class="focus-input100" data-placeholder="Nom d'utilisateur"></span>
					</div>

					<div style="width:300px" class="wrap-input100 validate-input" data-validate="Champ obligatoire">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" name="password">
						<span class="focus-input100" data-placeholder="Mot de passe"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="submit">
								Connexion
							</button>
							
						</div>
					</div>

					<div class="text-center p-t-100" >
						<span class="footer text-center text-info" style="font-size:15px">
						CNAS 2018 &copy; Direction d'informatique
						<br>
						version 1.0
						</span>

						<a class="txt2" href="#">
							
						</a>
					</div>
				</form>
			</div>
			
		</div>
		<footer class="footer text-center text-info"> </footer>
	</div>
	
	
	<div id="dropDownSelect1"></div>
	
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url(); ?>plugins/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/vendor/daterangepicker/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>plugins/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url(); ?>plugins/js/login/main.js"></script>

</body>
</html>