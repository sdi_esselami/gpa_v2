<!DOCTYPE html>
<html>
<head>

	<title> Détails du véhicule</title>
	<link href="<?php echo base_url(); ?>plugins/css/font.css" rel="stylesheet">
	<style type="text/css">
		body {
  background: rgb(255,255,255); 
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="portrait"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="portrait"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="portrait"] {
  width: 21cm;
  height: 14.8cm;  
}
@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
 
}
 h5 {
 	font-family: Ubuntu;
	font-size: 14px;
	font-style: normal;
	font-variant: normal;
	font-weight: 500;
	/*line-height: 15.4px;*/
	
}
	</style>
</head>
<body>
	<h5 id="designation" style=" position: absolute; margin-top: 253px;margin-left:140px"> AGENCE</h5>
	<h5 id="nom" style=" position: absolute; margin-top: 280px;margin-left:90px"> AGENCE TIARET</h5>
	<h5 id="code_wilaya" style=" position: absolute; margin-top: 305px;margin-left:140px"> W14</h5>
	<h5 id="compagnie_assurance" style="  position: absolute; margin-top: 379px;margin-left:212px"> CAAR</h5>
	<h5 id="numero_police" style="  position: absolute; margin-top: 405px;margin-left:152px"> 09876</h5>
	<h5 id="date_debut" style="  position: absolute; margin-top: 430px;margin-left:130px"> 01/01/2018</h5>
	<h5 id="date_fin" style="  position: absolute; margin-top: 454px;margin-left:107px"> 01/06/2018</h5>
	<h5 id="nom_chauffeur" style="  position: absolute; margin-top: 524px;margin-left:162px"> BELKHIR</h5>
	<h5 id="type_chauffeur" style="  position: absolute; margin-top: 550px;margin-left:174px"> PERMANANT</h5>
	<h5 id="type_permis" style="  position: absolute; margin-top: 575px;margin-left:157px"> B</h5>
	<h5 id="validite_permis" style="  position: absolute; margin-top: 599px;margin-left:263px"> 26/11/2018</h5>
	
	<h5 id="marque" style="  position: absolute; margin-top: 252px;margin-left:600px"> HYUNDAI</h5>
	<h5 id="modele" style="  position: absolute; margin-top: 278px;margin-left:591px"> ELANTRA</h5>
	<h5 id="motorisation" style="  position: absolute; margin-top: 303px;margin-left:638px"> ESSENCE</h5>
	<h5 id="numero_chassis" style="  position: absolute; margin-top: 326px;margin-left:616px"> </h5>
	<h5 id="numero_immatriculation" style="  position: absolute; margin-top: 348px;margin-left:678px"> 02929-117-14</h5>
	<h5 id="numero_immatriculation_ancien" style="  position: absolute; margin-top: 375px;margin-left:728px"> 00197-00-14</h5>
	<h5 id="gps_tracker" style="  position: absolute; margin-top: 400px;margin-left:658px"> OUI</h5>
	<h5 id="kilometrage" style="  position: absolute; margin-top: 427px;margin-left:633px"> 8098</h5>
	<h5 id="etat_general" style="  position: absolute; margin-top: 455px;margin-left:633px"> NEUF</h5>

	<textarea id="autres_informations" class="form-control" rows="3" style="  position: absolute; margin-top: 539px;margin-left:500px;
	font-family: ubuntu;font-size: 14px;font-style: normal;border: none;resize: none;width: 410px;height: 58px;
	text-align: center; white-space: normal;" maxlength="160" readonly="true"></textarea>	
	<img src="<?php echo base_url(); ?>plugins/images/details_vehicule.jpg">
	


	
</body>
</html>