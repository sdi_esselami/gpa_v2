-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  lun. 07 mai 2018 à 15:57
-- Version du serveur :  10.1.30-MariaDB
-- Version de PHP :  5.6.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `fleetco_db`
--

-- --------------------------------------------------------

--
-- Structure de la table `accidents`
--

CREATE TABLE `accidents` (
  `Id` int(11) NOT NULL,
  `SysDate` varchar(50) DEFAULT NULL,
  `Date` varchar(50) DEFAULT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `Vehicle` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Details` varchar(50) DEFAULT NULL,
  `Driver` varchar(50) DEFAULT NULL,
  `Injured` varchar(50) DEFAULT NULL,
  `Images` varchar(350) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `DamageToVehicle` varchar(50) DEFAULT NULL,
  `3rdPartyDamages` varchar(50) DEFAULT NULL,
  `Time` varchar(50) DEFAULT NULL,
  `Deaths` varchar(50) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `StatusInjured` varchar(50) DEFAULT NULL,
  `Category` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `carrier_uggroups`
--

CREATE TABLE `carrier_uggroups` (
  `GroupID` int(11) NOT NULL,
  `Label` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `carrier_uggroups`
--

INSERT INTO `carrier_uggroups` (`GroupID`, `Label`) VALUES
(1, 'manager'),
(2, 'user'),
(3, 'viewer');

-- --------------------------------------------------------

--
-- Structure de la table `carrier_ugmembers`
--

CREATE TABLE `carrier_ugmembers` (
  `UserName` varchar(300) NOT NULL,
  `GroupID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `carrier_ugmembers`
--

INSERT INTO `carrier_ugmembers` (`UserName`, `GroupID`) VALUES
('Admin', -1),
('Admin', 1),
('Admin', 2),
('Admin', 3),
('Manager', 1),
('Manager', 2),
('Manager', 3),
('User', 2),
('Vishan', -1),
('Vishan', 1),
('Vishan', 2),
('Vishan', 3);

-- --------------------------------------------------------

--
-- Structure de la table `carrier_ugrights`
--

CREATE TABLE `carrier_ugrights` (
  `TableName` varchar(300) NOT NULL,
  `GroupID` int(11) NOT NULL,
  `AccessMask` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `carrier_ugrights`
--

INSERT INTO `carrier_ugrights` (`TableName`, `GroupID`, `AccessMask`) VALUES
('accidents', -1, 'AEDSPI'),
('accidents', 1, 'AEDSP'),
('accidents', 2, 'AESP'),
('accidents', 3, 'SP'),
('accidents Chart', -1, 'S'),
('accidents-report', -1, 'AEDSPI'),
('accidents-report', 1, 'SP'),
('accidents-report', 2, 'SP'),
('accidents-report', 3, 'SP'),
('admin_members', -1, 'ADESPIM'),
('admin_rights', -1, 'ADESPIM'),
('admin_users', -1, 'ADESPIM'),
('availability', -1, 'ASPI'),
('carrierusers', -1, 'ADESPIM'),
('creategrn', -2, 'ASP'),
('creategrn', -1, 'ADESPIM'),
('creategrn', 1, 'AEDSP'),
('creategrn', 2, 'ASP'),
('creategrn', 3, 'SP'),
('creategrn-addnew', -1, 'AEDSPI'),
('creategrn-disposal', -1, 'AEDSPI'),
('creategrn-disposal', 1, 'AEDSP'),
('creategrn-disposal', 2, 'AESP'),
('creategrn-disposal', 3, 'SP'),
('creategrn-disposal-rebuild', -1, 'AEDSPI'),
('creategrn-disposal-rebuild', 1, 'AEDSP'),
('creategrn-disposal-rebuild', 2, 'AESP'),
('creategrn-disposal-rebuild', 3, 'SP'),
('creategrn-issue', -1, 'AEDSPI'),
('creategrn-issue', 1, 'AEDSP'),
('creategrn-issue', 2, 'AESP'),
('creategrn-issue', 3, 'SP'),
('creategrn-issue-price', -1, 'AEDSPI'),
('creategrn-issue-price', 1, 'AEDSP'),
('creategrn-issue-price', 2, 'ASP'),
('creategrn-issue-rebuilt', -1, 'AEDSPI'),
('creategrn-issue-rebuilt', 1, 'AEDSP'),
('creategrn-issue-rebuilt', 2, 'AESP'),
('creategrn-issue-rebuilt', 3, 'SP'),
('creategrn-issue-rebuilt-use', -1, 'AEDSPI'),
('creategrn-issueofrebuild', -1, 'AEDSPI'),
('creategrn-issueofrebuiltyres', -1, 'AEDSPI'),
('creategrn-issuetorebuild', -1, 'AEDSPI'),
('creategrn-issuetorebuild', 1, 'AEDSP'),
('creategrn-issuetorebuild', 2, 'AESP'),
('creategrn-issuetorebuild', 3, 'SP'),
('creategrn-new', -1, 'AEDSPI'),
('creategrn-new', 1, 'AEDSP'),
('creategrn-new', 2, 'ASP'),
('creategrn-purchase', -1, 'AEDSPI'),
('creategrn-quick', -1, 'ASPI'),
('creategrn-rebuilt-issue', -1, 'AEDSPI'),
('creategrn-receipt', -1, 'ASPI'),
('creategrn-receive', -1, 'AEDSPI'),
('creategrn-receive', 1, 'AEDSP'),
('creategrn-receive', 2, 'AESP'),
('creategrn-receive', 3, 'SP'),
('creategrn-receive-rebuilt', -1, 'AEDSPI'),
('creategrn-receiveafterrebuild', -1, 'AEDSPI'),
('creategrn-receiveafterrebuild', 1, 'AEDSP'),
('creategrn-receiveafterrebuild', 2, 'AESP'),
('creategrn-receiveafterrebuild', 3, 'SP'),
('creategrn-removal', -1, 'AEDSPI'),
('creategrn-remove', -1, 'AEDSPI'),
('creategrn-remove', 1, 'AEDSP'),
('creategrn-remove', 2, 'AESP'),
('creategrn-remove', 3, 'SP'),
('creategrn-remove-other', -1, 'AEDSPI'),
('creategrn-remove-other', 1, 'AEDSP'),
('creategrn-remove-other', 2, 'AESP'),
('creategrn-remove-other', 3, 'SP'),
('creategrn-stock-balance', -1, 'SP'),
('creategrn-stock-balance', 1, 'SP'),
('creategrn-stock-balance', 2, 'SP'),
('creategrn-stock-balance', 3, 'SP'),
('creategrn-used', -1, 'AEDSPI'),
('creategrn-used', 1, 'AEDSP'),
('creategrn-used', 2, 'ASP'),
('creategrn1', -1, 'ASPI'),
('creategrn11', -1, 'ASPI'),
('Dashboard', -1, 'S'),
('fleettype', -1, 'AEDSPI'),
('fleettype', 1, 'ADESP'),
('fleettype', 2, 'AESP'),
('fleettype', 3, 'SP'),
('fuelmaster', -1, 'AEDSPI'),
('fuelmaster', 1, 'AEDSP'),
('fuelmaster', 2, 'AESP'),
('fuelmaster', 3, 'SP'),
('fuelmaster Chart', -1, 'S'),
('fuelmaster Chart', 1, 'S'),
('fuelmaster Chart', 2, 'S'),
('fuelmaster Chart', 3, 'S'),
('fuelmaster-avg', -1, 'AEDSPI'),
('fuelmaster-report', -1, 'SP'),
('fuelmaster-reporting', -1, 'SP'),
('fuelmaster-reports', -1, 'SP'),
('fuelmaster-reports', 1, 'SP'),
('fuelmaster-reports', 2, 'SP'),
('fuelmaster-reports', 3, 'SP'),
('fuelmaster-view', -1, 'AEDSPI'),
('fuelmaster1', -1, 'AEDSPI'),
('fuelprices', -1, 'AEDSPI'),
('fuelprices', 1, 'ADESP'),
('fuelprices', 2, 'AESP'),
('fuelprices', 3, 'SP'),
('fuelstationmaster', -1, 'AEDSPI'),
('fuelstationmaster', 1, 'ADESP'),
('fuelstationmaster', 2, 'AESP'),
('fuelstationmaster', 3, 'SP'),
('generalmaster', -1, 'AEDSPI'),
('home', -1, 'AEDSPI'),
('insurance-payments', -1, 'AEDSPI'),
('insurance-payments', 1, 'AEDSP'),
('insurance-payments', 2, 'AESP'),
('insurance-payments', 3, 'SP'),
('insurance-payments-report', -1, 'SP'),
('insuranceclaims', -1, 'AEDSPI'),
('insuranceclaims', 1, 'AEDSP'),
('insuranceclaims', 2, 'AESP'),
('insuranceclaims', 3, 'SP'),
('insuranceclaims-report', -1, 'SP'),
('insurancecompany', -1, 'AEDSPI'),
('insurancecompany', 1, 'ADESP'),
('insurancecompany', 2, 'AESP'),
('insurancecompany', 3, 'SP'),
('inventorymaster', -2, 'ASP'),
('inventorymaster', -1, 'ADESPIM'),
('inventorymaster', 1, 'ADESP'),
('inventorymaster', 2, 'AESP'),
('inventorymaster', 3, 'SP'),
('inventorymaster Chart', -1, 'S'),
('inventorymaster Report', -1, 'SP'),
('inventorymaster-max', -1, 'AEDSPI'),
('inventorymaster-pricing', -1, 'M'),
('inventorymaster-qty', -1, 'M'),
('maintenenace', -1, 'SP'),
('maintenenace', 1, 'ADESP'),
('maintenenace', 2, 'AESP'),
('maintenenace', 3, 'SP'),
('maintenenace Chart', -1, 'S'),
('maintenenace Chart', 1, 'S'),
('maintenenace Chart', 2, 'S'),
('maintenenace Chart', 3, 'S'),
('maintenenace Comp', -1, 'S'),
('maintenenace-accident-repair', -1, 'AEDSPI'),
('maintenenace-accident-repair', 1, 'AEDSP'),
('maintenenace-accident-repair', 2, 'AESP'),
('maintenenace-accident-repair', 3, 'SP'),
('maintenenace-accidentrepair', -1, 'AEDSPI'),
('maintenenace-general-repair', -1, 'AEDSPI'),
('maintenenace-general-repair', 1, 'AEDSP'),
('maintenenace-general-repair', 2, 'AESP'),
('maintenenace-general-repair', 3, 'SP'),
('maintenenace-generalrepair', -1, 'AEDSPI'),
('maintenenace-other-maintain', -1, 'AEDSPI'),
('maintenenace-other-maintain', 1, 'AEDSP'),
('maintenenace-other-maintain', 2, 'AESP'),
('maintenenace-regularservice', -1, 'AEDSPI'),
('maintenenace-regularservice', 1, 'AEDSP'),
('maintenenace-regularservice', 2, 'AESP'),
('maintenenace-regularservice', 3, 'SP'),
('maintenenace-report', -1, 'SPI'),
('maintenenace-report', 1, 'SP'),
('maintenenace-report', 2, 'SP'),
('maxprice', -1, 'AEDSPI'),
('otherrenewal', -1, 'AEDSP'),
('otherrenewal', 1, 'AEDSP'),
('otherrenewal', 2, 'AESP'),
('otherrenewal', 3, 'SP'),
('OtherRenewals', -1, 'AEDSPI'),
('RenewalsMaster', -1, 'AEDSPI'),
('rnewalmastertable', -1, 'AEDSP'),
('rnewalmastertable', 1, 'AEDSP'),
('rnewalmastertable', 2, 'AESP'),
('rnewalmastertable', 3, 'SP'),
('servicetypemaster', -1, 'AEDSPI'),
('servicetypemaster', 1, 'ADESP'),
('servicetypemaster', 2, 'AESP'),
('servicetypemaster', 3, 'SP'),
('stockissues', -1, 'AEDSPI'),
('stockissues', 1, 'AEDSP'),
('stockissues', 2, 'ASP'),
('suppliermaster', -1, 'AEDSPI'),
('suppliermaster', 1, 'ADESP'),
('suppliermaster', 2, 'AESP'),
('suppliermaster', 3, 'SP'),
('usedornew', -1, 'AEDSPI'),
('vehiclemaster', -1, 'AEDSPI'),
('vehiclemaster', 1, 'ADESP'),
('vehiclemaster', 2, 'AESP'),
('vehiclemaster', 3, 'SP'),
('vehiclemaster-fullcost', -1, 'SP'),
('vehiclemaster-insu', -1, 'AEDSPI'),
('vehiclemaster-new', -1, 'AEDSPI'),
('vehiclemaster-report', -1, 'AEDSPI'),
('vehiclemaster-report', 1, 'SP'),
('vehiclemaster-report', 2, 'SP'),
('vehiclemaster-report', 3, 'SP'),
('vehicletype', -1, 'AEDSPI'),
('vehicletype', 1, 'ADESP'),
('vehicletype', 2, 'AESP'),
('vehicletype', 3, 'SP');

-- --------------------------------------------------------

--
-- Structure de la table `carte_naftal`
--

CREATE TABLE `carte_naftal` (
  `id` int(11) NOT NULL,
  `numero` varchar(20) DEFAULT NULL,
  `debit` varchar(12) DEFAULT NULL,
  `credit` varchar(12) DEFAULT NULL,
  `date_expiration` varchar(10) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  `code_wilaya` varchar(3) DEFAULT NULL,
  `id_vehicle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `chauffeur`
--

CREATE TABLE `chauffeur` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) COLLATE utf8_bin NOT NULL,
  `prenom` varchar(50) COLLATE utf8_bin NOT NULL,
  `date_recrutement` varchar(10) COLLATE utf8_bin NOT NULL,
  `type_permis` varchar(10) COLLATE utf8_bin NOT NULL,
  `validite_permis` varchar(10) COLLATE utf8_bin NOT NULL,
  `id_structure` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `chauffeur`
--

INSERT INTO `chauffeur` (`id`, `nom`, `prenom`, `date_recrutement`, `type_permis`, `validite_permis`, `id_structure`) VALUES
(1, 'HADJ CHAIB', 'FARID', '0000-00-00', 'B', '14/02/2026', 62),
(2, 'BELKHIR', 'MOHAMED', '0000-00-00', 'A,B,C', '13/10/2024', 62);

-- --------------------------------------------------------

--
-- Structure de la table `compagnie_assurance`
--

CREATE TABLE `compagnie_assurance` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `numero_police` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `code_wilaya` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `creategrn`
--

CREATE TABLE `creategrn` (
  `ID` int(11) NOT NULL,
  `ItemCode` varchar(50) DEFAULT NULL,
  `Brand` varchar(50) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Supplier` varchar(50) DEFAULT NULL,
  `Quantity` varchar(50) DEFAULT NULL,
  `RemovedFrom` varchar(50) DEFAULT NULL,
  `SystemDate` varchar(50) DEFAULT NULL,
  `GRNDate` varchar(50) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `ApprovedBy` varchar(50) DEFAULT NULL,
  `UnitPrice` varchar(50) DEFAULT NULL,
  `Status` varchar(50) DEFAULT NULL,
  `RefNumber` varchar(50) DEFAULT NULL,
  `CurrentStock` varchar(50) DEFAULT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `PriceLink` varchar(50) DEFAULT NULL,
  `Cost` varchar(50) DEFAULT NULL,
  `Remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `creategrn`
--

INSERT INTO `creategrn` (`ID`, `ItemCode`, `Brand`, `Description`, `Supplier`, `Quantity`, `RemovedFrom`, `SystemDate`, `GRNDate`, `EnteredBy`, `ApprovedBy`, `UnitPrice`, `Status`, `RefNumber`, `CurrentStock`, `Fleet`, `PriceLink`, `Cost`, `Remarks`) VALUES
(212, '2666TW', 'Ceat', 'Tyres', 'Kushi Tyres', '23', NULL, '2016-08-25 07:26:18', '2016-08-02 00:00:00', 'Mark Antony', NULL, '25', 'Purchase', '538', NULL, 'Cargo Carriers', NULL, '575', NULL),
(213, '5116519X', 'Toyota', 'Oil Filter', 'Meiken Traders', '11', NULL, '2016-08-25 07:26:56', '2016-08-04 00:00:00', 'Mark Antony', NULL, '360', 'Purchase', '8767', NULL, 'Cargo Carriers', NULL, '3960', NULL),
(214, '2666TW', 'Ceat', 'Tyres', 'Kushi Tyres', '231', NULL, '2016-08-25 07:27:20', '2016-08-26 00:00:00', 'Mark Antony', NULL, '12.50', 'Purchase', '7788', NULL, 'Cement Carriers', NULL, '2887.5', NULL),
(215, '54646G', 'Honda', 'Air Filter', 'Antony\'s Hardwares', '8', NULL, '2016-08-25 07:27:53', '2016-08-16 00:00:00', 'Mark Antony', NULL, '125', 'Purchase', '768', NULL, 'Container Carriers', NULL, '1000', NULL),
(216, '5116519X', 'Toyota', 'Oil Filter', 'Meiken Traders', '25', NULL, '2016-08-25 07:28:11', '2016-08-11 00:00:00', 'Mark Antony', NULL, '35', 'Purchase', '587', NULL, 'Container Carriers', NULL, '875', NULL),
(217, 'SQ234', 'Caltex', 'Motor Oil', 'NKS Motor Spares', '5', NULL, '2016-08-25 07:28:45', '2016-08-09 00:00:00', 'Mark Antony', NULL, '235', 'Purchase', '28776', NULL, 'Cement Carriers', NULL, '1175', NULL),
(218, '54646G', 'Honda', 'Air Filter', 'Antony\'s Hardwares', '12', NULL, '2016-08-25 07:29:27', '2016-08-10 00:00:00', 'Mark Antony', NULL, '75', 'Purchase', '868', NULL, 'Cargo Carriers', NULL, '900', NULL),
(219, '5116519X', 'Toyota', 'Oil Filter', 'Meiken Traders', '-10', 'BF1470', '2016-08-25 08:04:03', '2016-08-09 00:00:00', 'Mark Antony', NULL, '35', 'Issue', NULL, '27', 'Cargo Carriers', '5116519X', NULL, ''),
(220, 'SQ234', 'Caltex', 'Motor Oil', 'NKS Motor Spares', '-2', 'WK5874', '2016-08-25 08:04:43', '2016-08-09 00:00:00', 'Mark Antony', NULL, '235', 'Issue', NULL, '4', 'Cargo Carriers', 'SQ234', NULL, ''),
(221, '5116519X', 'Toyota', 'Oil Filter', 'Meiken Traders', '-18', 'KR6584', '2016-08-25 08:09:01', '2016-08-09 00:00:00', 'Mark Antony', NULL, '35', 'Issue', NULL, '26', 'Container Carriers', '5116519X', NULL, ''),
(222, '2666TW', 'Ceat', 'Tyres', 'Kushi Tyres', '-112', 'EF4771', '2016-08-25 08:09:39', '2016-08-03 00:00:00', 'Mark Antony', NULL, '12.50', 'Issue', NULL, '254', 'Cement Carriers', '2666TW', NULL, ''),
(223, '54646G', 'Honda', 'Air Filter', 'Antony\'s Hardwares', '-2', 'KI5455', '2016-08-25 08:10:08', '2016-08-02 00:00:00', 'Mark Antony', NULL, '75', 'Issue', NULL, '20', 'Container Carriers', '54646G', NULL, ''),
(224, '2666TW', 'Ceat', 'Tyres', 'Kushi Tyres', '1', 'WK5874', '2016-08-25 08:12:54', '2016-08-10 00:00:00', 'Mark Antony', NULL, '0', 'Removal', NULL, NULL, 'Cargo Carriers', NULL, '0', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `fleets`
--

CREATE TABLE `fleets` (
  `ID` int(11) NOT NULL,
  `marque` varchar(50) DEFAULT NULL,
  `modele` varchar(50) DEFAULT NULL,
  `type_chauffeur` varchar(50) DEFAULT NULL,
  `numero_immatriculation` varchar(50) DEFAULT NULL,
  `numero_immatriculation_old` varchar(50) DEFAULT NULL,
  `nom_chauffeur` varchar(50) DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  `motorisation` varchar(50) DEFAULT NULL,
  `compagnie_assurance` varchar(50) DEFAULT NULL,
  `gps_tracker` varchar(3) DEFAULT 'NON',
  `designation` varchar(20) NOT NULL,
  `nom_etablissement` varchar(100) NOT NULL,
  `code_wilaya` varchar(3) NOT NULL,
  `type_permis` varchar(1) NOT NULL,
  `validation_permis` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fleets`
--

INSERT INTO `fleets` (`ID`, `marque`, `modele`, `type_chauffeur`, `numero_immatriculation`, `numero_immatriculation_old`, `nom_chauffeur`, `etat`, `motorisation`, `compagnie_assurance`, `gps_tracker`, `designation`, `nom_etablissement`, `code_wilaya`, `type_permis`, `validation_permis`) VALUES
(14, '', '', '', 'undefined', 'undefined', NULL, '', '', '', '', '', '', '', '', ''),
(15, 'Hyundai', 'ELANTRA', 'Habituel', 'undefined', 'undefined', NULL, 'BON ETAT', 'ESSENCE', 'CAAR', 'OUI', 'AGENCE TIARET', 'AGENCE TIARET', 'W14', 'B', '28/02/2026'),
(16, 'Hyundai', 'ELANTRA', 'Habituel', 'undefined', 'undefined', NULL, 'BON ETAT', 'ESSENCE', 'CAAR', 'OUI', 'AGENCE TIARET', 'AGENCE TIARET', 'W14', 'B', '28/02/2026'),
(17, '', '', '', '01798-989-14', '02020-111-14', NULL, '', '', '', '', '', '', '', '', '');

-- --------------------------------------------------------

--
-- Structure de la table `fleettype`
--

CREATE TABLE `fleettype` (
  `Id` int(11) NOT NULL,
  `FleetType` varchar(50) DEFAULT NULL,
  `In-Charge` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fleettype`
--

INSERT INTO `fleettype` (`Id`, `FleetType`, `In-Charge`) VALUES
(1, 'Container Carriers', 'Daniel Thomas'),
(2, 'Cement Carriers', 'David Brian'),
(7, 'Cargo Carriers', 'Frank Anderson');

-- --------------------------------------------------------

--
-- Structure de la table `fuelmaster`
--

CREATE TABLE `fuelmaster` (
  `Id` int(11) NOT NULL,
  `SystemDate` varchar(50) DEFAULT NULL,
  `FuelDate` varchar(50) DEFAULT NULL,
  `Vehicle` varchar(50) DEFAULT NULL,
  `MeterReading` varchar(50) DEFAULT NULL,
  `LitersPumped` varchar(50) DEFAULT NULL,
  `PricePerLiter` varchar(50) DEFAULT NULL,
  `FuelStation` varchar(50) DEFAULT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `LastMileage` varchar(50) DEFAULT NULL,
  `Economy` varchar(50) DEFAULT NULL,
  `FillType` varchar(50) DEFAULT NULL,
  `CouponNo` varchar(50) DEFAULT NULL,
  `Driver` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fuelmaster`
--

INSERT INTO `fuelmaster` (`Id`, `SystemDate`, `FuelDate`, `Vehicle`, `MeterReading`, `LitersPumped`, `PricePerLiter`, `FuelStation`, `Fleet`, `LastMileage`, `Economy`, `FillType`, `CouponNo`, `Driver`) VALUES
(56, '2016-08-25 06:25:09', '2016-08-01 00:00:00', 'KI5455', '25233', '25', '45', 'Kiaco Fuel Station', 'Container Carriers', '24950', '11.32', 'Full Tank', '355', 'Kumar Sedhi'),
(57, '2016-08-25 06:36:15', '2016-08-04 00:00:00', 'KI5455', '25850', '35', '45', 'SK Fuel Station', 'Container Carriers', '25300', '15.714285714286', 'Full Tank', '863', 'Kumar Sedhi'),
(58, '2016-08-25 06:37:24', '2016-08-15 00:00:00', 'KI5455', '26250', '40', '45', 'Kiaco Fuel Station', 'Container Carriers', '25850', '10', 'Full Tank', '7598', 'Kumar Sedhi'),
(59, '2016-08-25 06:38:17', '2016-08-17 00:00:00', 'KI5455', '26752', '36', '45', 'Kiaco Fuel Station', 'Container Carriers', '26250', '13.944444444444', 'Full Tank', '8585', 'Kumar Sedhi'),
(60, '2016-08-25 06:39:00', '2016-08-22 00:00:00', 'KI5455', '27124', '41', '45', 'SK Fuel Station', 'Container Carriers', '26752', '9.0731707317073', 'Full Tank', '2577', 'Kumar Sedhi');

-- --------------------------------------------------------

--
-- Structure de la table `fuelprices`
--

CREATE TABLE `fuelprices` (
  `Id` int(11) NOT NULL,
  `FuelType` varchar(50) DEFAULT NULL,
  `PricePerLiter` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fuelprices`
--

INSERT INTO `fuelprices` (`Id`, `FuelType`, `PricePerLiter`) VALUES
(1, 'Diesel', '30'),
(2, 'Petrol', '45');

-- --------------------------------------------------------

--
-- Structure de la table `fuelstationmaster`
--

CREATE TABLE `fuelstationmaster` (
  `Id` int(11) NOT NULL,
  `FuelStation` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `ContactNumber` varchar(50) DEFAULT NULL,
  `Deposit` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `fuelstationmaster`
--

INSERT INTO `fuelstationmaster` (`Id`, `FuelStation`, `Address`, `ContactNumber`, `Deposit`) VALUES
(1, 'Kiaco Fuel Station', '25E Main Street', '+516546416', '500000'),
(2, 'SK Fuel Station', '656 Henty Road', '+87484565', '1000000');

-- --------------------------------------------------------

--
-- Structure de la table `historique_accident`
--

CREATE TABLE `historique_accident` (
  `id` int(11) NOT NULL,
  `id_vehicle` int(11) NOT NULL,
  `accident_date` varchar(10) COLLATE utf8_bin NOT NULL,
  `accident_time` varchar(10) COLLATE utf8_bin NOT NULL,
  `details` varchar(200) COLLATE utf8_bin NOT NULL,
  `id_chauffeur` int(11) DEFAULT NULL,
  `constat_accident` varchar(100) COLLATE utf8_bin NOT NULL,
  `date_system` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `historique_affectation`
--

CREATE TABLE `historique_affectation` (
  `id` int(11) NOT NULL,
  `id_priorietaire` int(11) DEFAULT NULL,
  `id_structure` int(11) DEFAULT NULL,
  `date_affectation` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `type_affectation` varchar(20) COLLATE utf8_bin DEFAULT NULL,
  `numero_decision` int(11) DEFAULT NULL,
  `document_decision` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `etat` varchar(100) COLLATE utf8_bin DEFAULT NULL,
  `kilometrage` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `id_vehicle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `historique_assurance`
--

CREATE TABLE `historique_assurance` (
  `id` int(11) NOT NULL,
  `id_vehicle` int(11) NOT NULL,
  `date_debut` varchar(10) COLLATE utf8_bin NOT NULL,
  `date_fin` varchar(10) COLLATE utf8_bin NOT NULL,
  `id_compagnie` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `historique_entretien`
--

CREATE TABLE `historique_entretien` (
  `id` int(11) NOT NULL,
  `id_vehicle` int(11) NOT NULL,
  `kilometrage` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `entretien` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `cout` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `date_system` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_entretien` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `id_reparateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `historique_reparation`
--

CREATE TABLE `historique_reparation` (
  `id` int(11) NOT NULL,
  `id_vehicle` int(11) NOT NULL,
  `reparations` varchar(250) COLLATE utf8_bin DEFAULT NULL,
  `cout` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `date_system` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `date_reparation` varchar(10) COLLATE utf8_bin DEFAULT NULL,
  `code_wilaya` varchar(3) COLLATE utf8_bin NOT NULL,
  `id_reparateur` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

-- --------------------------------------------------------

--
-- Structure de la table `historique_vehicle_chauffeur`
--

CREATE TABLE `historique_vehicle_chauffeur` (
  `id` int(11) NOT NULL,
  `id_vehicle` int(11) NOT NULL,
  `id_chauffeur` int(11) NOT NULL,
  `kilometrage` varchar(10) COLLATE utf8_bin NOT NULL,
  `etat` varchar(100) COLLATE utf8_bin NOT NULL,
  `date_affectation` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `date_depart` varchar(20) COLLATE utf8_bin NOT NULL,
  `date_retour` varchar(20) COLLATE utf8_bin NOT NULL,
  `mission` varchar(100) COLLATE utf8_bin NOT NULL,
  `motif_retard` varchar(100) COLLATE utf8_bin NOT NULL,
  `type_chauffeur` varchar(20) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `historique_vehicle_chauffeur`
--

INSERT INTO `historique_vehicle_chauffeur` (`id`, `id_vehicle`, `id_chauffeur`, `kilometrage`, `etat`, `date_affectation`, `date_depart`, `date_retour`, `mission`, `motif_retard`, `type_chauffeur`) VALUES
(3, 338, 1, '', '', '2018-04-19 09:19:22', '', '', '', '', 'HABITUEL'),
(4, 339, 1, '', '', '2018-04-19 09:24:10', '', '', '', '', 'HABITUEL'),
(5, 342, 1, '', '', '2018-04-23 16:34:39', '', '', '', '', 'OCCASIONNEL'),
(6, 342, 2, '', '', '2018-05-06 07:54:18', '', '', '', '', 'OCCASIONNEL');

-- --------------------------------------------------------

--
-- Structure de la table `insurance-payments`
--

CREATE TABLE `insurance-payments` (
  `Id` int(11) NOT NULL,
  `SysDate` varchar(50) DEFAULT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `VehicleNo` varchar(50) DEFAULT NULL,
  `RenewalDueDate` varchar(50) DEFAULT NULL,
  `Premium` varchar(50) DEFAULT NULL,
  `Cost` varchar(50) DEFAULT NULL,
  `PaymentVoucher` varchar(50) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `DateofPayment` varchar(50) DEFAULT NULL,
  `Insurer` varchar(50) DEFAULT NULL,
  `Date` varchar(50) DEFAULT NULL,
  `From` varchar(50) DEFAULT NULL,
  `To` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `insurance-payments`
--

INSERT INTO `insurance-payments` (`Id`, `SysDate`, `Fleet`, `Type`, `VehicleNo`, `RenewalDueDate`, `Premium`, `Cost`, `PaymentVoucher`, `EnteredBy`, `DateofPayment`, `Insurer`, `Date`, `From`, `To`) VALUES
(5, '2016-08-25 08:58:29', 'Cement Carriers', 'Machine', 'SK3266', '22 June', '3650', NULL, '2578', 'Mark Antony', '2016-08-23 00:00:00', 'Allianz Insurance', NULL, '2016-08-09 00:00:00', '2017-08-23 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `insuranceclaims`
--

CREATE TABLE `insuranceclaims` (
  `Id` int(11) NOT NULL,
  `SysDate` varchar(50) DEFAULT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `VehicleNo` varchar(50) DEFAULT NULL,
  `AccidentDate` varchar(50) DEFAULT NULL,
  `Claim` varchar(50) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `ReceiptNo` varchar(50) DEFAULT NULL,
  `Remarks` varchar(50) DEFAULT NULL,
  `insurer` varchar(50) DEFAULT NULL,
  `Date` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `insuranceclaims`
--

INSERT INTO `insuranceclaims` (`Id`, `SysDate`, `Fleet`, `Type`, `VehicleNo`, `AccidentDate`, `Claim`, `EnteredBy`, `ReceiptNo`, `Remarks`, `insurer`, `Date`) VALUES
(6, '2016-08-25 09:18:17', 'Cement Carriers', 'Machine', 'SK3266', '4', '2500', 'Mark Antony', '558', '', 'AIA Insurance Plc', '2016-08-18 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `insurancecompany`
--

CREATE TABLE `insurancecompany` (
  `Id` int(11) NOT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `ContactName` varchar(50) DEFAULT NULL,
  `ContactNo` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `insurancecompany`
--

INSERT INTO `insurancecompany` (`Id`, `Name`, `ContactName`, `ContactNo`) VALUES
(1, 'Allianz Insurance', 'Ajmal Nsheed', '+289124656'),
(2, 'AIA Insurance Plc', 'Arundhi Roy', '+54665699');

-- --------------------------------------------------------

--
-- Structure de la table `inventorymaster`
--

CREATE TABLE `inventorymaster` (
  `Id` int(11) NOT NULL,
  `ItemID` varchar(50) DEFAULT NULL,
  `Brand` varchar(50) DEFAULT NULL,
  `Description` varchar(50) DEFAULT NULL,
  `Supplier` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `inventorymaster`
--

INSERT INTO `inventorymaster` (`Id`, `ItemID`, `Brand`, `Description`, `Supplier`) VALUES
(1, 'SQ234', 'Caltex', 'Motor Oil', 'NKS Motor Spares'),
(2, '54646G', 'Honda', 'Air Filter', 'Antony\'s Hardwares'),
(5, '5116519X', 'Toyota', 'Oil Filter', 'Meiken Traders'),
(6, '2666TW', 'Ceat', 'Tyres', 'Kushi Tyres');

-- --------------------------------------------------------

--
-- Structure de la table `maintenenace`
--

CREATE TABLE `maintenenace` (
  `Id` int(11) NOT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `Vehicle` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `Supplier` varchar(50) DEFAULT NULL,
  `Cost` varchar(50) DEFAULT NULL,
  `Remarks` varchar(50) DEFAULT NULL,
  `RefNo` varchar(50) DEFAULT NULL,
  `SysDate` varchar(50) DEFAULT NULL,
  `Date` varchar(50) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `Approval` varchar(50) DEFAULT NULL,
  `MeterReading` varchar(50) DEFAULT NULL,
  `AccidentRef` varchar(50) DEFAULT NULL,
  `PaymentVoucher` varchar(50) DEFAULT NULL,
  `MaintType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `maintenenace`
--

INSERT INTO `maintenenace` (`Id`, `Fleet`, `Vehicle`, `Type`, `Supplier`, `Cost`, `Remarks`, `RefNo`, `SysDate`, `Date`, `EnteredBy`, `Approval`, `MeterReading`, `AccidentRef`, `PaymentVoucher`, `MaintType`) VALUES
(23, 'Cargo Carriers', 'BF1470', 'Lorry', 'Kushi Tyres', '2500', '', '15', '2016-11-22 12:37:04', '2016-11-22 00:00:00', 'Brian Thomas', NULL, '25402', NULL, '5455', 'Full Service'),
(24, 'Container Carriers', 'KR6584', 'Machine', 'Meiken Traders', '2540', '', '6554', '2016-11-22 12:37:32', '2016-11-08 00:00:00', 'Brian Thomas', NULL, '25466', NULL, '545', 'Lub Service');

-- --------------------------------------------------------

--
-- Structure de la table `otherrenewal`
--

CREATE TABLE `otherrenewal` (
  `ID` int(11) NOT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `VehicleNo` varchar(50) DEFAULT NULL,
  `VehicleType` varchar(50) DEFAULT NULL,
  `PaymentType` varchar(50) DEFAULT NULL,
  `PaymentDate` varchar(50) DEFAULT NULL,
  `Cost` varchar(50) DEFAULT NULL,
  `SystemDate` varchar(50) DEFAULT NULL,
  `EnteredBy` varchar(50) DEFAULT NULL,
  `PeriodFrom` varchar(50) DEFAULT NULL,
  `PeriodTo` varchar(50) DEFAULT NULL,
  `PaymentRef` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `otherrenewal`
--

INSERT INTO `otherrenewal` (`ID`, `Fleet`, `VehicleNo`, `VehicleType`, `PaymentType`, `PaymentDate`, `Cost`, `SystemDate`, `EnteredBy`, `PeriodFrom`, `PeriodTo`, `PaymentRef`) VALUES
(2, 'Cement Carriers', 'SK3266', 'Machine', 'Emission Test', '2016-08-17 00:00:00', '2500', '2016-08-25 09:11:44', 'Mark Antony', '2016-08-16 00:00:00', '2017-08-16 00:00:00', '6336');

-- --------------------------------------------------------

--
-- Structure de la table `priorietaire`
--

CREATE TABLE `priorietaire` (
  `id` int(11) NOT NULL,
  `nom` varchar(100) COLLATE utf8_bin NOT NULL,
  `id_type_structure` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `priorietaire`
--

INSERT INTO `priorietaire` (`id`, `nom`, `id_type_structure`) VALUES
(3, 'MINISTERE DE LA JUSTICE', 4),
(4, 'MINISTERE DU TRAVAIL DE L\'EMPLOI ET DE LA SECURITE SOCIALE', 4),
(5, 'PRESIDENT DU CONSEIL D\'ADMINISTRATION', 4),
(6, 'DIRECTEUR GENERAL', 2),
(7, 'DIRECTEUR GENERAL ADJOINT', 2),
(8, 'PRESIDENT DE LA COMMISSION NATIONALE MEDICALE', 4),
(9, 'D.ASS', 2),
(10, 'D.ATMP', 2),
(11, 'D.CM', 2),
(12, 'D.ESO', 2),
(13, 'D.INFORMATIQUE', 2),
(14, 'D.INSPCTION', 2),
(15, 'D.OF', 2),
(16, 'D.P', 2),
(17, 'D.PF', 2),
(18, 'D.RC', 2),
(19, 'D.REMG', 2),
(20, 'C.CONTENTIEUX', 2),
(21, 'C.CONVENTIONNEMENT', 2),
(22, 'SIE', 2),
(23, 'ASSISTANT CONSEILLER(E)', 4),
(24, 'ASSISTANT DE DIRECTION', 4),
(25, 'ECOLE SUPERIEURE DE LA SECURITE SOCIALE', 4),
(26, 'DIRECTEUR AGENCE', 1),
(27, 'DIRECTEUR ETABLISSEMENT', 3),
(28, 'ŒUVRE SOCIALE', 4),
(29, 'PARC AUTOMOBILE', 1);

-- --------------------------------------------------------

--
-- Structure de la table `reparateur`
--

CREATE TABLE `reparateur` (
  `id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `telephone` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `code_wilaya` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `reparateur`
--

INSERT INTO `reparateur` (`id`, `nom`, `telephone`, `adresse`, `code_wilaya`) VALUES
(1, 'TAHRI ABBES', '0770401041', 'RUE AUROLAIT ROUTE BOUCHAKIF TIARET', 'W14');

-- --------------------------------------------------------

--
-- Structure de la table `rnewalmastertable`
--

CREATE TABLE `rnewalmastertable` (
  `ID` int(11) NOT NULL,
  `Renewal` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `rnewalmastertable`
--

INSERT INTO `rnewalmastertable` (`ID`, `Renewal`) VALUES
(1, 'Fitness Certificate'),
(2, 'Port Permit'),
(3, 'Emission Test');

-- --------------------------------------------------------

--
-- Structure de la table `servicetypemaster`
--

CREATE TABLE `servicetypemaster` (
  `Id` int(11) NOT NULL,
  `Type` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `servicetypemaster`
--

INSERT INTO `servicetypemaster` (`Id`, `Type`) VALUES
(1, 'Full Service'),
(2, 'Lub Service'),
(3, 'Mechanical Service'),
(7, 'Other Maintenance');

-- --------------------------------------------------------

--
-- Structure de la table `structure_affectation`
--

CREATE TABLE `structure_affectation` (
  `Id` int(11) NOT NULL,
  `nom` varchar(100) DEFAULT NULL,
  `code_wilaya` varchar(3) NOT NULL,
  `id_type_structure` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `structure_affectation`
--

INSERT INTO `structure_affectation` (`Id`, `nom`, `code_wilaya`, `id_type_structure`) VALUES
(49, 'AGENCE Adrar', 'W01', 1),
(50, 'AGENCE Chlef', 'W02', 1),
(51, 'AGENCE Laghouat', 'W03', 1),
(52, 'AGENCE Oum El Boua', 'W04', 1),
(53, 'AGENCE Batna', 'W05', 1),
(54, 'AGENCE Béjaïa', 'W06', 1),
(55, 'AGENCE Biskra', 'W07', 1),
(56, 'AGENCE Béchar', 'W08', 1),
(57, 'AGENCE Blida', 'W09', 1),
(58, 'AGENCE Bouira', 'W10', 1),
(59, 'AGENCE Tamanrasse', 'W11', 1),
(60, 'AGENCE Tébessa', 'W12', 1),
(61, 'AGENCE Tlemcen', 'W13', 1),
(62, 'AGENCE Tiaret', 'W14', 1),
(63, 'AGENCE Tizi Ouzou', 'W15', 1),
(64, 'AGENCE Alger', 'W16', 1),
(65, 'AGENCE Djelfa', 'W17', 1),
(66, 'AGENCE Jijel', 'W18', 1),
(67, 'AGENCE Sétif', 'W19', 1),
(68, 'AGENCE Saïda', 'W20', 1),
(69, 'AGENCE Skikda', 'W21', 1),
(70, 'AGENCE Sidi Bel Abbes', 'W22', 1),
(71, 'AGENCE Annaba', 'W23', 1),
(72, 'AGENCE Guelma', 'W24', 1),
(73, 'AGENCE Constantin', 'W25', 1),
(74, 'AGENCE Médéa', 'W26', 1),
(75, 'AGENCE Mostaganem', 'W27', 1),
(76, 'AGENCE MSila', 'W28', 1),
(77, 'AGENCE Mascara', 'W29', 1),
(78, 'AGENCE Ouargla', 'W30', 1),
(79, 'AGENCE Oran', 'W31', 1),
(80, 'AGENCE El Bayadh', 'W32', 1),
(81, 'AGENCE Illizi', 'W33', 1),
(82, 'AGENCE Bordj Bou Arreridj', 'W34', 1),
(83, 'AGENCE Boumerdès', 'W35', 1),
(84, 'AGENCE El Tarf', 'W36', 1),
(85, 'AGENCE Tindouf', 'W37', 1),
(86, 'AGENCE Tissemsilt', 'W38', 1),
(87, 'AGENCE El Oued', 'W39', 1),
(88, 'AGENCE Khenchela', 'W40', 1),
(89, 'AGENCE Souk Ahras', 'W41', 1),
(90, 'AGENCE Tipaza', 'W42', 1),
(91, 'AGENCE Mila', 'W43', 1),
(92, 'AGENCE Aïn Defla', 'W44', 1),
(93, 'AGENCE Naâma', 'W45', 1),
(94, 'AGENCE Aïn Témouch', 'W46', 1),
(95, 'AGENCE Ghardaïa', 'W47', 1),
(96, 'AGENCE Relizane', 'W48', 1),
(97, 'EL DJORF ED\'DAHABI (MELBOU)', 'W06', 3),
(98, 'CENTRE FAMILIALE DE BEN AKNOUNE', 'W16', 3),
(99, 'IMPRIMERIE DE CONSTANTINE', 'W25', 3),
(100, 'CSORVAT MESSERGHINE', 'W31', 3),
(101, 'EHS-CMCI BOUISMAIL', 'W42', 3),
(102, 'DIRECTION GENERALE', 'W16', 2),
(103, 'MINISTERE DE LA JUSTICE', 'W16', 4),
(104, 'MINISTERE DU TRAVAIL DE L\'EMPLOI ET DE LA SECURITE SOCIALE', 'W16', 4),
(105, 'PRESIDENT DU CONSEIL D\'ADMINISTRATION', 'W16', 4),
(106, 'PRESIDENT DE LA COMMISSION NATIONALE MEDICALE', 'W16', 4),
(107, 'ASSISTANT CONSEILLER(E)', 'W16', 4),
(108, 'ASSISTANT DE DIRECTION', 'W16', 4),
(109, 'ECOLE SUPERIEURE DE LA SECURITE SOCIALE', 'W16', 4),
(110, 'ŒUVRE SOCIALE', 'W16', 4);

-- --------------------------------------------------------

--
-- Structure de la table `suppliermaster`
--

CREATE TABLE `suppliermaster` (
  `Id` int(11) NOT NULL,
  `SupplierName` varchar(50) DEFAULT NULL,
  `Address` varchar(50) DEFAULT NULL,
  `ContactPerson` varchar(50) DEFAULT NULL,
  `Telephone` varchar(50) DEFAULT NULL,
  `Remarks` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `suppliermaster`
--

INSERT INTO `suppliermaster` (`Id`, `SupplierName`, `Address`, `ContactPerson`, `Telephone`, `Remarks`) VALUES
(1, 'NKS Motor Spares', '546 Maek Aveneue', 'Davis Khan', '+3296656565', ''),
(2, 'Kushi Tyres', 'Jumaira Terras', 'Melani Hans', '+6521799196', ''),
(3, 'Meiken Traders', '245E Meinx Road', 'Anil Gupta', '+5665786786', ''),
(4, 'Antony\'s Hardwares', '62 Main Street', 'Mani Hussain', '+2948946115', '');

-- --------------------------------------------------------

--
-- Structure de la table `type_entretien`
--

CREATE TABLE `type_entretien` (
  `Id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_entretien`
--

INSERT INTO `type_entretien` (`Id`, `nom`) VALUES
(5, 'Filtre à air'),
(6, 'Filtre à huile'),
(7, 'Remplacement des bougies'),
(8, 'contrôle échappement'),
(9, 'Filtre à gazole'),
(10, 'check-up amortisseurs/pneumatiques'),
(11, 'La vidange');

-- --------------------------------------------------------

--
-- Structure de la table `type_structure`
--

CREATE TABLE `type_structure` (
  `Id` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `type_structure`
--

INSERT INTO `type_structure` (`Id`, `nom`) VALUES
(1, 'AGENCE'),
(2, 'DIRECTION GENERALE'),
(3, 'ETABLISSEMENT'),
(4, 'AUTRE');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `ID` int(11) NOT NULL,
  `username` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `fullname` varchar(300) DEFAULT NULL,
  `groupid` varchar(300) DEFAULT NULL,
  `active` int(11) DEFAULT NULL,
  `id_structure` int(11) NOT NULL,
  `code_wilaya` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`ID`, `username`, `password`, `email`, `fullname`, `groupid`, `active`, `id_structure`, `code_wilaya`) VALUES
(5, 'MANAGER', 'MANAGER', 'manager@cnas.dz', 'ABDELLATIF ESSELAMI', 'manager', 1, 62, 'W14'),
(6, 'CHEF_PARC_14', 'CNAS', 'chef_parc_14@cnas.dz', 'Chef Parc Tiaret', 'Chef_parc', 1, 62, 'W14');

-- --------------------------------------------------------

--
-- Structure de la table `vehicle`
--

CREATE TABLE `vehicle` (
  `ID` int(11) NOT NULL,
  `marque` varchar(50) DEFAULT NULL,
  `modele` varchar(50) DEFAULT NULL,
  `numero_immatriculation` varchar(50) NOT NULL,
  `numero_immatriculation_old` varchar(50) DEFAULT NULL,
  `etat` varchar(50) DEFAULT NULL,
  `motorisation` varchar(50) DEFAULT NULL,
  `gps_tracker` varchar(3) DEFAULT 'NON',
  `type_vehicle` varchar(50) NOT NULL,
  `numero_chassis` varchar(50) DEFAULT NULL,
  `code_wilaya` varchar(3) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vehicle`
--

INSERT INTO `vehicle` (`ID`, `marque`, `modele`, `numero_immatriculation`, `numero_immatriculation_old`, `etat`, `motorisation`, `gps_tracker`, `type_vehicle`, `numero_chassis`, `code_wilaya`) VALUES
(338, 'HYUNDAI', 'ELANTRA', '02071-117-14', '21214-00-14', 'NEUF', 'ESSENCE', 'OUI', 'VEHICULE TOURISTIQUE', 'KMHD841CAHU420639', 'W14'),
(339, 'Renault', 'KONGO', '00837-111-14', '14961-00-16', 'ETAT MOYEN', 'ESSENCE', 'NON', 'VEHICULE UTILITAIRE', 'VF1KWONB44734188', 'W14'),
(340, 'Renault', 'FLUENCE', '00858-111-14', '16751-00-16', 'BON ETAT', 'ESSENCE / GPL', 'NON', 'VEHICULE TOURISTIQUE', 'VF1LZBA44908489', 'W14'),
(341, 'Renault', 'KONGO', '03354-313-14', '35908-00-16', 'BON ETAT', 'DIESEL', 'NON', 'VEHICULE UTILITAIRE', 'VF1FW0LB48812531', 'W14'),
(342, 'Toyota', 'COROLA', '00282-104-14', '27660-00-04', 'ETAT MOYEN', 'DIESEL', 'NON', 'VEHICULE UTILITAIRE', 'CE12OLJTDBJ22E6', 'W14'),
(343, 'Isuzu', 'ISUZU', '05516-314-14', '41364-00-16', 'BON ETAT', 'DIESEL', 'NON', 'CAMION', 'JAA1KR55HD7101150', 'W14');

-- --------------------------------------------------------

--
-- Structure de la table `vehiclemaster`
--

CREATE TABLE `vehiclemaster` (
  `ID` int(11) NOT NULL,
  `RegNo` varchar(50) DEFAULT NULL,
  `Fleet` varchar(50) DEFAULT NULL,
  `Type` varchar(50) DEFAULT NULL,
  `RegDate` varchar(50) DEFAULT NULL,
  `Cost` varchar(50) DEFAULT NULL,
  `DriverAsigned` varchar(50) DEFAULT NULL,
  `Make` varchar(50) DEFAULT NULL,
  `Model` varchar(50) DEFAULT NULL,
  `InsuranceDue` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vehiclemaster`
--

INSERT INTO `vehiclemaster` (`ID`, `RegNo`, `Fleet`, `Type`, `RegDate`, `Cost`, `DriverAsigned`, `Make`, `Model`, `InsuranceDue`) VALUES
(1, 'KS1772', 'Container Carriers', 'Machine', '2014-04-09 00:00:00', '545000', 'Keith', 'Mitsubishi', 'MS403318', '10 June'),
(2, 'EF4771', 'Cement Carriers', 'Machine', '2014-01-16 00:00:00', '850000', 'Hussian', 'Volvo', 'LPW40122', '15 April'),
(3, 'BF1470', 'Cargo Carriers', 'Lorry', '2011-04-04 00:00:00', '658000', 'Antony Croos', 'Nissan', 'KMX4018E', '15 Jan'),
(4, 'WK5874', 'Cargo Carriers', 'Lorry', '2015-04-02 00:00:00', '468500', 'Anil Roy', 'Cherry', 'NOSI40133', '15 Dec'),
(5, 'KR6584', 'Container Carriers', 'Machine', '2014-04-09 00:00:00', '685000', 'Keith Nurega', 'Isuzu', 'UPS40185', '30 April'),
(6, 'SK3266', 'Cement Carriers', 'Machine', '2013-04-04 00:00:00', '475000', 'Ajith Siva', 'Toyota', 'BSP442018', '22 June'),
(7, 'KI5455', 'Container Carriers', 'Machine', '2015-04-04 00:00:00', '485000', 'Kumar Sedhi', 'Tata', 'NERS4018', '01 Jan'),
(8, 'SX1765', 'Cement Carriers', 'Machine', '2012-04-10 00:00:00', '650000', 'Anil Das', 'Tata', 'HTE40184', '19 Dec'),
(10, '654848', 'Cargo Carriers', 'Lorry', '2018-03-17 00:00:00', '36985', 'latif', 'mmm', 'hyundai', '1651650');

-- --------------------------------------------------------

--
-- Structure de la table `vehicletype`
--

CREATE TABLE `vehicletype` (
  `Id` int(11) NOT NULL,
  `VehicleType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vehicletype`
--

INSERT INTO `vehicletype` (`Id`, `VehicleType`) VALUES
(1, 'Machine'),
(2, 'Trailer'),
(3, 'Lorry'),
(4, 'Tanker'),
(5, 'ΤΑΞΙ');

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_brands`
--

CREATE TABLE `vehicle_brands` (
  `Id` int(11) NOT NULL,
  `brand` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vehicle_brands`
--

INSERT INTO `vehicle_brands` (`Id`, `brand`) VALUES
(1, 'HYUNDAI'),
(2, 'KIA'),
(3, 'Volkswagen'),
(4, 'Renault'),
(5, 'Seat'),
(6, 'Dacia'),
(7, 'Toyota'),
(8, 'Skoda'),
(9, 'Peugeot'),
(10, 'Audi'),
(11, 'Chevrolet'),
(12, 'Citroen'),
(13, 'BMW'),
(14, 'Suzuki'),
(15, 'Fiat'),
(16, 'Nissan'),
(17, 'Mercedes'),
(18, 'Land Rover'),
(19, 'Ford'),
(20, 'Chery'),
(21, 'Baic'),
(22, 'Great Wall'),
(23, 'DFSK'),
(24, 'Opel'),
(25, 'MG'),
(26, 'Porsche'),
(27, 'Mitsubishi'),
(28, 'Zotye'),
(29, 'Gonow'),
(30, 'Faw'),
(31, 'Jeep'),
(32, 'Mazda'),
(33, 'DS'),
(34, 'Iran Khodro'),
(35, 'Mini'),
(36, 'Emgrand'),
(37, 'Geely'),
(38, 'Haima'),
(39, 'Daewoo'),
(40, 'Lifan'),
(41, 'Daihatsu'),
(42, 'Alfa Romeo'),
(43, 'Mahindra'),
(44, 'BYD'),
(45, 'SsangYong'),
(46, 'Dodge'),
(47, 'Chana'),
(48, 'Brilliance'),
(49, 'Fiat Professional'),
(50, 'Changan'),
(51, 'Lancia'),
(52, 'Isuzu'),
(53, 'Tata'),
(54, 'JAC'),
(55, 'JMC'),
(56, 'Hafei motors'),
(57, 'DFM'),
(58, 'FOTON'),
(59, 'Saipa'),
(60, 'Aston Martin'),
(61, 'Bugatti'),
(62, 'Cadillac'),
(63, 'Chrysler'),
(64, 'Corvette'),
(65, 'Ferrari'),
(66, 'Honda'),
(67, 'Hummer'),
(68, 'Infiniti'),
(69, 'Jaguar'),
(70, 'Lada'),
(71, 'Lamborghini'),
(72, 'Lexus'),
(73, 'Maserati'),
(74, 'McLaren'),
(75, 'Rolls Royce'),
(76, 'Rover'),
(77, 'Saab'),
(78, 'Smart'),
(79, 'Subaru'),
(80, 'Volvo');

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_exterior_parts`
--

CREATE TABLE `vehicle_exterior_parts` (
  `id` int(11) NOT NULL,
  `part` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `vehicle_exterior_parts`
--

INSERT INTO `vehicle_exterior_parts` (`id`, `part`) VALUES
(1, 'Capot'),
(2, 'Aille Droite'),
(3, 'Aille Gauche'),
(4, 'Par choc Avant'),
(5, 'Par choc Arrière'),
(6, 'Porte Avant Droite'),
(7, 'Porte Avant Gauche'),
(8, 'Porte Arrière Droite'),
(9, 'Porte Arrière Gauche'),
(10, 'Bas de Caisse'),
(11, 'Lunette Avant'),
(12, 'Lunette Arrière'),
(13, 'Rétroviseur Droit'),
(14, 'Rétroviseur Gauche'),
(15, 'Feux Avant'),
(16, 'Feux Arrière'),
(17, 'PNEUS');

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_exterior_state`
--

CREATE TABLE `vehicle_exterior_state` (
  `id` int(11) NOT NULL,
  `capot` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `aille_droite` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `aille_gauche` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `par_choc_avant` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `par_choc_arriere` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `porte_avant_droite` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `porte_avant_gauche` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `porte_arriere_droite` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `porte_arriere_gauche` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `bas_de_caisse` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `lunette_avant` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `lunette_arriere` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `retroviseur_droit` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `retroviseur_gauche` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `feux_avant` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `feux_arriere` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `id_vehicle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `vehicle_exterior_state`
--

INSERT INTO `vehicle_exterior_state` (`id`, `capot`, `aille_droite`, `aille_gauche`, `par_choc_avant`, `par_choc_arriere`, `porte_avant_droite`, `porte_avant_gauche`, `porte_arriere_droite`, `porte_arriere_gauche`, `bas_de_caisse`, `lunette_avant`, `lunette_arriere`, `retroviseur_droit`, `retroviseur_gauche`, `feux_avant`, `feux_arriere`, `id_vehicle`) VALUES
(1, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 327),
(2, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 328),
(3, 'BB', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 297),
(4, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 330),
(5, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 331),
(6, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 332),
(7, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 333),
(8, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 334),
(9, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 335),
(10, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 336),
(11, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 337),
(12, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 338),
(13, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 339),
(14, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 340),
(15, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 341),
(16, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 342),
(17, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 343),
(18, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 344),
(19, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 345),
(20, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 344),
(21, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 345),
(22, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 346),
(23, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 347),
(24, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 348),
(25, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 349),
(26, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 350),
(27, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 351),
(28, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 352);

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_exterior_status`
--

CREATE TABLE `vehicle_exterior_status` (
  `id` int(11) NOT NULL,
  `id_part` int(11) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `id_vehicle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `vehicle_exterior_status`
--

INSERT INTO `vehicle_exterior_status` (`id`, `id_part`, `status`, `id_vehicle`) VALUES
(97, 1, 's', 360),
(98, 2, 's', 360),
(99, 3, 's', 360),
(100, 4, 's', 360),
(101, 5, 's', 360),
(102, 6, 's', 360),
(103, 7, 's', 360),
(104, 8, 's', 360),
(105, 9, 's', 360),
(106, 10, 's', 360),
(107, 11, 's', 360),
(108, 12, 's', 360),
(109, 13, 's', 360),
(110, 14, 's', 360),
(111, 15, 's', 360),
(112, 16, 's', 360);

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_interior_parts`
--

CREATE TABLE `vehicle_interior_parts` (
  `id` int(11) NOT NULL,
  `part` varchar(50) COLLATE utf8_bin NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `vehicle_interior_parts`
--

INSERT INTO `vehicle_interior_parts` (`id`, `part`) VALUES
(1, 'Siège Avant Droit'),
(2, 'Siège Avant Gauche'),
(3, 'Banquette Arrière');

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_interior_state`
--

CREATE TABLE `vehicle_interior_state` (
  `id` int(11) NOT NULL,
  `siege_avant_droit` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `siege_avant_gauche` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `banquette_arriere` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `id_vehicle` int(11) DEFAULT NULL,
  `kilometrage` varchar(10) COLLATE utf8_bin DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `vehicle_interior_state`
--

INSERT INTO `vehicle_interior_state` (`id`, `siege_avant_droit`, `siege_avant_gauche`, `banquette_arriere`, `id_vehicle`, `kilometrage`) VALUES
(1, 'A', 'A', 'A', 297, ''),
(2, '', '', '', 328, ''),
(3, '', '', '', 329, ''),
(4, '', '', '', 330, ''),
(5, '', '', '', 331, ''),
(6, '', '', '', 332, ''),
(7, '', '', '', 333, ''),
(8, '', '', '', 334, ''),
(9, '', '', '', 335, ''),
(10, '', '', '', 336, ''),
(11, '', '', '', 337, ''),
(12, '', '', '', 338, ''),
(13, '', '', '', 339, ''),
(14, '', '', '', 340, ''),
(15, '', '', '', 341, ''),
(16, '', '', '', 342, ''),
(17, '', '', '', 343, ''),
(18, '', '', '', 344, ''),
(19, '', '', '', 345, ''),
(20, '', '', '', 344, NULL),
(21, '', '', '', 345, NULL),
(22, '', '', '', 346, NULL),
(23, '', '', '', 347, NULL),
(24, '', '', '', 348, NULL),
(25, '', '', '', 349, NULL),
(26, '', '', '', 350, NULL),
(27, '', '', '', 351, NULL),
(28, '', '', '', 352, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_interior_status`
--

CREATE TABLE `vehicle_interior_status` (
  `id` int(11) NOT NULL,
  `id_part` int(11) DEFAULT NULL,
  `status` varchar(50) COLLATE utf8_bin DEFAULT NULL,
  `id_vehicle` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;

--
-- Déchargement des données de la table `vehicle_interior_status`
--

INSERT INTO `vehicle_interior_status` (`id`, `id_part`, `status`, `id_vehicle`) VALUES
(7, 1, 'sqq', 360),
(8, 2, 'qq', 360),
(9, 3, 'qq', 360);

-- --------------------------------------------------------

--
-- Structure de la table `vehicle_models`
--

CREATE TABLE `vehicle_models` (
  `Id` int(11) NOT NULL,
  `model` varchar(50) DEFAULT NULL,
  `id_brand` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `vehicle_models`
--

INSERT INTO `vehicle_models` (`Id`, `model`, `id_brand`) VALUES
(1, 'Creta', 1),
(2, 'Grand i10', 1),
(3, 'NEW i20', 1),
(4, 'Accent RB 4 portes', 1),
(5, 'New Tucson', 1),
(6, 'Accent RB 5 portes', 1),
(7, 'New i30', 1),
(8, 'SantaFe', 1),
(9, 'Elantra', 1),
(10, 'Grand i10 Sedan', 1),
(11, 'i30', 1),
(12, 'Accent', 1),
(13, 'i20', 1),
(14, 'i10', 1),
(15, 'H1', 1),
(16, 'i40', 1),
(17, 'Tucson', 1),
(18, 'Veloster', 1),
(19, 'EON', 1),
(20, 'i10 Plus', 1),
(21, 'Genesis', 1),
(22, 'i40SW', 1),
(23, 'New Sonata', 1),
(24, 'Equus', 1),
(25, 'Genesis Coupé', 1),
(26, 'Atos', 1),
(27, 'Azera', 1),
(28, 'Coupé', 1),
(29, 'Getz', 1),
(30, 'i30CW', 1),
(31, 'ix55', 1),
(32, 'Matrix', 1),
(33, 'New Accent', 1),
(34, 'Pony', 1),
(35, 'Terracan', 1),
(36, 'Trajet', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `accidents`
--
ALTER TABLE `accidents`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `carrier_uggroups`
--
ALTER TABLE `carrier_uggroups`
  ADD PRIMARY KEY (`GroupID`);

--
-- Index pour la table `carrier_ugmembers`
--
ALTER TABLE `carrier_ugmembers`
  ADD PRIMARY KEY (`UserName`(50),`GroupID`);

--
-- Index pour la table `carrier_ugrights`
--
ALTER TABLE `carrier_ugrights`
  ADD PRIMARY KEY (`TableName`(50),`GroupID`);

--
-- Index pour la table `carte_naftal`
--
ALTER TABLE `carte_naftal`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `chauffeur`
--
ALTER TABLE `chauffeur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_structure` (`id_structure`);

--
-- Index pour la table `compagnie_assurance`
--
ALTER TABLE `compagnie_assurance`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `creategrn`
--
ALTER TABLE `creategrn`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `fleets`
--
ALTER TABLE `fleets`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `fleettype`
--
ALTER TABLE `fleettype`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `fuelmaster`
--
ALTER TABLE `fuelmaster`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `fuelprices`
--
ALTER TABLE `fuelprices`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `fuelstationmaster`
--
ALTER TABLE `fuelstationmaster`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `historique_accident`
--
ALTER TABLE `historique_accident`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vehicle` (`id_vehicle`);

--
-- Index pour la table `historique_affectation`
--
ALTER TABLE `historique_affectation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_priorietaire` (`id_priorietaire`),
  ADD KEY `id_structure` (`id_structure`),
  ADD KEY `id_vehicle` (`id_vehicle`);

--
-- Index pour la table `historique_assurance`
--
ALTER TABLE `historique_assurance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vehicle` (`id_vehicle`);

--
-- Index pour la table `historique_entretien`
--
ALTER TABLE `historique_entretien`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vehicle` (`id_vehicle`);

--
-- Index pour la table `historique_reparation`
--
ALTER TABLE `historique_reparation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vehicle` (`id_vehicle`);

--
-- Index pour la table `historique_vehicle_chauffeur`
--
ALTER TABLE `historique_vehicle_chauffeur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_vehicle` (`id_vehicle`),
  ADD KEY `id_chauffeur` (`id_chauffeur`);

--
-- Index pour la table `insurance-payments`
--
ALTER TABLE `insurance-payments`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `insuranceclaims`
--
ALTER TABLE `insuranceclaims`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `insurancecompany`
--
ALTER TABLE `insurancecompany`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `inventorymaster`
--
ALTER TABLE `inventorymaster`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `maintenenace`
--
ALTER TABLE `maintenenace`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `otherrenewal`
--
ALTER TABLE `otherrenewal`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `priorietaire`
--
ALTER TABLE `priorietaire`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_type_structure` (`id_type_structure`);

--
-- Index pour la table `reparateur`
--
ALTER TABLE `reparateur`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `rnewalmastertable`
--
ALTER TABLE `rnewalmastertable`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `servicetypemaster`
--
ALTER TABLE `servicetypemaster`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `structure_affectation`
--
ALTER TABLE `structure_affectation`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `id_type_structure` (`id_type_structure`);

--
-- Index pour la table `suppliermaster`
--
ALTER TABLE `suppliermaster`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `type_entretien`
--
ALTER TABLE `type_entretien`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `type_structure`
--
ALTER TABLE `type_structure`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`ID`,`numero_immatriculation`);

--
-- Index pour la table `vehiclemaster`
--
ALTER TABLE `vehiclemaster`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `vehicletype`
--
ALTER TABLE `vehicletype`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `vehicle_brands`
--
ALTER TABLE `vehicle_brands`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `vehicle_exterior_parts`
--
ALTER TABLE `vehicle_exterior_parts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicle_exterior_state`
--
ALTER TABLE `vehicle_exterior_state`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicle_exterior_status`
--
ALTER TABLE `vehicle_exterior_status`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicle_interior_parts`
--
ALTER TABLE `vehicle_interior_parts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicle_interior_state`
--
ALTER TABLE `vehicle_interior_state`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicle_interior_status`
--
ALTER TABLE `vehicle_interior_status`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vehicle_models`
--
ALTER TABLE `vehicle_models`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `id_brand` (`id_brand`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `accidents`
--
ALTER TABLE `accidents`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `carrier_uggroups`
--
ALTER TABLE `carrier_uggroups`
  MODIFY `GroupID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `carte_naftal`
--
ALTER TABLE `carte_naftal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `chauffeur`
--
ALTER TABLE `chauffeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `compagnie_assurance`
--
ALTER TABLE `compagnie_assurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `creategrn`
--
ALTER TABLE `creategrn`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=225;

--
-- AUTO_INCREMENT pour la table `fleets`
--
ALTER TABLE `fleets`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `fleettype`
--
ALTER TABLE `fleettype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `fuelmaster`
--
ALTER TABLE `fuelmaster`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;

--
-- AUTO_INCREMENT pour la table `fuelprices`
--
ALTER TABLE `fuelprices`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `fuelstationmaster`
--
ALTER TABLE `fuelstationmaster`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `historique_accident`
--
ALTER TABLE `historique_accident`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `historique_affectation`
--
ALTER TABLE `historique_affectation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=48;

--
-- AUTO_INCREMENT pour la table `historique_assurance`
--
ALTER TABLE `historique_assurance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `historique_entretien`
--
ALTER TABLE `historique_entretien`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `historique_reparation`
--
ALTER TABLE `historique_reparation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `historique_vehicle_chauffeur`
--
ALTER TABLE `historique_vehicle_chauffeur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `insurance-payments`
--
ALTER TABLE `insurance-payments`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `insuranceclaims`
--
ALTER TABLE `insuranceclaims`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `insurancecompany`
--
ALTER TABLE `insurancecompany`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `inventorymaster`
--
ALTER TABLE `inventorymaster`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `maintenenace`
--
ALTER TABLE `maintenenace`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- AUTO_INCREMENT pour la table `otherrenewal`
--
ALTER TABLE `otherrenewal`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `priorietaire`
--
ALTER TABLE `priorietaire`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `reparateur`
--
ALTER TABLE `reparateur`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `rnewalmastertable`
--
ALTER TABLE `rnewalmastertable`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `servicetypemaster`
--
ALTER TABLE `servicetypemaster`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT pour la table `structure_affectation`
--
ALTER TABLE `structure_affectation`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT pour la table `suppliermaster`
--
ALTER TABLE `suppliermaster`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `type_entretien`
--
ALTER TABLE `type_entretien`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `type_structure`
--
ALTER TABLE `type_structure`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=361;

--
-- AUTO_INCREMENT pour la table `vehiclemaster`
--
ALTER TABLE `vehiclemaster`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `vehicletype`
--
ALTER TABLE `vehicletype`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `vehicle_brands`
--
ALTER TABLE `vehicle_brands`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=81;

--
-- AUTO_INCREMENT pour la table `vehicle_exterior_parts`
--
ALTER TABLE `vehicle_exterior_parts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `vehicle_exterior_state`
--
ALTER TABLE `vehicle_exterior_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `vehicle_exterior_status`
--
ALTER TABLE `vehicle_exterior_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=113;

--
-- AUTO_INCREMENT pour la table `vehicle_interior_parts`
--
ALTER TABLE `vehicle_interior_parts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `vehicle_interior_state`
--
ALTER TABLE `vehicle_interior_state`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT pour la table `vehicle_interior_status`
--
ALTER TABLE `vehicle_interior_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `vehicle_models`
--
ALTER TABLE `vehicle_models`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `historique_assurance`
--
ALTER TABLE `historique_assurance`
  ADD CONSTRAINT `historique_assurance_ibfk_1` FOREIGN KEY (`id_vehicle`) REFERENCES `vehicle` (`ID`);

--
-- Contraintes pour la table `historique_vehicle_chauffeur`
--
ALTER TABLE `historique_vehicle_chauffeur`
  ADD CONSTRAINT `historique_vehicle_chauffeur_ibfk_1` FOREIGN KEY (`id_vehicle`) REFERENCES `vehicle` (`ID`);

--
-- Contraintes pour la table `priorietaire`
--
ALTER TABLE `priorietaire`
  ADD CONSTRAINT `priorietaire_ibfk_1` FOREIGN KEY (`id_type_structure`) REFERENCES `type_structure` (`Id`);

--
-- Contraintes pour la table `structure_affectation`
--
ALTER TABLE `structure_affectation`
  ADD CONSTRAINT `structure_affectation_ibfk_1` FOREIGN KEY (`id_type_structure`) REFERENCES `type_structure` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
